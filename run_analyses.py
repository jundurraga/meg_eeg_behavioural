import subprocess
from environment import r_script_binary_path
# before running this script, please make sure to set up the environment.py file with the correct path for your local
# environment

# MEG ITD-FR sensor analyses#################
exec(open("./meg_processing/meg_sensor_database.py").read())
exec(open("./meg_processing/plot_freq_time_itd_fr_meg.py").read())
exec(open("./meg_processing/plots_topo_itm_fr_meg.py").read())
exec(open("./meg_processing/plot_plv_frequency_domain_waveforms_meg.py").read())
exec(open("./meg_processing/plots_topo_plv_meg.py").read())
exec(open("./meg_processing/damping_fitting_meg.py").read())

# MEG ASSR sensor analyses#################
exec(open("./meg_processing/meg_sensor_assr_database.py").read())
exec(open("./meg_processing/plots_topo_assr_meg.py.py").read())
exec(open("./meg_processing/plot_freq_time_assr_meg.py").read())

subprocess.call(r_script_binary_path + " ./statistical_analyses/meg/meg_stats_sensor_level.R", shell=True)

# MEG source analyses #################
exec(open("./meg_processing/meg_preparation_pipeline.py").read())
exec(open("./meg_processing/meg_group_average_from_time_sources.py").read())
exec(open("./meg_processing/meg_group_average_phase_locking_and_latency.py").read())
exec(open("./meg_processing/meg_individual_average_phase_locking_and_latency.py").read())
subprocess.call(r_script_binary_path +
                " ./statistical_analyses/meg/meg_sources_phase_locking_phase_latency_analysis.R",
                shell=True)
subprocess.call(r_script_binary_path +
                " ./statistical_analyses/meg/meg_sources_phase_locking_phase_latency_analysis_individuals.R",
                shell=True)
exec(open("./meg_processing/meg_group_average_time_course_videos.py").read())

exec(open("./meg_processing/meg_group_average_time_course_roi.py").read())

subprocess.call(r_script_binary_path + " ./statistical_analyses/meg/meg_sources_analysis.R", shell=True)

# MEG temporal phase #################
exec(open("./meg_processing/meg_sensor_assr_temporal_phase.py").read())
exec(open("./meg_processing/plot_freq_time_assr_meg_phase.py").read())
exec(open("./meg_processing/plots_topo_assr_meg.py").read())

# EEG ITD-FR ################################
exec(open("./eeg_processing/itd_fr_pipeline.py").read())
exec(open("./eeg_processing/plot_freq_time_itd_fr_eeg.py").read())
exec(open("./eeg_processing/plots_potentials_itd_fr_eeg.py").read())
exec(open("./eeg_processing/damping_fitting_eeg.py").read())

# EEG ASSR ################################
exec(open("./eeg_processing/assr_pipeline.py").read())
exec(open("./eeg_processing/plot_freq_time_assr_eeg.py").read())
exec(open("./eeg_processing/plots_potentials_assr_eeg.py").read())

# EEG temporal phase #################
exec(open("./eeg_processing/itd_fr_pipeline_temporal_phase.py").read())
exec(open("./eeg_processing/plot_freq_time_assr_phase_eeg.py").read())

subprocess.call(r_script_binary_path + " ./statistical_analyses/eeg/eeg_stats.R", shell=True)

# Behavioural ########################
exec(open("./behavioural_processing/damping_fitting_behavioural.py").read())

subprocess.call(r_script_binary_path + " ./statistical_analyses/behavioural/beahvioural_ranking_analysis.R",
                shell=True)
subprocess.call(r_script_binary_path + " ./statistical_analyses/group_analysis/meg_eeg_behavioural_analysis.R",
                shell=True)

# Computational model
exec(open("./computational_model/modeling_distributions_damping_ipd_space.py").read())
exec(open("./computational_model/modeling_distributions_damping_itd_space.py").read())
exec(open("./computational_model/plot_results_neural_data_model_ipd.py").read())
exec(open("./computational_model/plot_results_neural_data_model_itd.py").read())
exec(open("./computational_model/plot_results_neural_data_model_itd_to_ipd.py").read())

# Generate and fit simulated distributions
exec(open("./computational_model/modeling_distributions_damping_test_ipd.py").read())
exec(open("./computational_model/plot_results_simulated_distributions_ipd.py").read())

exec(open("./computational_model/modeling_distributions_damping_test_itd.py").read())
exec(open("./computational_model/plot_results_simulated_distributions_itd.py").read())

# Comparison with Macaque data
exec(open("./macaque_comparison/plot_results_macaque_data.py").read())
exec(open("./macaque_comparison/plot_results_macaque_data_itd_to_ipd.py").read())

# Comparison with Undurraga 2016
subprocess.call(r_script_binary_path +
                " ./statistical_analyses/comparison_undurraga_2016/comparison_with_undurraga_2016.R",
                shell=True)
