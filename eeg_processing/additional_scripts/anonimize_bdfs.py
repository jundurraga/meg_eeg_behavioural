# -*- coding: utf-8 -*-
from peegy.io.external_tools.file_tools import get_files_and_meta_files
from environment import eeg_data_directory
from peegy.io.edf_bdf_writer import change_subject_id
if __name__ == "__main__":
    to_process = get_files_and_meta_files(eeg_data_directory)
    for _, _data_links in to_process.iterrows():
        change_subject_id(file_name=_data_links.data_links.data_file,
                          subject_id=_data_links.Measurement_MeasurementModule_Subject)
        print(_data_links.Measurement_MeasurementModule_Subject)
