from peegy.io.storage.plot_tools import plot_topographic_maps
from peegy.io.storage.data_storage_reading_tools import sqlite_tables_to_pandas
from peegy.io.storage.plot_tools import plot_topographic_maps, get_topographic_maps_as_df
import numpy as np
import pandas as pd
import matplotlib
from environment import data_path_eeg, figures_path_eeg
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')

database_path = data_path_eeg + 'eeg_assr_data.sqlite'
df = sqlite_tables_to_pandas(database_path=database_path,
                             tables=['hotelling_t2_freq'])
df = df.hotelling_t2_freq
df = df[df.data_source == 'fft_ave_full_freq']
df = df.sort_values(by=['ITD'], ascending=False)
df['itd'] = (df['ITD'] * 1e6).astype(int)
df['alt_itd'] = (df['AltITD'] * 1e6).astype(int)
df['diotic'] = df['DioticControl'] == 1
df['frequency_tested'] = np.round(df['frequency_tested'], decimals=1).astype('category')
df['frequency_tested'] = df['frequency_tested'].cat.remove_unused_categories()
df['itd_condition'] = ('$\pm$' + df['alt_itd'].astype(str) + ' $\mu$s').astype('category')
dataset = df.query('diotic == False and itd == -alt_itd and frequency_tested == 40.4')
dataset = dataset.reset_index(drop=True)
dataset['itd_condition'] = dataset['itd_condition'].cat.remove_unused_categories()
dataset['itd_condition'] = dataset['itd_condition'].cat.reorder_categories(['$\pm$500 $\mu$s',
                                                                            '$\pm$1000 $\mu$s',
                                                                            '$\pm$1500 $\mu$s',
                                                                            '$\pm$2000 $\mu$s',
                                                                            '$\pm$2500 $\mu$s',
                                                                            '$\pm$3000 $\mu$s',
                                                                            '$\pm$3500 $\mu$s',
                                                                            '$\pm$4000 $\mu$s'])

dataset['complex_amplitude'] = dataset['mean_amplitude'] * np.exp(1j * dataset['mean_phase'])
# extract the spectral magnitude
complex_average = get_topographic_maps_as_df(dataframe=dataset,
                                             group_by=['itd_condition'],
                                             channels_column='channel',
                                             topographic_value='complex_amplitude',
                                             layout='biosemi64_2_EXT.lay',
                                             apply_function=lambda x: np.ma.mean(np.abs(x), axis=2)
                                             )
# reference phase set as the reference of the maximum amplitude per condition
ref_value = pd.DataFrame()
ref_value['ref_location'] = complex_average.groupby(['itd_condition'], observed=True)['potentials'].apply(
    lambda x: np.unravel_index(np.abs(x.values[0]).argmax(), x.values[0].shape))
ref_value = ref_value.reset_index()
centered_data = complex_average.copy()

_idx_match = int(np.argwhere(ref_value['itd_condition'] == '$\pm$500 $\mu$s'))
_i, _j = ref_value.at[_idx_match, 'ref_location']
ref_phase = np.angle(centered_data.at[_idx_match, 'potentials'][_i, _j])

for _is, _ref_row in ref_value.iterrows():
    # _idx_match = int(np.argwhere(centered_data['itd_condition'] == _ref_row['itd_condition']))
    # _i, _j = _ref_row['ref_location']
    # ref_phase = np.angle(centered_data.at[_idx_match, 'potentials'][_i, _j])
    centered_data.at[_is, 'potentials'] = (
            np.abs(centered_data.at[_is, 'potentials']) *
            np.cos(np.ma.angle(centered_data.at[_is, 'potentials']) - ref_phase))


fig_out = plot_topographic_maps(dataframe=centered_data,
                                rows_by='itd_condition',
                                normalize=False,
                                channels_column='channel',
                                layout='biosemi64_2_EXT.lay',
                                title_by=None,
                                title_v_offset=-0.05,
                                min_topographic_value=0,
                                max_topographic_value=0.45,
                                # title='ASSR',
                                apply_function=lambda x: np.ma.mean(x, axis=2),
                                show_sensors=False,
                                color_map_label='Amplitude [$\mu$V]'
                                )
inch = 2.54
fig_out.set_size_inches(2.5/inch, 12/inch)
fig_out.savefig(figures_path_eeg + 'figure_S2a.pdf', dpi=600)
fig_out.savefig(figures_path_eeg + 'figure_S2a.png', dpi=600)
