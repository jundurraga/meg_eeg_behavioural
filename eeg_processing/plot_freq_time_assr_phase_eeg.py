import numpy as np
import matplotlib
from peegy.io.storage.data_storage_reading_tools import sqlite_waveforms_to_pandas
from peegy.io.storage.plot_tools import plot_time_frequency_responses
from environment import data_path_eeg, figures_path_eeg
import astropy.units as u
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')

database_path = data_path_eeg + 'eeg_data_morlet_aligned.sqlite'

df = sqlite_waveforms_to_pandas(database_path=database_path,
                                group_factors=['ITD',
                                               'AltITD',
                                               'ITDModRate',
                                               'ModulationFrequency',
                                               'data_source',
                                               'DioticControl'],
                                channels=['EXG1'])

df = df[np.isin(df.data_source, ['phase_peaks_dss'])]
df = df.sort_values(by=['ITD'], ascending=False)
df['itd'] = (df['ITD'] * 1e6).astype(int)
df['alt_itd'] = (df['AltITD'] * 1e6).astype(int)
df['diotic'] = df['DioticControl'] == 1
dataset = df.query('diotic == False and itd == -alt_itd')
dataset['itd_condition'] = ('$\pm$' + dataset['alt_itd'].astype(str) + ' $\mu$s').astype('category')
dataset = dataset.reset_index(drop=True)
dataset['itd_condition'] = dataset['itd_condition'].cat.remove_unused_categories()
dataset['itd_condition'] = dataset['itd_condition'].cat.reorder_categories(['$\pm$500 $\mu$s',
                                                                            '$\pm$1000 $\mu$s',
                                                                            '$\pm$1500 $\mu$s',
                                                                            '$\pm$2000 $\mu$s',
                                                                            '$\pm$2500 $\mu$s',
                                                                            '$\pm$3000 $\mu$s',
                                                                            '$\pm$3500 $\mu$s',
                                                                            '$\pm$4000 $\mu$s'])

n_cycles_to_plot = 2
x_fs = np.mean(dataset['x_fs'])
samples_per_cycle = np.mean(dataset['x_fs'] / dataset['ITDModRate'])
sub_average_time_buffer_size = int(samples_per_cycle * n_cycles_to_plot)
time_vmarkers = np.arange(0, sub_average_time_buffer_size, samples_per_cycle) / x_fs
itm_rate = np.mean(df['ITDModRate'])
am_rate = np.mean(df['ModulationFrequency'])
freq_vmarkers = np.array([itm_rate, am_rate])
fig_out = plot_time_frequency_responses(dataframe=dataset,
                                        # cols_by='domain',
                                        rows_by='itd_condition',
                                        title_by='',
                                        sub_average_time_buffer_size=sub_average_time_buffer_size,
                                        time_xlim=[0, sub_average_time_buffer_size / x_fs],
                                        time_ylim=[-45, 45],
                                        freq_xlim=[0, 48],
                                        freq_ylim=[0, 0.7],
                                        time_vmarkers=time_vmarkers,
                                        freq_vmarkers=freq_vmarkers,
                                        freq_vmarkers_style='v',
                                        title_v_offset=-0.35,
                                        ylabel='Phase [degrees]',
                                        y_unit_to=u.deg,
                                        show_sd=True,
                                        show_sem=False,
                                        show_individual_waveforms=True,
                                        individual_waveforms_alpha=0.05,
                                        sd_alpha=0.3
                                        )
inch = 2.54
fig_out.set_size_inches(6.5/2/inch, 12/inch)
fig_out.subplots_adjust(top=0.98, bottom=0.07, wspace=0.38, left=0.3, right=0.80)
fig_out.savefig(figures_path_eeg + 'figure_S2_D.pdf', dpi=600)
fig_out.savefig(figures_path_eeg + 'figure_S2_D.pdf', dpi=600)
