from typing import List
import computational_model.modeling_distributions_tools as tools
import matplotlib
import numpy as np
import pandas as pd
import os
from environment import data_path_model, figures_path_model
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')


n_ac_neuron_per_band = 100
ipd_space = np.linspace(-np.pi, np.pi, 600)
cfs = np.linspace(150, 850, 15)
can_function = 'exp'
sigma = np.pi / 8
itds = np.linspace(-0.0045, 0.0045, 1000)
fix_weights = True

neural_distributions: List[str] = ['pi_limit', 'hancock_delgutte', 'stern_shear', 'colburn']
for neural_distribution in neural_distributions:
    data = pd.DataFrame()
    # generate target distributions
    weights, best_itds = tools.neural_weight(central_frequencies=cfs,
                                             model=neural_distribution,
                                             n_neurons=200,
                                             max_itd=0.004
                                             )

    fig_out = tools.plot_itd_distribution_and_ac_population_response(cfs=cfs,
                                                                     best_itds=best_itds,
                                                                     itds=itds,
                                                                     weights=weights,
                                                                     target_response=None,
                                                                     can_function=can_function,
                                                                     sigma=sigma,
                                                                     make_symmetric_itd=False,
                                                                     show_fitted=False)
    fig_out.show()
