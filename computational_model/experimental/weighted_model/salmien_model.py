import matplotlib.pyplot as plt
import pandas as pd

import pysignal_generator.sin_functions as sg
import pysignal_generator.noise_functions as nf
from matplotlib import gridspec
from pysignal_generator.tools import rms
from binaural.correlogram.correlogram import *
import matplotlib

if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')


def fit_axis(fig):
    all_axes = fig.get_axes()
    for ax in all_axes:
        ax.tick_params(axis='x', which='both', top='off')
        ax.tick_params(axis='y', which='both', right='off')
        for sp in ax.spines.values():
            sp.set_visible(False)
        if not ax._subplotspec.is_first_row():
            ax.spines['top'].set_visible(False)
        if not ax._subplotspec.is_last_row():
            ax.set_xticklabels([])
            ax.set_xlabel('')
        else:
            ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(False)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(12)
    fig.tight_layout()


fs = 44100.
max_time_lag = 0.005
f_low = 300
f_high = 700
n_filters = 500
n_repetitions = 2
itds = np.arange(0.0005, 0.0045, 0.0005)
root_path = '/home/jundurraga/pCloudDrive/Documents/Presentations/international_binaural_workshop_2018/figures/'
model = 'pi_limit'
# model = 'pi_limit_restricted'
ipd_low = 0.24
ipd_high = 0.25
# model = 'stern_shear'
mean_activity = np.empty((n_repetitions, itds.size))
rms_activity = np.empty((n_repetitions, itds.size))
mean_diff_activity = np.empty((n_repetitions, itds.size))
rms_diff_activity = np.empty((n_repetitions, itds.size))
for _n in range(n_repetitions):
    for _i, itd in enumerate(itds):
        pl = {'itd': -itd,
              'alt_itd': -itd,
              'amplitude': 0.9,
              'modulation_index': 1.0,
              'modulation_frequency': 41.0,
              'carrier_frequency': 520.0,
              'cycle_rate': 16384.0,
              'fs': fs,
              'duration': 0.5,
              'itd_mod_rate': 2.0,
              'include_triggers': 0,
              }
        pulse, time = nf.generate_noise_alt_delay_signal(pl)

        correlogram, weighted_correlogram, lags, cfs = get_correlogram(pulse[:, 0],
                                                                       pulse[:, 1],
                                                                       fs=fs,
                                                                       f_low=f_low,
                                                                       f_high=f_high,
                                                                       neural_density_model=model,
                                                                       n_filters=n_filters,
                                                                       max_time_lag=max_time_lag,
                                                                       ipd_low=ipd_low,
                                                                       ipd_high=ipd_high)

        across_freq_activity = np.sum(weighted_correlogram, axis=1)
        rms_activity[_n, _i] = rms(across_freq_activity)
        mean_activity[_n, _i] = np.mean(across_freq_activity)
        diff_activity = np.abs(across_freq_activity - np.flip(across_freq_activity, axis=0))

        rms_diff_activity[_n, _i] = rms(diff_activity)
        mean_diff_activity[_n, _i] = np.mean(diff_activity)
        if _n == 0  and _i == 0:
            across_freq_activity_matrix = np.zeros((n_repetitions, len(itds), across_freq_activity.shape[0]))
        across_freq_activity_matrix[_n, _i, :] = across_freq_activity
        print('repetition {:d}, itd: {:.1f}'.format(_n, itd * 1000))

# inch_cm = 2.54
# fig = plt.figure()
# gs = gridspec.GridSpec(1, 1)
# ax = fig.add_subplot((gs[0, 0:]))
# ax.errorbar(itds * 1000, np.mean(rms_activity, axis=0), yerr=np.std(rms_activity, axis=0), fmt='o')
# ax.set_xlabel("ITD [ms]")
# ax.set_ylabel("RMS activity [au]")
# fit_axis(fig)
# fig.set_size_inches(18 / inch_cm, 12 / inch_cm)
# fig.savefig(root_path + 'rms_neural_activity_{:}.png'.format(model))
# plt.close(fig)
#
#
# fig = plt.figure()
# gs = gridspec.GridSpec(1, 1)
# ax = fig.add_subplot((gs[0, 0:]))
# ax.errorbar(itds * 1000, np.mean(mean_activity, axis=0), yerr=np.std(rms_activity, axis=0), fmt='o')
# ax.set_xlabel("ITD [ms]")
# ax.set_ylabel("Average activity [au]")
# fit_axis(fig)
# fig.set_size_inches(18 / inch_cm, 12 / inch_cm)
# fig.savefig(root_path + 'ave_neural_activity_{:}.png'.format(model))
# plt.close(fig)
df = pd.DataFrame()
itd_m = np.ones(across_freq_activity_matrix.shape) * itds[None, :, None]
rep_m = np.ones(across_freq_activity_matrix.shape) * np.arange(n_repetitions)[:, None, None]
lags_m = np.ones(across_freq_activity_matrix.shape) * lags[None, None, :]

itd_col = np.reshape(itd_m, [-1, 1, 1]).squeeze()
rep_col = np.reshape(rep_m, [-1, 1, 1]).squeeze()
lag_col = np.reshape(lags_m, [-1, 1, 1]).squeeze()
df['lags'] = lag_col
df['repetition'] = rep_col
df['itd'] = itd_col
df.to_csv()
inch_cm = 2.54
fig = plt.figure()
gs = gridspec.GridSpec(1, 1)
ax = fig.add_subplot((gs[0, 0:]))
ax.errorbar(itds * 1000, np.mean(rms_diff_activity, axis=0), yerr=np.std(rms_diff_activity, axis=0), fmt='o')
ax.set_xlabel("ITD [ms]")
ax.set_ylabel("RMS activity [au]")
fit_axis(fig)
fig.set_size_inches(18 / inch_cm, 12 / inch_cm)
fig.savefig(root_path + 'rms_diff_neural_activity_{:}.png'.format(model))
plt.close(fig)

fig = plt.figure()
gs = gridspec.GridSpec(1, 1)
ax = fig.add_subplot((gs[0, 0:]))
ax.errorbar(itds * 1000, np.mean(mean_diff_activity, axis=0), yerr=np.std(mean_diff_activity, axis=0), fmt='o')
ax.set_xlabel("ITD [ms]")
ax.set_ylabel("Average activity [au]")
fit_axis(fig)
fig.set_size_inches(18 / inch_cm, 12 / inch_cm)
fig.savefig(root_path + 'ave_diff_neural_activity_{:}.png'.format(model))
plt.close(fig)
