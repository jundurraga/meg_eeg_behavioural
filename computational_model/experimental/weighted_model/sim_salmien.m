n_repetitions = 10;
% itds = [0.0005, 0.0015];
itds = 0.0005: 0.0005 : 0.004;
weighted_output = [];
total_output = zeros(numel(itds), 1);
% generate a reference for SPL calibration
noise = mcreatenoise1(500, 400, 40, 40, 0, 0, 1000, 10, 48000, 1);
ref_rms = rms(noise.leftwaveform);
ref = 10 ^ (-65 / 20)  * ref_rms;
neural_density = 'shear';
% neural_density = 'colburn';
% neural_density = 'shackleton';
for i = 1: numel(itds)
    for j = 1 : n_repetitions
        [y,t,s] = generateNoiseAltDelaySignal(...
            'Amplitude', 65, ...
            'Fs', 48000, ...
            'Duration', 4.056/4,...
            'CycleRate', 16384, ...
            'ModulationFrequency', 44.871794871794876, ...
            'ModulationIndex', 1, ...
            'ITDModRate', 6.4102564102564106, ...
            'RoundToCycle', 1, ...
            'ITD', itds(i), ...
            'AltITD', itds(i), ...
            'refSPL', ref ...
        );
    disp(strcat('SPL reference: ', num2str(20 * log10(ref_rms / ref))))
    disp(strcat('SPL stimulus: ', num2str(20 * log10(rms(y)/ ref))))
    %     
    noise.leftwaveform = y(:, 1) * 32765;
    noise.rightwaveform = y(:, 2) * 32765;

    %  noise = mcreatenoise1(500, 400, 40, 40, itds(i)*1e6, 0, 1000, 10, 48000, 1);
    crosscorrelgram = mcorrelogram(50,1500,4,-4000,4000,'hw','cp',noise, 1);
    [weighted_correlogram, output_delayweight] = mccgramdelayweight(crosscorrelgram, neural_density, 1);
    
        % use the fancier display
    %             mccgramplot4panel(crosscorrelgram);
    %             mccgramplot4panel(output_correlogram);

    across_freq = mccgramaverage(weighted_correlogram);
    if isempty(weighted_output)
        weighted_output = zeros(numel(itds), n_repetitions, ...
            size(across_freq , 2));
    end
    total_output(i, j) = sum(across_freq);
    weighted_output(i,j,:) = across_freq; 
    % the end
        %------------------------
    end
end
figure;
mean_activation = mean(total_output,2);
std_activation = std(total_output,[], 2);
plot(itds * 1e3, mean_activation);
hold on 
plot(itds * 1e3, mean_activation + std_activation );
plot(itds * 1e3, mean_activation - std_activation );


figure;
ave_res = squeeze(mean(weighted_output,2));
std_res = squeeze(std(weighted_output,[], 2));
plot(weighted_correlogram.delayaxis, ave_res);
hold on;
plot(weighted_correlogram.delayaxis, ave_res + std_res);
plot(weighted_correlogram.delayaxis, ave_res - std_res);

%%%%%%% acrosss frequency activation
itd_m = ones(size(weighted_output));
itd_m = itd_m .* itds';
rep_m = ones(size(weighted_output));
rep_m  = rep_m .* (1:n_repetitions);
x_axis = ones(size(weighted_output));
x_axis   = x_axis .* reshape(weighted_correlogram.delayaxis,1, 1, []);

data_col = reshape(reshape(weighted_output, 2, []), [], 1);
itd_col = reshape(reshape(itd_m, 2, []), [], 1);
rep_col = reshape(reshape(rep_m, 2, []), [], 1);
x_axis_col = reshape(reshape(x_axis, 2, []), [], 1);

df = DataFrame(itd_col, rep_col , x_axis_col, data_col, ...
    'VariableNames', {'itd', 'repetition', 'lag', 'activation'});
data_path =  dir(which(mfilename('fullpath'))) ;
cd(data_path.folder);
writetable(struct2table(df.toStruct), '../data/weighted_model_' + ...
    neural_density + '.csv')
%%%%%%% todal output
itd_m = ones(size(total_output));
itd_m = itd_m .* itds';
rep_m = ones(size(total_output));
rep_m  = rep_m .* (1:n_repetitions);
data_col = reshape(reshape(total_output, 2, []), [], 1);
itd_col = reshape(reshape(itd_m, 2, []), [], 1);
rep_col = reshape(reshape(rep_m, 2, []), [], 1);
df2 = DataFrame(itd_col, rep_col , data_col, ...
    'VariableNames', {'itd', 'repetition', 'total_activation'});
data_path =  dir(which(mfilename('fullpath'))) ;
cd(data_path.folder);
writetable(struct2table(df2.toStruct), ... 
    '../data/weighted_total_activation_' + neural_density + '.csv');




function data = mccgramaverage(correlogram)
    % average
    for delay=1: correlogram.ndelays
        integratedccfunction(delay) = 0;
        for filter=1: correlogram.nfilters
            integratedccfunction(delay) = integratedccfunction(delay) + correlogram.data(filter, delay);
        end
    end
    data = integratedccfunction;
end
