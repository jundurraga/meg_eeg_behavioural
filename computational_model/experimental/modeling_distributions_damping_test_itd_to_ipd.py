from typing import List
import computational_model.modeling_distributions_tools as tools
import matplotlib
import numpy as np
import pandas as pd
import os
from environment import data_path_model, figures_path_model
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')


n_ac_neuron_per_band = 100
itd_space = np.linspace(-4000e-6, 4000e6, 600)
cfs = np.linspace(150, 850, 15)
neural_distributions: List[str] = ['GuineaPig', 'ramp_down', 'ramp_up', 'straight']
can_function = 'exp'
sigma = np.pi/8
itds = np.linspace(-4000e-6, 4000e-6, 80)
fix_weights = True

method = "BH-L-BFGS-B"
# method = "DE"
# method = "L-BFGS-B"
# method = "GA"
# method = "PSO"
for neural_distribution in neural_distributions:
    data = pd.DataFrame()
    # generate target ipds
    best_ipds = tools.generate_population(n_ac_neuron_per_band=n_ac_neuron_per_band, cfs=cfs, type=neural_distribution)
    best_itds = best_ipds / (2 * np.pi * cfs)
    weights = np.ones(best_itds.shape)
    # weights[:, 0:5] = 1

    target_response = tools.ic_population_response_itd(cfs=cfs,
                                                       itds=itds,
                                                       best_itds=best_itds,
                                                       weights=weights,
                                                       make_symmetric_itd=True,
                                                       can_function=can_function,
                                                       sigma=sigma)

    fig_out = tools.plot_itd_distribution_and_ac_population_response(cfs=cfs,
                                                                     best_itds=best_itds,
                                                                     itds=itds,
                                                                     weights=weights,
                                                                     target_response=target_response,
                                                                     can_function=can_function,
                                                                     sigma=sigma,
                                                                     freq_band_width=100,
                                                                     show_fitted=False)
    fig_out.show()
    _model = '{:}_{:}_{:}_itd'.format(n_ac_neuron_per_band, neural_distribution, int(sigma*180/np.pi))
    fig_out.savefig(figures_path_model + _model + '.png')
    ipds_0 = tools.generate_population(n_ac_neuron_per_band=n_ac_neuron_per_band,
                                       cfs=cfs,
                                       type='gaussian').flatten()
    ipts_0 = 1000e-6 * np.random.randn(n_ac_neuron_per_band, cfs.shape[0])
    # ipds_0 = best_ipds + np.pi/8 * np.random.randn(n_ac_neuron_per_band, cfs.shape[0])

    optimal_itds, optimal_weights, optimal_sigmas = tools.fit_population_itd_to_ipd(
        cfs=cfs,
        best_itds=ipts_0,
        weights=weights,
        fix_weights=fix_weights,
        n_ac_neuron_per_band=n_ac_neuron_per_band,
        itds=itds,
        target_response=target_response,
        method=method,
        n_to_update_plot=200,
        itd_space=itd_space,
        n_generations_pso_ga=200000,
        can_function=can_function,
        sigma=sigma,
        do_plots=True
    )

    fig_out = tools.plot_itd_distribution_and_ac_population_response(
        cfs=cfs,
        best_itds=optimal_itds,
        weights=optimal_weights,
        sigma=optimal_sigmas,
        itds=itds,
        target_response=target_response)

    _data = {''}
    data = pd.concat([data,
                      pd.DataFrame([{'n_ac_neuron_per_band': n_ac_neuron_per_band,
                                     'can_function': can_function,
                                     'sigma': optimal_sigmas,
                                     'case': neural_distribution,
                                     'method': method,
                                     'cfs': cfs,
                                     'best_itds': optimal_itds,
                                     'weights': optimal_weights,
                                     'fix_weights': fix_weights
                                     }])],
                     ignore_index=True)

    _condition = '{:}_{:}_{:}_fix_weights_{:}_cfs_{:}'.format(
        method, n_ac_neuron_per_band, neural_distribution, fix_weights, cfs.size)
    fig_out.savefig(figures_path_model + _condition + '_ipd_fitting_damping_test_power_itd.png')
    data.to_pickle(data_path_model + os.path.sep + _condition + '_results_itd_to_ipd.pkl')
