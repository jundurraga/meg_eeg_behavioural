import modeling_distributions_tools as tools
import matplotlib
import pandas as pd
import numpy as np
import pickle
from scipy.optimize import curve_fit
from scipy.stats import pearsonr
import matplotlib.pyplot as plt
from environment import data_path_model, figures_path_model, data_path_meg
matplotlib.use('TkAgg')


def func(x, a, b, c):
    return a * np.exp(-b * abs(x)**1) + c


files = []

data_path = data_path_model + \
            'original_BH-L-BFGS-B_100_interpolated_fitting_data_meg_exp_True_cfs_15_results_model_ipd_domain.pkl'

# data_path = data_path_model + \
#             'only_damping_BH-L-BFGS-B_100_interpolated_fitting_data_meg_exp_True_cfs_15_results_model_ipd_domain.pkl'


target_path = data_path_meg + 'interpolated_fitting_data_meg.csv'

# read data to fit
ipd_data = np.genfromtxt(target_path,
                         delimiter=',',
                         skip_header=True)
_idx = np.argsort(ipd_data[:, 1])
fitted_target_response = ipd_data[_idx, 2]
itds = ipd_data[_idx, 1] / 1000

with open(data_path, 'rb') as handle:
    results = pickle.load(handle)

v1 = np.arange(0, itds.size // 2)
v2 = np.arange(itds.size // 2, itds.size)
popt, pcov = curve_fit(func, itds[v1], fitted_target_response[v1], maxfev=10000)
target_response_l = func(itds[v1], *popt)
popt, pcov = curve_fit(func, itds[v2], fitted_target_response[v2], maxfev=10000)
target_response_r = func(itds[v2], *popt)
errors = []
sigmas = []
results['unique_sigma'] = results['sigma'].apply(lambda x: np.round(np.unique(x) * 180 / np.pi, decimals=1)[0])
for _i, _row in results.iterrows():
    if _row.case == 'original' and _row.unique_sigma == 22.5:
        cfs = _row.cfs
        type = str(_row.case)
        can_function = str(_row.can_function)
        sigma = _row.sigma
        optimal_ipds = _row.best_ipds
        n_cn_neuron_per_band = _row.n_ac_neuron_per_band
        optimal_weights = _row.weights
        itds = _row.itds
        # filter cfs and ipds to required bandwidth
        low_cut = 300
        high_cut = 700
        itds = np.linspace(-4000e-6, 4000e-6, 160)
        # target_response = np.array([0.1800846, 0.2937604, 0.3544912, 0.3793658, 0.3230197, 0.2513010, 0.1650038])
        _idx = np.argwhere(np.logical_and(cfs >= low_cut, cfs <= high_cut)).squeeze()
        cfs = cfs[_idx]
        optimal_ipds = optimal_ipds[:, _idx]
        optimal_weights = optimal_weights[:, _idx]
        sigma = sigma[:, _idx]
        optimal_response = tools.ic_population_response_ipd(cfs=cfs,
                                                            itds=itds,
                                                            best_ipds=optimal_ipds,
                                                            make_symmetric_ipd=False,
                                                            can_function=can_function,
                                                            sigma=sigma)
        _max_pop = optimal_response.max()
        _min_pop = optimal_response.min()
        optimal_response /= _max_pop
        # _max = target_response.max()
        # _min = target_response.min()
        # target_response /= _max
        _itd_penalization = 1
        # error = np.sum(np.square(np.diff((optimal_response - target_response))))
        # unexplained_var = np.var(optimal_response - target_response)
        # total_var = np.var(target_response)
        # RSquared = 1 - unexplained_var / total_var
        # corr_raw = pearsonr(optimal_response, target_response)
        # save results
        # df = pd.DataFrame({'ipds': np.round(itds / (1/500) * 360, decimals=1),
        #                    'model_amplitude': optimal_response})
        # df.to_csv(
        #     path_or_buf=data_path_model + 'model_sim_sinusoidal.csv')

        human_diameter = 19/100
        f_out, _ = tools.plot_ipd_distribution_and_ac_population_response(
            cfs=cfs,
            best_ipds=optimal_ipds,
            weights=optimal_weights,
            itds=itds,
            # target_response=target_response,
            can_function=can_function,
            sigma=sigma,
            make_symmetric_ipd=False,
            head_diameter=human_diameter,
            freq_band_width=50
        )
        plt.show()
        axes = f_out.get_axes()
        # axes[-2].text(0.2, 0.2, '$R^2$ = {:.2f}'.format(RSquared), fontsize=8)
        if can_function == 'exp':
            axes[-2].text(0.2, 0.1, r'{:}, $\sigma$ = {:.1f}'.format(can_function,
                                                                     round(np.mean(sigma)*180/np.pi, ndigits=1)),
                          fontsize=8)
        else:
            axes[-2].text(0.2, 0.2, r'{:}'.format(can_function), fontsize=8)

        # _error = tools.error_measure(fitted=optimal_ipds,
        #                              cfs=cfs,
        #                              # target_response=target_response,
        #                              itds=itds,
        #                              do_plots=False,
        #                              can_function=can_function,
        #                              sigma=sigma,
        #                              fix_weights=True,
        #                              weights=optimal_weights)
        # errors.append(_error)
        sigmas.append(sigma)
        _condition = 'meg_data_fitting_{:}_{:}_can_fun_{:}_sigma_{:}_sim'.format(
            _row.case,
            _row.n_ac_neuron_per_band*_row.cfs.size,
            can_function, round(np.mean(sigma)*180/np.pi))
        f_out.savefig(figures_path_model + _condition + '.png', bbox_inches='tight')
        f_out.savefig(figures_path_model + _condition + '.pdf', bbox_inches='tight')
        plt.show()
        plt.close(f_out)
