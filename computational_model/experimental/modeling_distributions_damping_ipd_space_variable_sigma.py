import computational_model.modeling_distributions_tools as tools
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
from environment import data_path_model, figures_path_model, data_path_meg
from scipy.optimize import curve_fit
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')


def func(x, a, b, c):
    return a * np.exp(-b * abs(x)**1) + c


# define paths
_data_file = data_path_meg + 'interpolated_fitting_data_meg.csv'

# define model parameters
n_ac_neuron_per_band = 100
can_function = 'exp'
sigma_range = np.array([np.pi/8, np.pi/4])
ipd_space = np.linspace(-np.pi, np.pi, 160)
cfs = np.linspace(150, 850, 15)
fix_weights = True
fix_sigmas = False

# define solver
method = "BH-L-BFGS-B"
# method = "PSO"
# method = "GA"

# read data to fit
ipd_data = np.genfromtxt(_data_file,
                         delimiter=',',
                         skip_header=True)
_idx = np.argsort(ipd_data[:, 1])

# run different cases
cases_to_run = ['original',
                # 'only_damping',
                # 'only_linear',
                # 'control_original',
                # 'control_only_damping',
                # 'control_only_linear'
                ]
# conditions = list(itertools.product(cases_to_run, sigmas))

for case in cases_to_run:
    data = pd.DataFrame()

    itds = ipd_data[_idx, 1] / 1e6  # convert to seconds
    target_response = ipd_data[_idx, 2]
    v1 = np.arange(0, itds.size // 2)
    v2 = np.arange(itds.size // 2, itds.size)
    popt, pcov = curve_fit(func, itds[v1], target_response[v1], maxfev=10000)
    target_response_l = func(itds[v1], *popt)
    popt, pcov = curve_fit(func, itds[v2], target_response[v2], maxfev=10000)
    target_response_r = func(itds[v2], *popt)

    if case == 'original':
        target_response = target_response
        plt.plot(itds, target_response)
    if case == 'only_damping':
        target_response -= np.concatenate((target_response_l, target_response_r))
        target_response += 1 - target_response.max()
        plt.plot(itds, target_response)

    if case == 'only_linear':
        target_response = np.concatenate((target_response_l, target_response_r))
        target_response += 1 - target_response.max()
        plt.plot(itds, target_response)

    if case == 'control_only_damping':
        target_response = -np.concatenate((target_response_l, target_response_r))
        target_response += 1 - target_response.max()
        plt.plot(itds, target_response)
    if case == 'control_only_linear':
        target_response -= np.concatenate((target_response_l, target_response_r))
        target_response *= -1
        target_response += 1 - target_response.max()
        plt.plot(itds, target_response)
    if case == 'control_original':
        target_response *= -1
        target_response += 1 - target_response.max()
        plt.plot(itds, target_response)

    _condition = '{:}_{:}_{:}_{:}_{:}_{:}_cfs_{:}'.format(case,
                                                          method,
                                                          n_ac_neuron_per_band,
                                                          os.path.basename(_data_file).split('.')[0],
                                                          can_function,
                                                          fix_weights,
                                                          cfs.size)

    best_ipds = np.pi/1.0 * (np.random.rand(n_ac_neuron_per_band, cfs.shape[0]) - 0.5) / 0.5
    sigmas = np.random.rand(n_ac_neuron_per_band, cfs.shape[0]) * (sigma_range.max() - sigma_range.min()) + \
             sigma_range.min()
    # #######you can try with different starting populations, below some examples##########
    # best_ipds = np.pi/1.5 + 1*np.pi/8 * np.random.randn(n_ac_neuron_per_band, cfs.shape[0])
    # best_ipds = np.pi/8 * np.ones((n_ac_neuron_per_band, cfs.shape[0]))
    # best_ipds *= 0
    # best_ipds = np.array(([2.53576335,  1.71796843,  1.87374318,  1.91782721,  2.02800527], ) * n_neuro)
    # best_ipds = np.array(([2.53576335,  1.71796843,  1.87374318,  1.91782721,  2.02800527, 1.5, 1.2, 0.9], ) *
    #                      n_neuro)
    # best_ipds = np.array(([np.pi/8,  np.pi/8,  np.pi/8,  np.pi/8,  np.pi/8, np.pi/8, np.pi/8, np.pi/8], )
    #                      * n_neuro)
    # best_ipds = best_ipds + np.pi/12*np.random.randn(n_neuro, cfs.shape[0])

    # fits the model, to visualize the results you can set do_plots to True.
    # The fitting may take a very long time.
    optimal_ipds, optimal_weights, optimal_sigmas = tools.fit_population(cfs=cfs,
                                                                         best_ipds=best_ipds,
                                                                         weights=np.ones(best_ipds.shape),
                                                                         fix_weights=fix_weights,
                                                                         fix_sigmas=fix_sigmas,
                                                                         n_ac_neuron_per_band=n_ac_neuron_per_band,
                                                                         itds=itds,
                                                                         target_response=target_response,
                                                                         method=method,
                                                                         n_to_update_plot=1000,
                                                                         ipd_space=ipd_space,
                                                                         can_function=can_function,
                                                                         sigma=sigmas,
                                                                         do_plots=True,
                                                                         tol=1e-20
                                                                         )
    _data = {''}
    data = pd.concat([data,
                      pd.DataFrame(
                          [{'n_ac_neuron_per_band': n_ac_neuron_per_band,
                            'can_function': can_function,
                            'sigma_range': sigma_range,
                            'sigma': optimal_sigmas,
                            'case': case,
                            'method': method,
                            'cfs': cfs,
                            'best_ipds': optimal_ipds,
                            'weights': optimal_weights,
                            'target_response': target_response,
                            'itds': itds
                            }])],
                     ignore_index=True)
    data.to_pickle(data_path_model + _condition + '_results_model_ipd_domain_variable_sigma_fix_weights.pkl')
    fig_out, _ = tools.plot_ipd_distribution_and_ac_population_response(
        cfs=cfs,
        best_ipds=optimal_ipds,
        weights=optimal_weights,
        itds=itds,
        target_response=target_response,
        can_function=can_function,
        sigma=optimal_sigmas,
        make_symmetric_ipd=False
    )
    fig_out.savefig(figures_path_model + _condition + 'variable_sigma_fix_weights_fitting_damping_ipd_domain.png')
    plt.close(fig=fig_out)
