import matplotlib.pyplot as plt
import pandas as pd

import pysignal_generator.sin_functions as sg
import pysignal_generator.noise_functions as nf
from matplotlib import gridspec
from pysignal_generator.tools import rms
from binaural.correlogram.correlogram import *
import matplotlib
from environment import data_path_model, figures_path_model
import computational_model.modeling_distributions_tools as model_tools

if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')


def fit_axis(fig):
    all_axes = fig.get_axes()
    for ax in all_axes:
        ax.tick_params(axis='x', which='both', top='off')
        ax.tick_params(axis='y', which='both', right='off')
        for sp in ax.spines.values():
            sp.set_visible(False)
        if not ax._subplotspec.is_first_row():
            ax.spines['top'].set_visible(False)
        if not ax._subplotspec.is_last_row():
            ax.set_xticklabels([])
            ax.set_xlabel('')
        else:
            ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(False)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(12)
    fig.tight_layout()


fs = 48000.
max_time_lag = 0.005
f_low = 300
f_high = 700
n_filters = 15
n_repetitions = 20
itds = np.arange(0.0005, 0.0045, 0.0005)
root_path = '/home/jundurraga/pCloudDrive/Documents/Presentations/international_binaural_workshop_2018/figures/'
# model = 'pi_limit'
# model = 'stern_shear'
# model = 'pi_limit_restricted'
# model = 'hancock_delgutte'
ipd_low = 0.24
ipd_high = 0.25
# model = 'stern_shear'
data_path = data_path_model + \
            'original_BH-L-BFGS-B_100_interpolated_fitting_data_meg_exp_True_cfs_15_results_model_ipd_domain.pkl'
with open(data_path, 'rb') as handle:
    results = pd.read_pickle(handle)
for _i, (_ir, _row) in enumerate(results.iterrows()):
    phase = np.unique(_row.sigma * 180 / np.pi)
    cfs = _row.cfs
    type = str(_row.case)
    can_function = str(_row.can_function)
    sigma = _row.sigma
    mean_sigma = np.mean(sigma[:])
    optimal_ipds = _row.best_ipds
    optimal_weights = _row.weights
    if phase == 22.5:
        break
    # itds = _row.itds
optimal_itds = optimal_ipds / (2 * np.pi * cfs)
# generate plot
human_diameter = 19 / 100
# f_out = model_tools.plot_itd_distribution_and_ac_population_response(
#     cfs=cfs,
#     best_itds=optimal_itds,
#     weights=optimal_weights,
#     itds=itds,
#     # target_response=target_response,
#     target_label='MEG-GFP',
#     can_function=can_function,
#     sigma=sigma,
#     head_diameter=human_diameter,
#     freq_band_width=50,
#     make_symmetric_itd=False
# )

models = ['hancock_delgutte', 'pi_limit', 'stern_shear']
for model in models:
    summed_activity = np.empty((n_repetitions, itds.size))
    summed_activity_damping = np.empty((n_repetitions, itds.size))
    rms_activity = np.empty((n_repetitions, itds.size))
    mean_diff_activity = np.empty((n_repetitions, itds.size))
    rms_diff_activity = np.empty((n_repetitions, itds.size))
    for _n in range(n_repetitions):
        fig = plt.figure()
        gs = gridspec.GridSpec(1, 3)
        for _i, itd in enumerate(itds):
            pl = {'itd': itd,
                  'alt_itd': itd,
                  'amplitude': 0.9,
                  'modulation_index': 1.0,
                  'modulation_frequency': 44.871794871794876,
                  'cycle_rate': 16384.0,
                  'fs': fs,
                  'duration': 4.056/4,
                  'itd_mod_rate': 6.4102564102564106,
                  'include_triggers': 0,
                  }
            pulse, time = nf.generate_noise_alt_delay_signal(pl)

            correlogram, weighted_correlogram, lags, cfs = get_correlogram(pulse[:, 0],
                                                                           pulse[:, 1],
                                                                           fs=fs,
                                                                           f_low=350,
                                                                           f_high=750,
                                                                           neural_density_model=model,
                                                                           n_filters=n_filters,
                                                                           max_time_lag=max_time_lag,
                                                                           ipd_low=ipd_low,
                                                                           ipd_high=ipd_high,
                                                                           # cfs=cfs
                                                                           )

            weights_data = np.zeros((lags.size, cfs.size))
            for _l, _lag in enumerate(lags):
                _average, spikes_per_cf = model_tools.ic_response_population_itd(cfs=cfs,
                                                                                 itd=_lag,
                                                                                 best_itds=optimal_itds,
                                                                                 weights=optimal_weights,
                                                                                 can_function=can_function,
                                                                                 sigma=sigma
                                                                                 )
                weights_data[_l, :] = spikes_per_cf

            ax = plt.subplot((gs[0, 0]))
            _correlogram = np.mean(correlogram, axis=1)
            ax.plot(lags, _correlogram / _correlogram.max(), label='{:} us'.format(np.round(itd * 1e6)))
            ax.legend()
            ax = plt.subplot((gs[0, 1]))
            _weighted_correlogram = np.mean(weighted_correlogram, axis=1)
            ax.plot(lags, _weighted_correlogram, label='{:} us'.format(np.round(itd * 1e6)))
            ax.legend()
            ax = plt.subplot((gs[0, 2]))
            damping_model_correlogram = np.mean(correlogram * weights_data, axis=1)
            ax.plot(lags, damping_model_correlogram,
                    label='{:} us'.format(np.round(itd * 1e6)))
            ax.legend()

            across_freq_activity = np.sum(weighted_correlogram, axis=1)
            rms_activity[_n, _i] = rms(across_freq_activity)
            summed_activity[_n, _i] = np.sum(across_freq_activity)
            diff_activity = np.abs(across_freq_activity - np.flip(across_freq_activity, axis=0))
            summed_activity_damping[_n, _i] = np.sum(damping_model_correlogram)
            rms_diff_activity[_n, _i] = rms(diff_activity)
            mean_diff_activity[_n, _i] = np.mean(diff_activity)
            if _n == 0 and _i == 0:
                across_freq_activity_matrix = np.zeros((n_repetitions, len(itds), across_freq_activity.shape[0]))
            across_freq_activity_matrix[_n, _i, :] = across_freq_activity
            print('repetition {:d}, itd: {:.1f}'.format(_n, itd * 1000))
    plt.show()
    inch_cm = 2.54
    fig = plt.figure()
    gs = gridspec.GridSpec(1, 1)
    ax = fig.add_subplot((gs[0, 0:]))
    mean_damping = np.mean(summed_activity_damping, axis=0)
    ax.errorbar(itds * 1000, mean_damping, yerr=np.std(summed_activity_damping, axis=0), fmt='o')
    mean_model = np.mean(summed_activity, axis=0)
    ax.errorbar(itds * 1000, mean_model, yerr=np.std(summed_activity, axis=0), fmt='o')
    ax.set_xlabel("ITD [ms]")
    ax.set_ylabel("Neural activity [au]")
    ax.legend()


    fit_axis(fig)
    fig.set_size_inches(18 / inch_cm, 12 / inch_cm)
    fig.savefig(root_path + 'rms_neural_activity_{:}.png'.format(model))
    # inch_cm = 2.54
    # fig = plt.figure()
    # gs = gridspec.GridSpec(1, 1)
    # ax = fig.add_subplot((gs[0, 0:]))
    # ax.errorbar(itds * 1000, np.mean(rms_activity, axis=0), yerr=np.std(rms_activity, axis=0), fmt='o')
    # ax.set_xlabel("ITD [ms]")
    # ax.set_ylabel("RMS activity [au]")
    # fit_axis(fig)
    # fig.set_size_inches(18 / inch_cm, 12 / inch_cm)
    # fig.savefig(root_path + 'rms_neural_activity_{:}.png'.format(model))
    # plt.close(fig)
    #
    #
    # fig = plt.figure()
    # gs = gridspec.GridSpec(1, 1)
    # ax = fig.add_subplot((gs[0, 0:]))
    # ax.errorbar(itds * 1000, np.mean(mean_activity, axis=0), yerr=np.std(rms_activity, axis=0), fmt='o')
    # ax.set_xlabel("ITD [ms]")
    # ax.set_ylabel("Average activity [au]")
    # fit_axis(fig)
    # fig.set_size_inches(18 / inch_cm, 12 / inch_cm)
    # fig.savefig(root_path + 'ave_neural_activity_{:}.png'.format(model))
    # plt.close(fig)
    itd_m = np.ones(across_freq_activity_matrix.shape) * itds[None, :, None]
    rep_m = np.ones(across_freq_activity_matrix.shape) * np.arange(n_repetitions)[:, None, None]
    lags_m = np.ones(across_freq_activity_matrix.shape) * lags[None, None, :]

    itd_col = np.reshape(itd_m, [-1, 1, 1]).squeeze()
    rep_col = np.reshape(rep_m, [-1, 1, 1]).squeeze()
    lag_col = np.reshape(lags_m, [-1, 1, 1]).squeeze()
    activation_col = np.reshape(across_freq_activity_matrix, [-1, 1, 1]).squeeze()
    df = pd.DataFrame()
    df['lag'] = lag_col
    df['repetition'] = rep_col
    df['itd'] = itd_col
    df['activation'] = activation_col
    df.to_csv(
        '/home/jundurraga/Documents/source_code/meg_eeg_behavioural/computational_model/data/{:}_model.csv'.format(model),
        index=False)

    itd_m = np.ones(summed_activity.shape) * itds[None, :]
    rep_m = np.ones(summed_activity.shape) * np.arange(n_repetitions)[:, None]
    itd_col = np.reshape(itd_m, [-1, 1, 1]).squeeze()
    rep_col = np.reshape(rep_m, [-1, 1, 1]).squeeze()
    total_activation_col = np.reshape(summed_activity, [-1, 1, 1]).squeeze()
    df = pd.DataFrame()
    df['repetition'] = rep_col
    df['itd'] = itd_col
    df['total_activation'] = total_activation_col
    df.to_csv(
        '/home/jundurraga/Documents/source_code/meg_eeg_behavioural/computational_model/data/{:}_model_total_activation.csv'.format(model),
        index=False)

    inch_cm = 2.54
    fig = plt.figure()
    gs = gridspec.GridSpec(1, 1)
    ax = fig.add_subplot((gs[0, 0:]))
    ax.errorbar(itds * 1000, np.mean(rms_diff_activity, axis=0), yerr=np.std(rms_diff_activity, axis=0), fmt='o')
    ax.set_xlabel("ITD [ms]")
    ax.set_ylabel("RMS activity [au]")
    fit_axis(fig)
    fig.set_size_inches(18 / inch_cm, 12 / inch_cm)
    fig.savefig(root_path + 'rms_diff_neural_activity_{:}.png'.format(model))
    plt.close(fig)

    fig = plt.figure()
    gs = gridspec.GridSpec(1, 1)
    ax = fig.add_subplot((gs[0, 0:]))
    ax.errorbar(itds * 1000, np.mean(mean_diff_activity, axis=0), yerr=np.std(mean_diff_activity, axis=0), fmt='o')
    ax.set_xlabel("ITD [ms]")
    ax.set_ylabel("Average activity [au]")
    fit_axis(fig)
    fig.set_size_inches(18 / inch_cm, 12 / inch_cm)
    fig.savefig(root_path + 'ave_diff_neural_activity_{:}.png'.format(model))
    plt.close(fig)
