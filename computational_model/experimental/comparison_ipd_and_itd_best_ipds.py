import numpy as np
from environment import data_path_model, figures_path_model, data_path_meg
import scipy.spatial as sp
import scipy
import matplotlib
import matplotlib.pyplot as plt
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')

ipd_data = np.load(data_path_model + 'numpy_optimal_ipds_original.npy')
ipd_from_itd_data = np.load(data_path_model + 'numpy_itd_to_optimal_ipds_original.npy')
fig, ax = plt.subplots(1, 1)
c = ax.pcolor(ipd_data / np.max(np.abs(ipd_data)) - ipd_from_itd_data/np.max(np.abs(ipd_from_itd_data)))
fig.colorbar(c, ax=ax)
