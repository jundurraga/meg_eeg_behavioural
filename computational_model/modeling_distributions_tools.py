import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
from pyswarms.single import GeneralOptimizerPSO
from pyswarms.utils.plotters import plot_cost_history, plot_contour
from pyswarms.utils.plotters.formatters import Mesher
from scipy.optimize import basinhopping, brute, fmin, minimize, differential_evolution
from deap import base, creator, tools, algorithms
import multiprocessing
import random
from matplotlib import rc
from scipy import interpolate
from sklearn.mixture import GaussianMixture
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')
rc('text', usetex=True)
global fig_out_fitting, iteration_counter
fig_out_fitting = None
iteration_counter = 0
plt.ion()


def head_itd(head_diameter: float = 0.1,
             sound_speed: float = 344.0,
             angle: float = 0.0):
    itd = (angle + np.sin(angle)) * head_diameter * 0.5 / sound_speed
    return itd


def max_ecological_itd(head_diameter: float = 0.1,
                       sound_speed: float = 344.0):
    _max_itd = head_itd(head_diameter=head_diameter,
                        sound_speed=sound_speed,
                        angle=np.pi/2)
    return _max_itd


def max_ipd(max_itd: float = None,
            cf: float = None,
            restricted=True):
    """
    This function determines ecologically valid IPD for a central frequency
    :param max_itd: maximum itd determined by head size
    :param cf: central frequency in Hz
    :param restricted: if true, max itd restricted to pi
    :return: max ipd for input frequency
    """
    if restricted:
        _max_ipd = np.pi * np.minimum(0.5/cf, max_itd) / (0.5 / cf)
    else:
        _max_ipd = np.pi * max_itd / (0.5 / cf)
    return _max_ipd


def pi_limit(cf: float = None,
             period_n: float = 1):
    """
    This function determines ecologically valid IPD for a central frequency
    :param cf: central frequency in Hz
    :param period_n: float indicating  fraction of the period to return
    :return: pi limit in seconds relative to the center frequency
    """
    _pi_limit = period_n / (2 * cf)
    return _pi_limit


def g_p(ipd: float = None,
        best_ipd: float = None,
        r_max: float = 100,
        can_function: str = 'exp',
        sigma: float = np.pi/8):
    """
    This function computes the spike rate of a peaker IC neuron at best_ipd
    :param ipd: ipd in rads
    :param best_ipd: best ipd in rads
    :param r_max: maximum spike rate per second
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :return: spike rate at given ipd
    """
    out = None
    if can_function == "cos":
        out = r_max * (0.5 + 0.5 * np.cos(ipd - best_ipd)) ** 4.0
    if can_function == "exp":
        _ipd = np.mod(ipd - best_ipd - np.pi, 2*np.pi) - np.pi
        out = r_max * np.exp(-0.5 * (_ipd / sigma) ** 2.0)
    if can_function == "exp2":
        out = r_max * np.exp(-0.5 * np.abs(np.sin(0.5*(ipd - best_ipd)) / sigma) ** 2.0)
    if can_function == "hancock_delgutte":
        w = 0.19
        mu_1 = 0.23
        sigma_1 = 0.04
        mu_2 = 0.16
        sigma_2 = 0.19
        original_bd = (mu_1 + mu_2) / 2
        best_phase = np.abs(best_ipd / (2 * np.pi))
        delta_mean = (best_phase - original_bd)
        mu_1 = mu_1 + delta_mean
        mu_2 = mu_2 + delta_mean
        phase = np.abs(ipd / (2 * np.pi))

        out = (w / (sigma_1 * np.sqrt(2 * np.pi)) * np.exp(-(phase - mu_1) ** 2.0 / (2 * sigma_1 ** 2)) +
               (1 - w) / (sigma_2 * np.sqrt(2 * np.pi)) * np.exp(
                    -(phase - mu_2) ** 2.0 / (2 * sigma_2 ** 2))
               )
    return out


def g_itd(itd: float = None,
          best_itd: float = None,
          r_max: float = 100,
          can_function: str = 'exp',
          sigma: float = np.pi/8,
          cf: float = 500):
    """
    This function computes the spike rate of a peaker IC neuron at best_ipd
    :param itd: itd in rads
    :param best_itd: best itd in rads
    :param r_max: maximum spike rate per second
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :param cf: central frequency
    :return: spike rate at given ipd
    """
    out = None
    sigma = sigma / (2 * np.pi * cf)
    if can_function == "cos":
        out = r_max * (0.5 + 0.5 * np.cos((itd - best_itd) * (2 * np.pi * cf))) ** 4.0
    if can_function == "exp":
        _ipd = np.mod(itd - best_itd - np.pi, 2 * np.pi) - np.pi
        out = r_max * np.exp(-0.5 * (_ipd / sigma) ** 2.0)
    if can_function == "exp2":
        out = r_max * np.exp(-0.5 * np.abs(np.sin(0.5*(itd - best_itd)) / sigma) ** 2.0)
    return out


def g_t(ipd: float = None,
        best_ipd: np.array = None,
        r_max: float = 100):
    """
    This function computes the spike rate of a trough IPD neuron at best_ipd
    :param ipd: ipd in rads
    :param best_ipd: best ipd in rads
    :param r_max: maximum spike rate per second
    :return: spike rate at given ipd
    """
    out = r_max * (1 - (0.5 + 0.5 * np.cos(ipd - best_ipd))) ** 4.0
    return out


def g_p_t(ipd: float = None,
          best_ipd: np.array = None,
          r_max: float = 100,
          delta: float = np.pi/4):
    """
    This function computes the spike rate of a peaker IC neuron at best_ipd
    :param ipd: ipd in rads
    :param best_ipd: best ipd in rads
    :param r_max: maximum spike rate per second
    :param delta: float indicating the ipd separation between peaker and trougher neuron
    :return: spike rate at given ipd
    """
    out = r_max * (
            (0.5 + 0.5 * np.cos(ipd - best_ipd - delta)) ** 4.0 +
            (0.5 + 0.5 * np.cos(ipd - best_ipd + delta)) ** 4.0)

    return out


def itd_to_ipd(cf: float = None,
               itd: float = None):
    """
    This function converts itd to relative IPD at characteristic frequency
    :param cf: characteristic frequency in Hz
    :param itd: interaural time difference in seconds
    :return: relative ipd
    """
    _ipd = (np.pi * itd / (0.5 / cf) + np.pi) % (2 * np.pi) - np.pi
    return _ipd


def g_p_ipd(cf: float = 500.,
            itd: float = 0.,
            best_ipd: float = 0.,
            r_max: float = 100,
            can_function='exp',
            sigma: np.array = np.array([np.pi/8])):
    """
    This function computes the spike rate of trough IPD neuron  for  a given itd for a neuron having a given
    characteristic frequency
    :param cf: characteristic frequency in Hz
    :param itd: interaural time difference in seconds
    :param best_ipd: best ipd of that neuron
    :param r_max: maximum spike rate of the neuron
    :param can_function: string indicating the canonical function defining the spike rate
    :param sigma: float indicating the standard deviation of an exponential canonical function
    :return: spike rate of neuron to a given input itd
    """
    _ipd = itd_to_ipd(cf=cf, itd=itd)
    out = g_p(_ipd, best_ipd,
              r_max=r_max,
              can_function=can_function,
              sigma=sigma)
    return out, _ipd


def g_p_itd(cf: float = 500.,
            itd: float = 0.,
            best_itd: float = 0.,
            r_max: float = 100,
            can_function: str = 'exp',
            sigma: np.array = np.array([np.pi/8])):
    """
    This function computes the spike rate of trough IPD neuron  for  a given itd for a neuron having a given
    characteristic frequency
    :param cf: characteristic frequency in Hz
    :param itd: interaural time difference in seconds
    :param best_itd: best itd of that neuron
    :param r_max: maximum spike rate of the neuron
    :param can_function: string indicating the canonical function defining the spike rate
    :param sigma: float indicating the standard deviation of an exponential canonical function
    :return: spike rate of neuron to a given input itd
    """
    out = g_itd(itd, best_itd, r_max=r_max, can_function=can_function, sigma=sigma, cf=cf)
    return out, itd


def g_t_ipd(cf: float = 500.,
            itd: float = 0.,
            best_ipd: float = 0.,
            r_max: float = 100):
    """
    This function computes the spike rate of peaker neuron for  a given itd for a neuron having a given characteristic
    frequency
    :param cf: characteristic frequency in Hz
    :param itd: interaural time difference in seconds
    :param best_ipd: best ipd of that neuron
    :param r_max: maximum spike rate of the neuron
    :return: spike rate of neuron to a given input itd
    """

    _ipd = itd_to_ipd(cf=cf, itd=itd)
    out = g_t(_ipd, best_ipd, r_max=r_max)
    return out, _ipd


def g_p_t_ipd(cf: float = 500.,
              itd: float = 0.,
              best_ipd: float = 0.,
              r_max: float = 100,
              delta: float = np.pi/4):
    """
    This function computes the spike rate of peaker and a trough assuming they are separated by pi/2. This is calculated
    for a given itd for neurons having a given characteristic frequency.
    :param cf: characteristic frequency in Hz
    :param itd: interaural time difference in seconds
    :param best_ipd: best ipd of that neuron
    :param r_max: maximum spike rate of the neuron
    :param delta: float indicating the ipd separation between peaker and trougher neuron
    :return: spike rate of neuron to a given input itd
    """

    _ipd = itd_to_ipd(cf=cf, itd=itd)
    out = g_p_t(_ipd, best_ipd, r_max=r_max, delta=delta)
    return out, _ipd


def ic_response(
        cfs: np.array = None,
        itd: float = None,
        best_ipds: np.array = None,
        ac_ipds: np.array = None):
    """
    This function will compute IC neuron (ipd tuned) responses
    :param cfs: central frequencies
    :param itd: input interaural time differences
    :param best_ipds: best ipds of neurons
    :param ac_ipds: characteristic ipds of ic neurons
    :return: ic spike rate as function of ipd
    """
    mean_spike_rate = np.zeros((ac_ipds.shape[0], cfs.shape[0]))
    _best_ipds = best_ipds.reshape(-1, cfs.shape[0])
    for _idx_cf, _cf in enumerate(cfs):
        for _best_ipd in _best_ipds[:, _idx_cf]:
            spike_rate, _ipd = g_p_ipd(cf=_cf, itd=itd, best_ipd=_best_ipd)
            _idx_ipd = np.argmin((np.abs(ac_ipds - _best_ipd)))
            mean_spike_rate[_idx_ipd, _idx_cf] += spike_rate
    mean_spike_rate = np.sum(mean_spike_rate, axis=1)
    return mean_spike_rate


def ic_response_population(
        cfs: np.array = None,
        itd: float = None,
        best_ipds: np.array = None,
        weights: np.array = None,
        can_function='exp',
        sigma: float = np.pi/8) -> np.array:
    """
    compute IC neuron population responses (averaging activity from all neurons)
    :param cfs: central frequencies
    :param itd: input interaural time differences
    :param best_ipds: best ipds of neurons
    :param weights: the weights of individual neurons
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :return: ic spike rate as function of ipd
    """
    _best_ipds = best_ipds.reshape(-1, cfs.shape[0])
    _weights = weights.reshape(-1, cfs.shape[0])
    _sigma = sigma.reshape(-1, cfs.shape[0])
    spikes = np.zeros(_best_ipds.shape)
    if can_function == 'hancock_delgutte':
        sigma_cf = 0.51
        mean_cf = 6.5
        f_bf = (1 / (cfs * sigma_cf * np.sqrt(2 * np.pi)) *
                np.exp(-(np.log(cfs) - mean_cf) ** 2.0 / (2 * sigma_cf ** 2.0)))
        _weights = np.ones(spikes.shape) * f_bf

    for _idx_cf, _cf in enumerate(cfs):
        spike_rate, _ipd = g_p_ipd(cf=_cf,
                                   itd=itd,
                                   best_ipd=_best_ipds[:, _idx_cf],
                                   can_function=can_function,
                                   sigma=_sigma[:, _idx_cf])

        spikes[:, _idx_cf] = spike_rate * _weights[:, _idx_cf]
    print('N active neurons {:}'.format(np.nansum(_weights.flatten())))
    mean_spike_rate = np.nansum(np.sum(spikes, axis=1))
    return mean_spike_rate, _weights


def ic_response_population_itd(
        cfs: np.array = None,
        itd: float = None,
        best_itds: np.array = None,
        weights: np.array = None,
        can_function='exp',
        sigma: float = np.pi/8) -> np.array:
    """
    compute IC neuron population responses (averaging activity from all neurons)
    :param cfs: central frequencies
    :param itd: input interaural time differences
    :param best_itds: best itds of neurons
    :param weights: the weights of individual neurons
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :return: ic spike rate as function of ipd
    """
    _best_itds = best_itds.reshape(-1, cfs.shape[0])
    _weights = weights.reshape(-1, cfs.shape[0])
    _sigma = sigma.reshape(-1, cfs.shape[0])
    spikes = np.zeros(_best_itds.shape)
    for _idx_cf, _cf in enumerate(cfs):
        spike_rate, _ipd = g_p_itd(cf=_cf,
                                   itd=itd,
                                   best_itd=_best_itds[:, _idx_cf],
                                   can_function=can_function,
                                   sigma=_sigma[:, _idx_cf])
        spikes[:, _idx_cf] = spike_rate * _weights[:, _idx_cf]
    print('N active neurons {:}'.format(np.nansum(_weights.flatten())))
    mean_spike_rate = np.nansum(np.sum(spikes, axis=1))
    spikes_per_cf = np.sum(spikes, axis=0)
    return mean_spike_rate, spikes_per_cf


def ic_population_response_ipd(cfs: np.array = None,
                               best_ipds: np.array = None,
                               weights: np.array = None,
                               itds: np.array = None,
                               normalize: bool = True,
                               log: bool = False,
                               make_symmetric_ipd: bool = True,
                               can_function: str = 'exp',
                               sigma: np.array = np.array([np.pi/8])) -> np.array:
    """
    This function will calculate the average activity across (all neurons)
    :param cfs: central frequencies
    :param best_ipds: best ipds of neurons
    :param weights: the weights of individual neurons
    :param itds: input interaural time differences
    :param normalize: population response will be normalized if this is true
    :param log: population response will be transformed to log scale if this is true
    :param make_symmetric_ipd: if true, best_ipds are mirrored to generate full symmetric ipd space
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :return: average population response (across all neurons)
    """
    _ic_population_response = np.zeros(itds.shape[0])
    if weights is None:
        weights = np.ones(best_ipds.shape)

    if isinstance(sigma, float):
        sigma = np.ones(best_ipds.shape) * sigma

    if make_symmetric_ipd:
        sym_best_ipds = np.concatenate((best_ipds, -best_ipds))
        sym_weights = np.concatenate((weights, weights))
        sym_sigmas = np.concatenate((sigma, sigma))
    else:
        sym_best_ipds = best_ipds
        sym_weights = weights
        sym_sigmas = sigma

    for _i, _itd in enumerate(itds):
        _ic_population_response[_i], sym_weights = ic_response_population(cfs=cfs,
                                                                          itd=_itd,
                                                                          best_ipds=sym_best_ipds,
                                                                          weights=sym_weights,
                                                                          can_function=can_function,
                                                                          sigma=sym_sigmas
                                                                          )
    if normalize:
        _max = np.max(_ic_population_response)
        _ic_population_response /= _max
    if log:
        _ic_population_response = np.log_p(_ic_population_response)
    return _ic_population_response


def ic_population_response_itd(cfs: np.array = None,
                               best_itds: np.array = None,
                               weights: np.array = None,
                               itds: np.array = None,
                               normalize: bool = True,
                               log: bool = False,
                               make_symmetric_itd: bool = True,
                               can_function: str = 'exp',
                               sigma: np.array = np.array([np.pi/8])):
    """
    This function will calculate the average activity across (all neurons)
    :param cfs: central frequencies
    :param best_itds: best itds of neurons
    :param weights: the weights of individual neurons
    :param itds: input interaural time differences
    :param normalize: population response will be normalized if this is true
    :param log: population response will be transformed to log scale if this is true
    :param make_symmetric_itd: if true, best_itds are mirrored to generate full symmetric itd space
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :return: average population response (across all neurons)
    """
    _ic_population_response = np.zeros(itds.shape[0])
    if weights is None:
        weights = np.ones(best_itds.shape)

    if isinstance(sigma, float):
        sigma = np.ones(best_itds.shape) * sigma

    if make_symmetric_itd:
        sym_best_itds = np.concatenate((best_itds, -best_itds))
        sym_weights = np.concatenate((weights, weights))
        sym_sigmas = np.concatenate((sigma, sigma))
    else:
        sym_best_itds = best_itds
        sym_weights = weights
        sym_sigmas = sigma

    for _i, _itd in enumerate(itds):
        _ic_population_response[_i], _ = ic_response_population_itd(cfs=cfs,
                                                                    itd=_itd,
                                                                    best_itds=sym_best_itds,
                                                                    weights=sym_weights,
                                                                    can_function=can_function,
                                                                    sigma=sym_sigmas
                                                                    )
    if normalize:
        _max = np.max(_ic_population_response)
        _ic_population_response /= _max
    if log:
        _ic_population_response = np.log_p(_ic_population_response)
    return _ic_population_response


def population_spike_rate(ipd: np.array = None,
                          best_ipds: np.array = None,
                          can_function: str = 'exp',
                          sigma: bool = np.pi/8):
    _sr = np.zeros(ipd.shape)
    for _ipd in best_ipds:
        _sr += g_p(ipd=ipd, best_ipd=_ipd, can_function=can_function, sigma=sigma)
    return _sr / ipd.shape[0]


def error_measure(fitted: np.array = None,
                  cfs: np.array = None,
                  target_response: np.array = None,
                  itds: np.array = None,
                  do_plots=True,
                  caller=None,
                  n_to_update_plot: int = 200,
                  title: str = '',
                  can_function: str = 'exp',
                  sigma: float = np.pi/8,
                  weights: np.array = None,
                  fix_weights: bool = True,
                  fix_sigmas: bool = True,
                  make_symmetric_ipd: bool = True
                  ):
    """
    This function measures the error between the population and target AC response for the current AC best_ipds
    :param fitted: fitted ipd values
    :param cfs: numpy array with central frequencies to be used
    :param target_response: numpy array with target population response as a function of the ITD
    :param itds: numpy array indicating the ITDs corresponding to the target_response
    :param do_plots: bool, if true plots will be generated
    :param caller: function handler
    :param n_to_update_plot: int indicating the number of iterations to refresh plots
    :param title: str indicating the window title
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :param weights: numpy array with individual neural weights (between 0 and 1)
    :param fix_weights: bool indicating if weights were fitted or not
    :param fix_sigmas: bool indicating if sigma were fitted or not
    :param make_symmetric_ipd: if true, best_ipds are assumed to represent one hemisphere, therefore the used best_ipd
    array will be make symmetric.
    :return: score (error) between target and estimated population response
    """
    if isinstance(fitted, list):
        fitted = np.array(fitted)
    if isinstance(sigma, float):
        sigma = np.ones(fitted.shape) * sigma
    if fix_weights and fix_sigmas:
        sym_best_ipds = fitted.flatten()
        sym_weights = weights.flatten()
        sym_sigmas = sigma.flatten()
    elif fix_weights and not fix_sigmas:
        sym_best_ipds = fitted.flatten()[0: fitted.size // 2]
        sym_weights = weights.flatten()
        sym_sigmas = fitted.flatten()[fitted.size // 2::]
    elif not fix_weights and fix_sigmas:
        sym_best_ipds = fitted.flatten()[0: fitted.size // 2]
        sym_weights = fitted.flatten()[fitted.size // 2::]
        sym_sigmas = sigma.flatten()
    elif not fix_weights and not fix_sigmas:
        sym_best_ipds = fitted.flatten()[0: fitted.size // 3]
        sym_weights = fitted.flatten()[fitted.size // 3: (2 * fitted.size) // 3]
        sym_sigmas = fitted.flatten()[(2 * fitted.size) // 3::]

    pop_response = ic_population_response_ipd(cfs=cfs,
                                              itds=itds,
                                              best_ipds=sym_best_ipds,
                                              weights=sym_weights,
                                              normalize=False,
                                              log=False,
                                              can_function=can_function,
                                              make_symmetric_ipd=make_symmetric_ipd,
                                              sigma=sym_sigmas)

    _max_pop = np.max(pop_response)
    pop_response /= _max_pop
    _max = np.max(target_response)
    target_response /= _max
    _itd_penalization = 1
    out = np.sum(np.square(np.diff((pop_response - target_response) * _itd_penalization)))
    global iteration_counter
    global fig_out_fitting
    _best_score = out

    if do_plots and np.mod(iteration_counter, n_to_update_plot) == 0:
        if caller is not None and len(caller.cost_history) > 0:
            _best_score = np.min(caller.cost_history)
            sym_best_ipds = caller.swarm.pbest_pos[caller.swarm.pbest_cost.argmin()].flatten()
            sym_best_ipds = np.concatenate((sym_best_ipds.flatten(), -sym_best_ipds.flatten()))
        _best_ipds = sym_best_ipds.reshape(-1, cfs.shape[0])
        _weights = sym_weights.reshape(-1, cfs.shape[0])
        _sigmas = sym_sigmas.reshape(-1, cfs.shape[0])
        fig_out, _ = plot_ipd_distribution_and_ac_population_response(
            cfs=cfs,
            best_ipds=_best_ipds,
            weights=_weights,
            fig_out=fig_out_fitting,
            itds=itds,
            target_response=target_response,
            can_function=can_function,
            sigma=_sigmas
        )
        fig_out.canvas.setWindowTitle(title)
        fig_out.suptitle("N: {:}, Score {:}".format(iteration_counter, _best_score))
        plt.pause(0.0001)
        fig_out_fitting = fig_out
    iteration_counter += 1
    print("N: {:}, Score {:}".format(iteration_counter, _best_score))
    return out


def error_measure_width(fitted: np.array = None,
                        cfs: np.array = None,
                        target_response: np.array = None,
                        itds: np.array = None,
                        do_plots=True,
                        caller=None,
                        n_to_update_plot: int = 200,
                        title: str = '',
                        can_function: str = 'exp',
                        sigma: float = np.pi/8,
                        weights: np.array = None,
                        fix_weights: bool = False,
                        make_symmetric_ipd: bool = True
                        ):
    """
    This function measures the error between the population and target AC response for the current AC best_ipds
    :param fitted: fitted ipd values
    :param cfs: numpy array with central frequencies to be used
    :param target_response: numpy array with target population response as a function of the ITD
    :param itds: numpy array indicating the ITDs corresponding to the target_response
    :param do_plots: bool, if true plots will be generated
    :param caller: function handler
    :param n_to_update_plot: int indicating the number of iterations to refresh plots
    :param title: str indicating the window title
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :param weights: numpy array with individual neural weights (between 0 and 1)
    :param fix_weights: bool indicating if weights were fitted or not
    :param make_symmetric_ipd: if true, best_ipds are assumed to represent one hemisphere, therefore the used best_ipd
    array will be make symmetric.
    :return: score (error) between target and estimated population response
    """
    if isinstance(fitted, list):
        fitted = np.array(fitted)
        # sym_best_ipds = np.concatenate((best_ipds.flatten(), -best_ipds.flatten()))
        sym_weights = weights.flatten()
        sym_best_ipds = fitted.flatten()[0: fitted.size // 2]
        sym_best_widths = fitted.flatten()[fitted.size // 2::]
    pop_response = ic_population_response_ipd(cfs=cfs,
                                              itds=itds,
                                              best_ipds=sym_best_ipds,
                                              weights=sym_weights,
                                              sigma=sym_best_widths,
                                              normalize=False,
                                              log=False,
                                              can_function=can_function,
                                              make_symmetric_ipd=make_symmetric_ipd
                                              )

    _max_pop = np.max(pop_response)
    pop_response /= _max_pop
    _max = np.max(target_response)
    target_response /= _max
    _itd_penalization = 1
    out = np.sum(np.square(np.diff((pop_response - target_response) * _itd_penalization)))
    global iteration_counter
    global fig_out_fitting
    _best_score = out

    if do_plots and np.mod(iteration_counter, n_to_update_plot) == 0:
        if caller is not None and len(caller.cost_history) > 0:
            _best_score = np.min(caller.cost_history)
            sym_best_ipds = caller.swarm.pbest_pos[caller.swarm.pbest_cost.argmin()].flatten()
            sym_best_ipds = np.concatenate((sym_best_ipds.flatten(), -sym_best_ipds.flatten()))
        _best_ipds = sym_best_ipds.reshape(-1, cfs.shape[0])
        _weights = sym_weights.reshape(-1, cfs.shape[0])
        fig_out = plot_ipd_distribution_and_ac_population_response(
            cfs=cfs,
            best_ipds=_best_ipds,
            weights=_weights,
            fig_out=fig_out_fitting,
            itds=itds,
            target_response=target_response,
            can_function=can_function,
            sigma=sigma
        )
        fig_out.canvas.setWindowTitle(title)
        fig_out.suptitle("N: {:}, Score {:}".format(iteration_counter, _best_score))
        plt.pause(0.0001)
        fig_out_fitting = fig_out
    iteration_counter += 1
    print("N: {:}, Score {:}".format(iteration_counter, _best_score))
    return out


def error_measure_itd(fitted: np.array = None,
                      cfs: np.array = None,
                      target_response: np.array = None,
                      itds: np.array = None,
                      do_plots=True,
                      caller=None,
                      n_to_update_plot: int = 200,
                      title: str = '',
                      can_function: str = 'exp',
                      sigma: float = np.pi/8,
                      weights: np.array = None,
                      fix_weights: bool = True,
                      fix_sigmas: bool = True,
                      make_symmetric_itd: bool = True,
                      ):
    """
    This function measures the error between the population and target AC response for the current AC best_itds
    :param fitted: fitted itd values
    :param cfs: numpy array with central frequencies to be used
    :param target_response: numpy array with target population response as a function of the ITD
    :param itds: numpy array indicating the ITDs corresponding to the target_response
    :param do_plots: bool, if true plots will be generated
    :param caller: function handler
    :param n_to_update_plot: int indicating the number of iterations to refresh plots
    :param title: str indicating the window title
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :param weights: numpy array with individual neural weights (between 0 and 1)
    :param fix_sigmas: bool indicating if sigma were fitted or not
    :param fix_weights: bool indicating if weights were fitted or not
    :param make_symmetric_itd: if true, best_itds are assumed to represent one hemisphere, therefore the used best_itd
    array will be make symmetric.
    :return: score (error) between target and estimated population response
    """
    if isinstance(fitted, list):
        fitted = np.array(fitted)
    if isinstance(sigma, float):
        sigma = np.ones(fitted.shape) * sigma
    if fix_weights and fix_sigmas:
        sym_best_itds = fitted.flatten()
        sym_weights = weights.flatten()
        sym_sigmas = sigma.flatten()
    elif fix_weights and not fix_sigmas:
        sym_best_itds = fitted.flatten()[0: fitted.size // 2]
        sym_weights = weights.flatten()
        sym_sigmas = fitted.flatten()[fitted.size // 2::]
    elif not fix_weights and fix_sigmas:
        sym_best_itds = fitted.flatten()[0: fitted.size // 2]
        sym_weights = fitted.flatten()[fitted.size // 2::]
        sym_sigmas = sigma.flatten()
    elif not fix_weights and not fix_sigmas:
        sym_best_itds = fitted.flatten()[0: fitted.size // 3]
        sym_weights = fitted.flatten()[fitted.size // 3: (2 * fitted.size) // 3]
        sym_sigmas = fitted.flatten()[(2 * fitted.size) // 3::]
    ####

    pop_response = ic_population_response_itd(cfs=cfs,
                                              itds=itds,
                                              best_itds=sym_best_itds,
                                              weights=sym_weights,
                                              normalize=False,
                                              log=False,
                                              can_function=can_function,
                                              make_symmetric_itd=make_symmetric_itd,
                                              sigma=sym_sigmas)

    _max_pop = np.max(pop_response)
    pop_response /= _max_pop
    _max = np.max(target_response)
    target_response /= _max
    _itd_penalization = 1
    out = np.sum(np.square(np.diff((pop_response - target_response) * _itd_penalization)))
    global iteration_counter
    global fig_out_fitting
    _best_score = out

    if do_plots and np.mod(iteration_counter, n_to_update_plot) == 0:
        if caller is not None and len(caller.cost_history) > 0:
            _best_score = np.min(caller.cost_history)
        _best_itds = sym_best_itds.reshape(-1, cfs.shape[0])
        _weights = sym_weights.reshape(-1, cfs.shape[0])
        _sigmas = sym_sigmas.reshape(-1, cfs.shape[0])
        fig_out = plot_itd_distribution_and_ac_population_response(
            cfs=cfs,
            best_itds=_best_itds,
            weights=_weights,
            fig_out=fig_out_fitting,
            itds=itds,
            target_response=target_response,
            can_function=can_function,
            sigma=_sigmas
        )
        fig_out.canvas.setWindowTitle(title)
        fig_out.suptitle("N: {:}, Score {:}".format(iteration_counter, _best_score))
        plt.pause(0.0001)
        fig_out_fitting = fig_out
    iteration_counter += 1
    print("N: {:}, Score {:}".format(iteration_counter, _best_score))
    return out


def error_measure_itd_to_ipd(fitted: np.array = None,
                             cfs: np.array = None,
                             target_response: np.array = None,
                             itds: np.array = None,
                             do_plots=True,
                             caller=None,
                             n_to_update_plot: int = 200,
                             title: str = '',
                             can_function: str = 'exp',
                             sigma: float = np.pi/8,
                             weights: np.array = None,
                             fix_weights: bool = True,
                             fix_sigmas: bool = True,
                             make_symmetric_itd: bool = True,
                             ):
    """
    This function measures the error between the population and target AC response for the current AC best_itds
    :param fitted: fitted itd values
    :param cfs: numpy array with central frequencies to be used
    :param target_response: numpy array with target population response as a function of the ITD
    :param itds: numpy array indicating the ITDs corresponding to the target_response
    :param do_plots: bool, if true plots will be generated
    :param caller: function handler
    :param n_to_update_plot: int indicating the number of iterations to refresh plots
    :param title: str indicating the window title
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :param weights: numpy array with individual neural weights (between 0 and 1)
    :param fix_sigmas: bool indicating if sigma were fitted or not
    :param fix_weights: bool indicating if weights were fitted or not
    :param make_symmetric_itd: if true, best_itds are assumed to represent one hemisphere, therefore the used best_itd
    array will be make symmetric.
    :return: score (error) between target and estimated population response
    """
    if isinstance(fitted, list):
        fitted = np.array(fitted)
    if isinstance(sigma, float):
        sigma = np.ones(fitted.shape) * sigma
    if fix_weights and fix_sigmas:
        sym_best_itds = fitted.flatten()
        sym_weights = weights.flatten()
        sym_sigmas = sigma.flatten()
    elif fix_weights and not fix_sigmas:
        sym_best_itds = fitted.flatten()[0: fitted.size // 2]
        sym_weights = weights.flatten()
        sym_sigmas = fitted.flatten()[fitted.size // 2::]
    elif not fix_weights and fix_sigmas:
        sym_best_itds = fitted.flatten()[0: fitted.size // 2]
        sym_weights = fitted.flatten()[fitted.size // 2::]
        sym_sigmas = sigma.flatten()
    elif not fix_weights and not fix_sigmas:
        sym_best_itds = fitted.flatten()[0: fitted.size // 3]
        sym_weights = fitted.flatten()[fitted.size // 3: (2 * fitted.size) // 3]
        sym_sigmas = fitted.flatten()[(2 * fitted.size) // 3::]
    ####
    _cfs = (np.ones((sym_best_itds.shape[0] // cfs.shape[0], cfs.shape[0])) * cfs).flatten()
    sym_best_ipds = itd_to_ipd(cf=_cfs, itd=sym_best_itds)
    pop_response = ic_population_response_ipd(cfs=cfs,
                                              itds=itds,
                                              best_ipds=sym_best_ipds,
                                              weights=sym_weights,
                                              normalize=False,
                                              log=False,
                                              can_function=can_function,
                                              make_symmetric_ipd=make_symmetric_itd,
                                              sigma=sym_sigmas)
    _max_pop = np.max(pop_response)
    pop_response /= _max_pop
    _max = np.max(target_response)
    target_response /= _max
    _itd_penalization = 1
    out = np.sum(np.square(np.diff((pop_response - target_response) * _itd_penalization)))
    global iteration_counter
    global fig_out_fitting
    _best_score = out

    if do_plots and np.mod(iteration_counter, n_to_update_plot) == 0:
        if caller is not None and len(caller.cost_history) > 0:
            _best_score = np.min(caller.cost_history)
            sym_best_ipds = caller.swarm.pbest_pos[caller.swarm.pbest_cost.argmin()].flatten()
            sym_best_ipds = np.concatenate((sym_best_ipds.flatten(), -sym_best_ipds.flatten()))
        _best_ipds = sym_best_ipds.reshape(-1, cfs.shape[0])
        _weights = sym_weights.reshape(-1, cfs.shape[0])
        _sigmas = sym_sigmas.reshape(-1, cfs.shape[0])
        fig_out, _ = plot_ipd_distribution_and_ac_population_response(
            cfs=cfs,
            best_ipds=_best_ipds,
            weights=_weights,
            fig_out=fig_out_fitting,
            itds=itds,
            target_response=target_response,
            can_function=can_function,
            sigma=_sigmas
        )
        fig_out.canvas.setWindowTitle(title)
        fig_out.suptitle("N: {:}, Score {:}".format(iteration_counter, _best_score))
        plt.pause(0.0001)
        fig_out_fitting = fig_out
    iteration_counter += 1
    print("N: {:}, Score {:}".format(iteration_counter, _best_score))
    return out


def error_measure_ga(cfs: np.array = None,
                     target_response: np.array = None,
                     itds: np.array = None,
                     do_plots: bool = True,
                     caller=None,
                     title: str = '',
                     n_to_update_plot: int = 10000,
                     can_function: str = 'exp',
                     sigma: float = np.pi/8,
                     ):
    """

    :param cfs: numpy array indicating the central frequencies
    :param target_response: numpy array with the target IC population response per itd
    :param itds: numpy array indicating the itds of each target response
    :param do_plots: bool, if true plots will be generated
    :param caller: function handler
    :param n_to_update_plot: number of iterations to refresh plot
    :param title: str indicating the window title
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :return:
    """

    out = error_measure(
        cfs=cfs,
        target_response=target_response,
        itds=itds,
        do_plots=do_plots,
        caller=caller,
        n_to_update_plot=n_to_update_plot,
        title=title,
        can_function=can_function,
        sigma=sigma)
    return out,


def error_measure_pso(best_ipds: np.array = None,
                      cfs: np.array = None,
                      target_response: np.array = None,
                      itds: np.array = None,
                      do_plots: bool = True,
                      caller=None,
                      title: str = '',
                      n_to_update_plot: int = 10000,
                      can_function: str = 'exp',
                      sigma: float = np.pi/8,
                      weights: np.array = None,
                      fix_weights: bool = False
                      ):
    """

    :param cfs: numpy array indicating the central frequencies
    :param target_response: numpy array with the target IC population response per itd
    :param itds: numpy array indicating the itds of each target response
    :param do_plots: bool, if true plots will be generated
    :param caller: function handler
    :param n_to_update_plot: number of iterations to refresh plot
    :param best_ipds: numpy array nxm with ipds of each cochlear nucleus neuron
    :param title: str indicating the window title
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :param weights: numpy array with individual neural weights (between 0 and 1)
    :param fix_weights: bool indicating if weights were fitted or not
    :return:
    """
    out = []
    for _i in range(best_ipds.shape[0]):
        out.append(error_measure(
            cfs=cfs,
            target_response=target_response,
            itds=itds,
            do_plots=do_plots,
            caller=caller,
            n_to_update_plot=n_to_update_plot,
            title=title,
            can_function=can_function,
            sigma=sigma,
            weights=weights,
            fix_weights=fix_weights))
    return np.array(out)


def load_individuals(individual, n=int, initial_values=None):
    individuals = []
    for _n in range(n):
        individuals.append(individual(initial_values))
    return individuals


def fit_population(best_ipds: np.array = None,
                   weights: np.array = None,
                   fix_weights: bool = False,
                   fix_sigmas: bool = True,
                   cfs: np.array = None,
                   n_ac_neuron_per_band: int = 200,
                   itds: np.array = None,
                   can_function: str = 'exp',
                   sigma: np.array = np.array([np.pi / 8]),
                   target_response: np.array = None,
                   method: str = 'GA',
                   do_plots: bool = True,
                   n_to_update_plot: int = 10000,
                   n_jobs=None,
                   ipd_space=None,
                   n_generations_pso_ga=200000,
                   tol: float = 1e-10):
    """

    :param cfs: numpy array with central frequencies
    :param n_ac_neuron_per_band: number of neurons per cf
    :param best_ipds: nxm numpy array with best ipds of cochlear nucleus neurons where n is the corresponding cf and m
    total of best ipds
    :param weights: numpy array with weights
    :param fix_weights: if True, weights will not be fitted
    :param fix_sigmas: if True, sigma will not be fitted
    :param itds: numpy array with itds of target response
    :param target_response: ic population response for each itd
    array will be make symmetric.
    :param method: string indicating the optimization method
    :param do_plots: bool, if true plots will be generated
    :param n_to_update_plot: int indicating the number of iterations to refresh plot
    :param n_jobs: number of parallel jobs to process data
    :param ipd_space: numpy array containing the ipds that will be allowed during the optimization process.
    This parameter is only used with GA method
    :param can_function: either "cos" or "exp"
    :param n_generations_pso_ga: number of generations to evolve
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :param tol: tolerance for optimization
    :return: nxm array with optimal ipd for each neuron and center frequency
    """
    global iteration_counter
    iteration_counter = 0
    optimal_fit = None
    bounds_ipd = (((-np.pi, np.pi),) * n_ac_neuron_per_band) * cfs.shape[0]
    bounds_w = (((0, 1),) * n_ac_neuron_per_band) * cfs.shape[0]
    bounds_sigma = (((np.pi / 8, np.pi/4),) * n_ac_neuron_per_band) * cfs.shape[0]
    ####################################################################
    bounds = bounds_ipd
    to_fit = best_ipds.flatten()
    if not fix_weights:
        to_fit = np.concatenate((to_fit.flatten(), weights.flatten()))
        bounds = bounds + bounds_w

    if isinstance(sigma, float):
        sigma = np.ones(best_ipds.shape) * sigma
    if not fix_sigmas:
        to_fit = np.concatenate((to_fit.flatten(), sigma.flatten()))
        bounds = bounds + bounds_sigma

    if method == "L-BFGS-B":
        res_1 = minimize(error_measure, to_fit,
                         args=(cfs, target_response, itds, do_plots, None, n_to_update_plot, method,
                               can_function, sigma, weights, fix_weights, fix_sigmas),
                         method='L-BFGS-B', jac='2-point',
                         options={'gtol': 1e-8, 'disp': True, 'maxiter': 1000}, bounds=bounds, tol=tol)
        optimal_fit = res_1.x
    # ####################################################################
    if method == "BH-L-BFGS-B":
        minimizer_kwargs = {"method": "L-BFGS-B", "bounds": bounds, "tol": tol,
                            "args": (cfs, target_response, itds, do_plots, None, n_to_update_plot, method,
                                     can_function, sigma, weights, fix_weights, fix_sigmas)}
        res_1 = basinhopping(error_measure, x0=to_fit,
                             niter=100, stepsize=np.pi / 128, T=1e-8,
                             minimizer_kwargs=minimizer_kwargs)
        optimal_fit = res_1.x
    # ####################################################################
    if method == "DE":
        res_1 = differential_evolution(error_measure, bounds_ipd,
                                       args=(cfs, target_response, itds, do_plots, None, n_to_update_plot, method,
                                             can_function, sigma, weights, fix_weights),
                                       updating='deferred',
                                       workers=1)
        optimal_fit = res_1.x
    #######################################################################################
    if method == "PSO":
        n_particles = 300
        options = {'c1': 0.7, 'c2': 0.5, 'w': 0.8, 'p': 1, 'k': n_particles//4}
        _ini_ipds = best_ipds.reshape(-1, )
        _lower_bound = np.array(bounds_ipd)[:, 0].reshape(-1, )
        _higher_bound = np.array(bounds_ipd)[:, 1].reshape(-1, )
        _idx = np.argwhere(_ini_ipds < _lower_bound)
        _ini_ipds[_idx] = _lower_bound[_idx]
        _idx = np.argwhere(_ini_ipds > _higher_bound)
        _ini_ipds[_idx] = _higher_bound[_idx]
        from pyswarms.backend.topology import ring
        my_topology = ring.Ring(static=False)
        optimizer = GeneralOptimizerPSO(n_particles=n_particles,
                                        dimensions=cfs.shape[0]*n_ac_neuron_per_band,
                                        options=options,
                                        bounds=(_lower_bound, _higher_bound),
                                        init_pos=np.repeat(_ini_ipds.reshape(1, -1), [n_particles], axis=0),
                                        topology=my_topology,
                                        vh_strategy='invert'
                                        )
        cost, pos = optimizer.optimize(error_measure_pso, n_generations_pso_ga,
                                       cfs=cfs,
                                       target_response=target_response,
                                       itds=itds,
                                       n_to_update_plot=n_to_update_plot,
                                       caller=optimizer if do_plots else None,
                                       do_plots=do_plots,
                                       n_processes=None if do_plots else None,
                                       title=method,
                                       can_function=can_function,
                                       sigma=sigma,
                                       weights=weights,
                                       fix_weights=fix_weights)
        plot_cost_history(cost_history=optimizer.cost_history)
        plt.show()
        # Make animation
        m = Mesher(func=lambda x_in: error_measure(x_in, cfs=cfs,
                                                   target_response=target_response,
                                                   itds=itds,
                                                   n_to_update_plot=n_to_update_plot,
                                                   caller=optimizer if do_plots else None,
                                                   do_plots=do_plots,
                                                   title=method,
                                                   can_function=can_function,
                                                   sigma=sigma))
        plot_contour(pos_history=optimizer.pos_history,
                     mesher=m,
                     mark=(0, 0))
        optimal_fit = pos
    #######################################################################################
    if method == "BF":
        rranges = (slice(0, np.pi, np.pi/8), ) * cfs.shape[0] * n_ac_neuron_per_band
        resbrute = brute(error_measure,
                         rranges,
                         args=(cfs, target_response, itds, do_plots, method, can_function, sigma),
                         full_output=True,
                         finish=fmin)
        optimal_fit = resbrute[0]

    #######################################################################################
    if method == "GA":
        population_size = 1000
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMin)
        toolbox = base.Toolbox()
        # parallelization
        if n_jobs is not None:
            pool = multiprocessing.Pool(n_jobs)
            toolbox.register("map", pool.map)

        # Gene Pool
        _ipd_space = ipd_space[ipd_space >= 0]
        toolbox.register("attr_list", random.choice, _ipd_space)
        # Initialize population
        toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_list,
                         cfs.shape[0] * n_ac_neuron_per_band)
        # toolbox.register("individual", tools.random.choice, creator.Individual,
        #                  toolbox.ipds)
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)
        # toolbox.register("population", lambda c, n: load_individuals(c, n, initial_values=best_ipds.flatten()),
        #                  creator.Individual)

        do_plots = do_plots and n_jobs is None
        toolbox.register("evaluate", error_measure_ga, cfs, target_response, itds, do_plots, None,
                         method, n_to_update_plot, can_function, sigma)

        toolbox.register("mate", tools.cxTwoPoint)
        toolbox.register("mutate", tools.mutShuffleIndexes, indpb=0.05)
        toolbox.register("select", tools.selTournament, tournsize=10)
        population = toolbox.population(n=population_size)

        NGEN = n_generations_pso_ga // population_size
        for gen in range(NGEN):
            offspring = algorithms.varAnd(population, toolbox, cxpb=0.5, mutpb=0.01)
            fits = toolbox.map(toolbox.evaluate, offspring)
            for fit, ind in zip(fits, offspring):
                ind.fitness.values = fit
            population = toolbox.select(offspring, k=len(population))
        optimal_fit = np.array(tools.selBest(population, k=1)).flatten()
    #######################################################################################
    if fix_weights and fix_sigmas:
        optimal_ipds = np.concatenate((optimal_fit, -optimal_fit))
        optimal_weights = np.concatenate((weights.flatten(), weights.flatten()))
        optimal_sigmas = np.concatenate((sigma.flatten(), sigma.flatten()))
    elif not fix_weights and fix_sigmas:
        _opt_ipds = optimal_fit[0:optimal_fit.size // 2]
        optimal_ipds = np.concatenate((_opt_ipds, -_opt_ipds))
        _opt_weights = optimal_fit[optimal_fit.size // 2::]
        optimal_weights = np.concatenate((_opt_weights, _opt_weights))
        optimal_sigmas = np.concatenate((sigma.flatten(), sigma.flatten()))
    elif fix_weights and not fix_sigmas:
        _opt_ipds = optimal_fit[0:optimal_fit.size // 2]
        optimal_ipds = np.concatenate((_opt_ipds, -_opt_ipds))
        optimal_weights = np.concatenate((weights.flatten(), weights.flatten()))
        _opt_sigmas = optimal_fit[optimal_fit.size // 2::]
        optimal_sigmas = np.concatenate((_opt_sigmas, _opt_sigmas))
    elif not fix_weights and not fix_sigmas:
        _opt_ipds = optimal_fit[0:optimal_fit.size // 3]
        optimal_ipds = np.concatenate((_opt_ipds, -_opt_ipds))
        _opt_weights = optimal_fit[optimal_fit.size // 3: (2 * optimal_fit.size) // 3]
        optimal_weights = np.concatenate((_opt_weights, _opt_weights))
        _opt_sigmas = optimal_fit[(2 * optimal_fit.size) // 3::]
        optimal_sigmas = np.concatenate((_opt_sigmas, _opt_sigmas))
    optimal_ipds = optimal_ipds.reshape(-1, cfs.shape[0])
    optimal_weights = optimal_weights.reshape(-1, cfs.shape[0])
    optimal_sigmas = optimal_sigmas.reshape(-1, cfs.shape[0])
    return optimal_ipds, optimal_weights, optimal_sigmas


def fit_population_itd(best_itds: np.array = None,
                       weights: np.array = None,
                       fix_weights: bool = False,
                       fix_sigmas: bool = True,
                       cfs: np.array = None,
                       n_ac_neuron_per_band: int = 200,
                       itds: np.array = None,
                       can_function: str = 'exp',
                       sigma: float = np.pi / 8,
                       target_response: np.array = None,
                       method='GA',
                       do_plots=True,
                       n_to_update_plot=10000,
                       n_jobs=None,
                       itd_space=None,
                       n_generations_pso_ga=200000):
    """

    :param cfs: numpy array with central frequencies
    :param n_ac_neuron_per_band: number of neurons per cf
    :param best_itds: nxm numpy array with best ipds of cochlear nucleus neurons where n is the corresponding cf and m
    total of best ipds
    :param weights: numpy array with weights
    :param fix_weights: if True, weights will not be fitted
    :param fix_sigmas: if True, sigma will not be fitted
    :param itds: numpy array with itds of target response
    :param target_response: ic population response for each itd
    array will be make symmetric.
    :param method: string indicating the optimization method
    :param do_plots: bool, if true plots will be generated
    :param n_to_update_plot: int indicating the number of iterations to refresh plot
    :param n_jobs: number of parallel jobs to process data
    :param itd_space: numpy array containing the itds that will be allowed during the optimization process.
    This parameter is only used with GA method
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :param n_generations_pso_ga: number of generations to evolve
    :return: nxm array with optimal ipd for each neuron and center frequency
    """
    global iteration_counter
    iteration_counter = 0
    bounds_itd = (((-4000e-6, 4000e-6),) * n_ac_neuron_per_band) * cfs.shape[0]
    optimal_fit = None
    bounds_w = (((0, 1),) * n_ac_neuron_per_band) * cfs.shape[0]
    bounds_sigma = (((np.pi / 8, np.pi / 4),) * n_ac_neuron_per_band) * cfs.shape[0]
    ####################################################################
    bounds = bounds_itd
    to_fit = best_itds.flatten()
    if not fix_weights:
        to_fit = np.concatenate((to_fit.flatten(), weights.flatten()))
        bounds = bounds + bounds_w

    if isinstance(sigma, float):
        sigma = np.ones(best_itds.shape) * sigma
    if not fix_sigmas:
        to_fit = np.concatenate((to_fit.flatten(), sigma.flatten()))
        bounds = bounds + bounds_sigma

    if method == "L-BFGS-B":
        res_1 = minimize(error_measure_itd, to_fit,
                         args=(cfs, target_response, itds, do_plots, None, n_to_update_plot, method,
                               can_function, sigma, weights, fix_weights),
                         method='L-BFGS-B', jac='2-point',
                         options={'gtol': 1e-8, 'disp': True, 'maxiter': 1000}, bounds=bounds, tol=1e-10)
        optimal_fit = res_1.x
    # ####################################################################
    if method == "BH-L-BFGS-B":
        minimizer_kwargs = {"method": "L-BFGS-B", "bounds": bounds, "tol": 1e-10,
                            "args": (cfs, target_response, itds, do_plots, None, n_to_update_plot, method,
                                     can_function, sigma, weights, fix_weights, fix_sigmas)}
        res_1 = basinhopping(error_measure_itd, x0=to_fit,
                             niter=100, stepsize=50e-6, T=1e-8,
                             minimizer_kwargs=minimizer_kwargs)
        optimal_fit = res_1.x
    # ####################################################################
    if method == "DE":
        res_1 = differential_evolution(error_measure_itd, bounds_itd,
                                       args=(cfs, target_response, itds, do_plots, None, n_to_update_plot, method,
                                             can_function, sigma, weights, fix_weights),
                                       updating='deferred',
                                       workers=1)
        optimal_fit = res_1.x
    #######################################################################################
    if method == "PSO":
        n_particles = 300
        options = {'c1': 0.7, 'c2': 0.5, 'w': 0.8, 'p': 1, 'k': n_particles//4}
        _ini_itds = best_itds.reshape(-1, )
        _lower_bound = np.array(bounds_itd)[:, 0].reshape(-1, )
        _higher_bound = np.array(bounds_itd)[:, 1].reshape(-1, )
        _idx = np.argwhere(_ini_itds < _lower_bound)
        _ini_itds[_idx] = _lower_bound[_idx]
        _idx = np.argwhere(_ini_itds > _higher_bound)
        _ini_itds[_idx] = _higher_bound[_idx]
        from pyswarms.backend.topology import ring
        my_topology = ring.Ring(static=False)
        optimizer = GeneralOptimizerPSO(n_particles=n_particles,
                                        dimensions=cfs.shape[0]*n_ac_neuron_per_band,
                                        options=options,
                                        bounds=(_lower_bound, _higher_bound),
                                        init_pos=np.repeat(_ini_itds.reshape(1, -1), [n_particles], axis=0),
                                        topology=my_topology,
                                        vh_strategy='invert'
                                        )

        cost, pos = optimizer.optimize(error_measure_pso, n_generations_pso_ga,
                                       cfs=cfs,
                                       target_response=target_response,
                                       itds=itds,
                                       n_to_update_plot=n_to_update_plot,
                                       caller=optimizer if do_plots else None,
                                       do_plots=do_plots,
                                       n_processes=None if do_plots else None,
                                       title=method,
                                       can_function=can_function,
                                       sigma=sigma,
                                       weights=weights,
                                       fix_weights=fix_weights)
        plot_cost_history(cost_history=optimizer.cost_history)
        plt.show()
        # Make animation
        m = Mesher(func=lambda x_in: error_measure(x_in, cfs=cfs,
                                                   target_response=target_response,
                                                   itds=itds,
                                                   n_to_update_plot=n_to_update_plot,
                                                   caller=optimizer if do_plots else None,
                                                   do_plots=do_plots,
                                                   title=method,
                                                   can_function=can_function,
                                                   sigma=sigma))
        plot_contour(pos_history=optimizer.pos_history,
                     mesher=m,
                     mark=(0, 0))
        optimal_fit = pos
    #######################################################################################
    if method == "BF":
        rranges = (slice(0, 4000e-6, 4000e-6/8), ) * cfs.shape[0] * n_ac_neuron_per_band
        resbrute = brute(error_measure,
                         rranges,
                         args=(cfs, target_response, itds, do_plots, method, can_function, sigma),
                         full_output=True,
                         finish=fmin)
        optimal_fit = resbrute[0]

    #######################################################################################
    if method == "GA":
        population_size = 1000
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMin)
        toolbox = base.Toolbox()
        # parallelization
        if n_jobs is not None:
            pool = multiprocessing.Pool(n_jobs)
            toolbox.register("map", pool.map)

        # Gene Pool
        _itd_space = itd_space[itd_space >= 0]
        toolbox.register("attr_list", random.choice, _itd_space)
        # Initialize population
        toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_list,
                         cfs.shape[0] * n_ac_neuron_per_band)
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)

        do_plots = do_plots and n_jobs is None
        toolbox.register("evaluate", error_measure_ga, cfs, target_response, itds, do_plots, None,
                         method, n_to_update_plot, can_function, sigma)

        toolbox.register("mate", tools.cxTwoPoint)
        toolbox.register("mutate", tools.mutShuffleIndexes, indpb=0.05)
        toolbox.register("select", tools.selTournament, tournsize=10)
        population = toolbox.population(n=population_size)

        NGEN = n_generations_pso_ga // population_size
        for gen in range(NGEN):
            offspring = algorithms.varAnd(population, toolbox, cxpb=0.5, mutpb=0.01)
            fits = toolbox.map(toolbox.evaluate, offspring)
            for fit, ind in zip(fits, offspring):
                ind.fitness.values = fit
            population = toolbox.select(offspring, k=len(population))
        optimal_fit = np.array(tools.selBest(population, k=1)).flatten()
    #######################################################################################
    #######################################################################################
    if fix_weights and fix_sigmas:
        optimal_itds = np.concatenate((optimal_fit, -optimal_fit))
        optimal_weights = np.concatenate((weights.flatten(), weights.flatten()))
        optimal_sigmas = np.concatenate((sigma.flatten(), sigma.flatten()))
    elif not fix_weights and fix_sigmas:
        _opt_itds = optimal_fit[0:optimal_fit.size // 2]
        optimal_itds = np.concatenate((_opt_itds, -_opt_itds))
        _opt_weights = optimal_fit[optimal_fit.size // 2::]
        optimal_weights = np.concatenate((_opt_weights, _opt_weights))
        optimal_sigmas = np.concatenate((sigma.flatten(), sigma.flatten()))
    elif fix_weights and not fix_sigmas:
        _opt_itds = optimal_fit[0:optimal_fit.size // 2]
        optimal_itds = np.concatenate((_opt_itds, -_opt_itds))
        optimal_weights = np.concatenate((weights.flatten(), weights.flatten()))
        _opt_sigmas = optimal_fit[optimal_fit.size // 2::]
        optimal_sigmas = np.concatenate((_opt_sigmas, _opt_sigmas))
    elif not fix_weights and not fix_sigmas:
        _opt_itds = optimal_fit[0:optimal_fit.size // 3]
        optimal_itds = np.concatenate((_opt_itds, -_opt_itds))
        _opt_weights = optimal_fit[optimal_fit.size // 3: (2 * optimal_fit.size) // 3]
        optimal_weights = np.concatenate((_opt_weights, _opt_weights))
        _opt_sigmas = optimal_fit[(2 * optimal_fit.size) // 3::]
        optimal_sigmas = np.concatenate((_opt_sigmas, _opt_sigmas))
    optimal_itds = optimal_itds.reshape(-1, cfs.shape[0])
    optimal_weights = optimal_weights.reshape(-1, cfs.shape[0])
    optimal_sigmas = optimal_sigmas.reshape(-1, cfs.shape[0])
    return optimal_itds, optimal_weights, optimal_sigmas


def fit_population_itd_to_ipd(best_itds: np.array = None,
                              weights: np.array = None,
                              fix_weights: bool = False,
                              fix_sigmas: bool = True,
                              cfs: np.array = None,
                              n_ac_neuron_per_band: int = 200,
                              itds: np.array = None,
                              can_function: str = 'exp',
                              sigma: float = np.pi / 8,
                              target_response: np.array = None,
                              method='GA',
                              do_plots=True,
                              n_to_update_plot=10000,
                              n_jobs=None,
                              itd_space=None,
                              n_generations_pso_ga=200000):
    """

    :param cfs: numpy array with central frequencies
    :param n_ac_neuron_per_band: number of neurons per cf
    :param best_itds: nxm numpy array with best ipds of cochlear nucleus neurons where n is the corresponding cf and m
    total of best ipds
    :param weights: numpy array with weights
    :param fix_weights: if True, weights will not be fitted
    :param fix_sigmas: if True, sigma will not be fitted
    :param itds: numpy array with itds of target response
    :param target_response: ic population response for each itd
    array will be make symmetric.
    :param method: string indicating the optimization method
    :param do_plots: bool, if true plots will be generated
    :param n_to_update_plot: int indicating the number of iterations to refresh plot
    :param n_jobs: number of parallel jobs to process data
    :param itd_space: numpy array containing the itds that will be allowed during the optimization process.
    This parameter is only used with GA method
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :param n_generations_pso_ga: number of generations to evolve
    :return: nxm array with optimal ipd for each neuron and center frequency
    """
    global iteration_counter
    iteration_counter = 0
    bounds_itd = (((-4000e-6, 4000e-6),) * n_ac_neuron_per_band) * cfs.shape[0]
    optimal_fit = None
    bounds_w = (((0, 1),) * n_ac_neuron_per_band) * cfs.shape[0]
    bounds_sigma = (((np.pi / 8, np.pi / 4),) * n_ac_neuron_per_band) * cfs.shape[0]
    ####################################################################
    bounds = bounds_itd
    to_fit = best_itds.flatten()

    if not fix_weights:
        to_fit = np.concatenate((to_fit.flatten(), weights.flatten()))
        bounds = bounds + bounds_w

    if isinstance(sigma, float):
        sigma = np.ones(best_itds.shape) * sigma
    if not fix_sigmas:
        to_fit = np.concatenate((to_fit.flatten(), sigma.flatten()))
        bounds = bounds + bounds_sigma

    if method == "L-BFGS-B":
        res_1 = minimize(error_measure_itd_to_ipd, to_fit,
                         args=(cfs, target_response, itds, do_plots, None, n_to_update_plot, method,
                               can_function, sigma, weights, fix_weights),
                         method='L-BFGS-B', jac='2-point',
                         options={'gtol': 1e-8, 'disp': True, 'maxiter': 1000}, bounds=bounds, tol=1e-10)
        optimal_fit = res_1.x
    # ####################################################################
    if method == "BH-L-BFGS-B":
        minimizer_kwargs = {"method": "L-BFGS-B", "bounds": bounds, "tol": 1e-10,
                            "args": (cfs, target_response, itds, do_plots, None, n_to_update_plot, method,
                                     can_function, sigma, weights, fix_weights, fix_sigmas)}
        res_1 = basinhopping(error_measure_itd_to_ipd, x0=to_fit,
                             niter=100, stepsize=50e-6, T=1e-8,
                             minimizer_kwargs=minimizer_kwargs)
        optimal_fit = res_1.x
    # ####################################################################
    if method == "DE":
        res_1 = differential_evolution(error_measure_itd, bounds_itd,
                                       args=(cfs, target_response, itds, do_plots, None, n_to_update_plot, method,
                                             can_function, sigma, weights, fix_weights),
                                       updating='deferred',
                                       workers=1)
        optimal_fit = res_1.x
    #######################################################################################
    if method == "PSO":
        n_particles = 300
        options = {'c1': 0.7, 'c2': 0.5, 'w': 0.8, 'p': 1, 'k': n_particles//4}
        _ini_itds = best_itds.reshape(-1, )
        _lower_bound = np.array(bounds_itd)[:, 0].reshape(-1, )
        _higher_bound = np.array(bounds_itd)[:, 1].reshape(-1, )
        _idx = np.argwhere(_ini_itds < _lower_bound)
        _ini_itds[_idx] = _lower_bound[_idx]
        _idx = np.argwhere(_ini_itds > _higher_bound)
        _ini_itds[_idx] = _higher_bound[_idx]
        from pyswarms.backend.topology import ring
        my_topology = ring.Ring(static=False)
        optimizer = GeneralOptimizerPSO(n_particles=n_particles,
                                        dimensions=cfs.shape[0]*n_ac_neuron_per_band,
                                        options=options,
                                        bounds=(_lower_bound, _higher_bound),
                                        init_pos=np.repeat(_ini_itds.reshape(1, -1), [n_particles], axis=0),
                                        topology=my_topology,
                                        vh_strategy='invert'
                                        )

        cost, pos = optimizer.optimize(error_measure_pso, n_generations_pso_ga,
                                       cfs=cfs,
                                       target_response=target_response,
                                       itds=itds,
                                       n_to_update_plot=n_to_update_plot,
                                       caller=optimizer if do_plots else None,
                                       do_plots=do_plots,
                                       n_processes=None if do_plots else None,
                                       title=method,
                                       can_function=can_function,
                                       sigma=sigma,
                                       weights=weights,
                                       fix_weights=fix_weights)
        plot_cost_history(cost_history=optimizer.cost_history)
        plt.show()
        # Make animation
        m = Mesher(func=lambda x_in: error_measure(x_in, cfs=cfs,
                                                   target_response=target_response,
                                                   itds=itds,
                                                   n_to_update_plot=n_to_update_plot,
                                                   caller=optimizer if do_plots else None,
                                                   do_plots=do_plots,
                                                   title=method,
                                                   can_function=can_function,
                                                   sigma=sigma))
        plot_contour(pos_history=optimizer.pos_history,
                     mesher=m,
                     mark=(0, 0))
        optimal_fit = pos
    #######################################################################################
    if method == "BF":
        rranges = (slice(0, 4000e-6, 4000e-6/8), ) * cfs.shape[0] * n_ac_neuron_per_band
        resbrute = brute(error_measure,
                         rranges,
                         args=(cfs, target_response, itds, do_plots, method, can_function, sigma),
                         full_output=True,
                         finish=fmin)
        optimal_fit = resbrute[0]

    #######################################################################################
    if method == "GA":
        population_size = 1000
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMin)
        toolbox = base.Toolbox()
        # parallelization
        if n_jobs is not None:
            pool = multiprocessing.Pool(n_jobs)
            toolbox.register("map", pool.map)

        # Gene Pool
        _itd_space = itd_space[itd_space >= 0]
        toolbox.register("attr_list", random.choice, _itd_space)
        # Initialize population
        toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_list,
                         cfs.shape[0] * n_ac_neuron_per_band)
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)

        do_plots = do_plots and n_jobs is None
        toolbox.register("evaluate", error_measure_ga, cfs, target_response, itds, do_plots, None,
                         method, n_to_update_plot, can_function, sigma)

        toolbox.register("mate", tools.cxTwoPoint)
        toolbox.register("mutate", tools.mutShuffleIndexes, indpb=0.05)
        toolbox.register("select", tools.selTournament, tournsize=10)
        population = toolbox.population(n=population_size)

        NGEN = n_generations_pso_ga // population_size
        for gen in range(NGEN):
            offspring = algorithms.varAnd(population, toolbox, cxpb=0.5, mutpb=0.01)
            fits = toolbox.map(toolbox.evaluate, offspring)
            for fit, ind in zip(fits, offspring):
                ind.fitness.values = fit
            population = toolbox.select(offspring, k=len(population))
        optimal_fit = np.array(tools.selBest(population, k=1)).flatten()
    #######################################################################################
    #######################################################################################
    if fix_weights and fix_sigmas:
        optimal_itds = np.concatenate((optimal_fit, -optimal_fit))
        optimal_weights = np.concatenate((weights.flatten(), weights.flatten()))
        optimal_sigmas = np.concatenate((sigma.flatten(), sigma.flatten()))
    elif not fix_weights and fix_sigmas:
        _opt_itds = optimal_fit[0:optimal_fit.size // 2]
        optimal_itds = np.concatenate((_opt_itds, -_opt_itds))
        _opt_weights = optimal_fit[optimal_fit.size // 2::]
        optimal_weights = np.concatenate((_opt_weights, _opt_weights))
        optimal_sigmas = np.concatenate((sigma.flatten(), sigma.flatten()))
    elif fix_weights and not fix_sigmas:
        _opt_itds = optimal_fit[0:optimal_fit.size // 2]
        optimal_itds = np.concatenate((_opt_itds, -_opt_itds))
        optimal_weights = np.concatenate((weights.flatten(), weights.flatten()))
        _opt_sigmas = optimal_fit[optimal_fit.size // 2::]
        optimal_sigmas = np.concatenate((_opt_sigmas, _opt_sigmas))
    elif not fix_weights and not fix_sigmas:
        _opt_itds = optimal_fit[0:optimal_fit.size // 3]
        optimal_itds = np.concatenate((_opt_itds, -_opt_itds))
        _opt_weights = optimal_fit[optimal_fit.size // 3: (2 * optimal_fit.size) // 3]
        optimal_weights = np.concatenate((_opt_weights, _opt_weights))
        _opt_sigmas = optimal_fit[(2 * optimal_fit.size) // 3::]
        optimal_sigmas = np.concatenate((_opt_sigmas, _opt_sigmas))
    optimal_itds = optimal_itds.reshape(-1, cfs.shape[0])
    optimal_weights = optimal_weights.reshape(-1, cfs.shape[0])
    optimal_sigmas = optimal_sigmas.reshape(-1, cfs.shape[0])
    return optimal_itds, optimal_weights, optimal_sigmas


def plot_ipd_distribution(cfs: np.array = None,
                          n_bins=40,
                          best_ipds: np.array = None,
                          make_symmetric_ipd=True,
                          fig_out: matplotlib.figure.Figure = None):
    """
    plot neural distribution  as function of ipd
    :param cfs: numpy array with central frequencies
    :param n_bins: number of bins for ipd space
    :param best_ipds: nxm numpy array with best ipds of cochlear nucleus neurons where n is the corresponding cf and m
    total of best ipds
    :param make_symmetric_ipd: if true, best_ipds are assumed to represent one hemisphere, therefore the used best_ipd
    array will be make symmetric.
    :param fig_out: handle to figure
    """
    h = np.empty((cfs.shape[0], n_bins))
    _edge = None
    for _idx in range(best_ipds.shape[1]):
        if make_symmetric_ipd:
            sym_best_ipds = np.concatenate((best_ipds[:, _idx].flatten(), -best_ipds[:, _idx].flatten()))
        else:
            sym_best_ipds = best_ipds[:, _idx].flatten()
        _counts, _edge = np.histogram(sym_best_ipds,
                                      bins=np.linspace(-np.pi, np.pi, n_bins + 1),
                                      range=(-np.pi, np.pi)
                                      )
        h[_idx, :] = _counts

    _max_itd = max_ecological_itd(head_diameter=22 / 100)
    _max_ipd = max_ipd(cf=cfs, max_itd=_max_itd)

    if fig_out is None:
        fig_out = plt.figure(constrained_layout=True)
    gs = fig_out.add_gridspec(ncols=1, nrows=1)
    ax = plt.subplot(gs[0, 0])
    xx, yy = np.meshgrid(_edge * 0.5 / np.pi, cfs)
    ax.pcolor(xx, yy, h)
    ax.plot(_max_ipd / (2 * np.pi), cfs, 'r')
    ax.plot(-_max_ipd / (2 * np.pi), cfs, 'r')
    ax.set_xlabel('Best IPD [cycles]')
    ax.set_ylabel('CF')
    ax.set_xlim(-0.5, 0.5)
    ax.set_ylim(cfs.min(), cfs.max())

    return fig_out


def plot_ipd_distribution_and_ac_population_response(cfs: np.array = None,
                                                     best_ipds: np.array = None,
                                                     weights: np.array = None,
                                                     itds: np.array = None,
                                                     target_response: np.array = None,
                                                     target_label: str = 'target',
                                                     fig_out: matplotlib.figure.Figure = None,
                                                     make_symmetric_ipd=True,
                                                     can_function: str = 'exp',
                                                     sigma: np.array = np.array([np.pi / 8]),
                                                     show_fitted=True,
                                                     n_bins=70,
                                                     freq_band_width: float = None,
                                                     head_diameter: float = 22,
                                                     vmax: float = None,
                                                     show_centroid=True,
                                                     n_clusters=1
                                                     ):
    """
    plot neural distribution and AC output as function of ipd
    :param cfs: numpy array with central frequencies
    :param best_ipds: nxm numpy array with best ipds of cochlear nucleus neurons where n is the corresponding cf and m
    total of best ipds
    :param weights: weights assigned to each neuron
    :param itds: numpy array with itds of target response
    :param target_response: ic population response for each itd
    :param target_label: label for the legend
    :param fig_out: handle to pass figure
    :param make_symmetric_ipd: if true, best_ipds are assumed to represent one hemisphere, therefore the used best_ipd
    array will be make symmetric.
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :param show_fitted: If true, fitted data will be shown
    :param n_bins: number of bins in which ipd space will be divided
    :param freq_band_width: bandwidth of the frequency bins in Hz
    :param head_diameter: diameter of head (in cm)
    :param vmax: maximum value in color bar
    :param show_centroid: whether to show the centroid (in ipd/itd) or not
    :param n_clusters: number of gaussian functions fitted to estimate centroids
    :return: figure handle
    """

    h = np.empty((cfs.shape[0], n_bins))
    _edge_binned = None
    _edge = None
    if weights is None:
        weights = np.ones(best_ipds.shape)

    if isinstance(sigma, float):
        sigma = np.ones(best_ipds.shape) * sigma
    if make_symmetric_ipd:
        sym_best_ipds = np.vstack((best_ipds, -best_ipds))
        sym_weights = np.vstack((weights, weights))
        sym_sigmas = np.vstack((sigma, sigma))
    else:
        sym_best_ipds = best_ipds
        sym_weights = weights
        sym_sigmas = sigma

    for _idx in range(best_ipds.shape[1]):
        _sym_best_ipds = sym_best_ipds[:, _idx].flatten()
        if isinstance(_sym_best_ipds, np.ma.masked_array):
            _sym_best_ipds = _sym_best_ipds.data[~_sym_best_ipds.mask]
        _sym_weights = sym_weights[:, _idx].flatten()
        if isinstance(_sym_weights, np.ma.masked_array):
            _sym_weights = _sym_weights.data[~_sym_weights.mask]

        _counts, _edge = np.histogram(_sym_best_ipds, bins=n_bins,
                                      range=(-np.pi, np.pi))
        h[_idx, :] = _counts
    h_total = np.sum(h, axis=0)

    cfs_binned, optimal_ipds_binned, optimal_weights_binned, optimal_sigmas_binned = frequency_binning(
        f_low=cfs.min(),
        f_high=cfs.max(),
        band_width=freq_band_width,
        cfs=np.repeat(cfs[:, None], [sym_best_ipds.shape[0]], axis=1).T,
        ipds=sym_best_ipds,
        sigmas=sym_sigmas,
        weights=sym_weights)
    h_binned = np.empty((cfs_binned.shape[0], n_bins))
    for _idx in range(optimal_ipds_binned.shape[1]):
        _sym_best_binned_ipds = optimal_ipds_binned[:, _idx].flatten()
        if isinstance(_sym_best_binned_ipds, np.ma.masked_array):
            _sym_best_binned_ipds = _sym_best_binned_ipds.data[~_sym_best_binned_ipds.mask]
        _sym_binned_weights = optimal_weights_binned[:, _idx].flatten()
        if isinstance(_sym_binned_weights, np.ma.masked_array):
            _sym_binned_weights = _sym_binned_weights.data[~_sym_binned_weights.mask]
        _counts_binned, _edge_binned = np.histogram(_sym_best_binned_ipds, bins=n_bins,
                                                    range=(-np.pi, np.pi))
        h_binned[_idx, :] = _counts_binned
    _edge_binned = _edge_binned[0:-1]
    _edge += np.mean(np.diff(_edge)) / 2

    if fig_out is None:
        inch = 2.54
        fig_out = plt.figure(constrained_layout=False)
        fig_out.set_size_inches(10 / inch, 9 / inch)
        fig_out.subplots_adjust(hspace=0.4, top=0.98, bottom=0.08, left=0.15, right=.85, wspace=0.05)

    widths = [0.6, 0.02, 0.2, 0.6]
    heights = [1, 1, 1]
    gs = fig_out.add_gridspec(ncols=4, nrows=3, width_ratios=widths,
                              height_ratios=heights, wspace=0.1)
    axs = []
    ax = plt.subplot(gs[0, 0])

    x = np.linspace(-0.5, 0.5, 100)
    neural_activity = np.zeros(x.shape)
    for _i in range(best_ipds.shape[1]):
        srf = np.array([np.max(h_total) * _w * g_p(ipd=np.pi * x / 0.5,
                                                   best_ipd=_b_ipd,
                                                   sigma=_sigma,
                                                   can_function=can_function) / 100
                        for _b_ipd, _w, _sigma in zip(sym_best_ipds[:, _i],
                                                      sym_weights[:, _i],
                                                      sym_sigmas[:, _i]
                                                      )]).T
        neural_activity += np.nansum(srf, axis=1)

    ipd_axis = _edge[0:-1] / (2 * np.pi)
    bin_width = np.mean(np.diff(ipd_axis))
    ax.bar(ipd_axis, h_total,
           color='r',
           width=bin_width,
           edgecolor="black",
           alpha=0.8)
    if show_centroid:
        all_ipds = optimal_ipds_binned.flatten() / (2 * np.pi)
        features = all_ipds[all_ipds <= 0][:, None]
        gm = GaussianMixture(n_components=n_clusters,
                             covariance_type='full',
                             random_state=0,
                             max_iter=1000).fit(features)
        centroids = np.sort(gm.means_[:, 0])
        for _centroid in centroids:
            ax.axvline(_centroid, color='greenyellow')
            ax.axvline(-_centroid, color='greenyellow')
    ax.plot(x, np.max(h_total) * neural_activity / np.max(neural_activity),
            color='k',
            linestyle=':',
            alpha=1,
            linewidth=3)

    ax.set_ylabel('Count', fontsize=8)
    ax.set_xlim(-0.5, 0.5)
    ax.set_xticks(np.array([-0.5, 0, 0.5]))
    ax.tick_params(axis='both', labelsize=8)
    ax.set_ylim(0, None)
    ax.set_xlabel('Best-ITD [cycles]', fontsize=8)

    axs += [ax]
    ax = plt.subplot(gs[1, 0])
    xx, yy = np.meshgrid(_edge[0:-1] * 0.5 / np.pi, cfs_binned)
    ax.pcolor(xx, yy, h_binned.astype(int),
              cmap='jet',
              vmin=np.min(h_binned),
              vmax=np.minimum(50, np.max(h_binned)))
    if show_centroid:
        centroid_left = np.zeros((yy.shape[0], n_clusters))
        for _idx_freq in range(yy.shape[0]):
            _idx_left = np.argwhere(xx[_idx_freq, :] <= 0)
            all_ipds = optimal_ipds_binned[:, _idx_freq].flatten() / (2 * np.pi)
            features = all_ipds[all_ipds <= 0][:, None]
            gm = GaussianMixture(n_components=n_clusters,
                                 covariance_type='full',
                                 random_state=0,
                                 max_iter=1000).fit(features)
            centroid_left[_idx_freq, :] = np.sort(gm.means_[:, 0])

        for i_c in range(n_clusters):
            ax.plot(centroid_left[:, i_c], cfs_binned, 'greenyellow')
            ax.plot(-centroid_left[:, i_c], cfs_binned, 'greenyellow')
    _max_itd = None
    if head_diameter is not None:
        _max_itd = max_ecological_itd(head_diameter=head_diameter)
        _freq_range = ax.get_ylim()
        _freq_range_plot = np.arange(_freq_range[0], _freq_range[1], 10)
        _max_ipd_plot = max_ipd(cf=_freq_range_plot, max_itd=_max_itd, restricted=False)
        ax.plot(_max_ipd_plot / (2 * np.pi),
                _freq_range_plot, 'r',
                linewidth=2)
        ax.plot(-_max_ipd_plot / (2 * np.pi),
                _freq_range_plot, 'r',
                linewidth=2)
    ax.set_xlabel('Best-ITD [cycles]', fontsize=8)
    ax.set_ylabel('CF', fontsize=8)
    ax.set_xlim(-0.5, 0.5)
    ax.set_xticks(np.array([-0.5, 0, 0.5]))
    ax.tick_params(axis='both', labelsize=8)
    # color bar

    ax_cmap2 = plt.subplot(gs[1, 1])
    cmap = mpl.cm.jet
    if vmax is None:
        vmax = np.max(h_binned)
    norm = mpl.colors.Normalize(vmin=0, vmax=vmax)
    cb2 = mpl.colorbar.ColorbarBase(ax_cmap2, cmap=cmap,
                                    norm=norm,
                                    orientation='vertical')
    cb2.set_label('Neurons', fontsize=8)
    cb2.ax.tick_params(labelsize=8)
    axs += [ax]
    ax = plt.subplot(gs[2, 0])
    _label = ''
    if show_fitted:
        _label = 'model'
    population_response = ic_population_response_ipd(cfs=cfs,
                                                     best_ipds=best_ipds,
                                                     weights=weights,
                                                     itds=itds,
                                                     can_function=can_function,
                                                     sigma=sigma,
                                                     make_symmetric_ipd=make_symmetric_ipd)
    ax.plot(itds*1e6, population_response,
            label=_label,
            color="salmon")
    if target_response is not None:
        ax.plot(itds*1e6, target_response, label=target_label, color="indigo")
    ax.set_xlabel(r'ITD [$\mu$s]', fontsize=8)
    ax.set_xticks(np.array([-4000, 0, 4000]))
    ax.tick_params(axis='both', labelsize=8)
    ax.set_ylabel('Normalized Amplitude', fontsize=8)
    ax.set_ylim(0, 1.4)
    plt.legend(frameon=False, loc='upper right', prop={'size': 6})

    axs += [ax]
    ax = plt.subplot(gs[:, 3:])
    srf = np.zeros((optimal_ipds_binned.shape[1], x.shape[0], optimal_ipds_binned.shape[0]))
    for _i in range(optimal_ipds_binned.shape[1]):
        srf[_i, ...] = np.array([_w * g_p(ipd=np.pi * x / 0.5,
                                          best_ipd=_b_ipd,
                                          sigma=_sigma,
                                          can_function=can_function)
                                 for _b_ipd, _w, _sigma in zip(optimal_ipds_binned[:, _i],
                                                               optimal_weights_binned[:, _i],
                                                               optimal_sigmas_binned[:, _i])]).T

    srf = srf / np.nanmax(srf)
    for _i in range(optimal_ipds_binned.shape[1]):
        _srf = srf[_i, ...]
        mask = np.isnan(_srf)
        _srf[mask] = 0
        srf_thr = 0.1
        _srf[_srf < srf_thr] = None
        ax.plot(x, _srf + _i - srf_thr,
                color='k',
                alpha=0.05)

    ax.set_facecolor('xkcd:white')
    ax.set_xlabel('Best-ITD [cycles]', fontsize=8)
    ax.set_yticks(np.arange(0, len(cfs_binned)))
    ax.set_yticklabels(cfs_binned.astype(int))
    ax.set_ylim(0, len(cfs_binned))
    ax.yaxis.tick_right()
    ax.set_ylabel('CF [Hz]', fontsize=8)
    ax.yaxis.set_label_position("right")
    ax.set_xticks(np.array([-0.5, 0, 0.5]))
    ax.tick_params(axis='both', labelsize=8)

    return fig_out, h_binned


def plot_itd_distribution_and_ac_population_response(cfs: np.array = None,
                                                     best_itds: np.array = None,
                                                     weights: np.array = None,
                                                     itds: np.array = None,
                                                     target_response: np.array = None,
                                                     target_label: str = 'target',
                                                     fig_out: matplotlib.figure.Figure = None,
                                                     make_symmetric_itd=True,
                                                     can_function: str = 'exp',
                                                     sigma: np.array = np.array([np.pi / 8]),
                                                     show_fitted=True,
                                                     n_bins=70,
                                                     freq_band_width=100,
                                                     head_diameter=0.22,
                                                     show_centroid=False,
                                                     n_clusters=3
                                                     ):
    """
    plot neural distribution and AC output as function of itd
    :param cfs: numpy array with central frequencies
    :param best_itds: n x m numpy array with best itds of AC neurons where n is the corresponding cf and m
    total of best itds
    :param weights: weights assigned to each neuron
    :param itds: numpy array with itds of target response
    :param target_response: ic population response for each itd
    :param target_label: label for the legend
    :param fig_out: handle to pass figure
    :param make_symmetric_itd: if true, best_itds are assumed to represent one hemisphere, therefore the used best_itd
    array will be make symmetric.
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :param show_fitted: If true, fitted data will be shown
    :param n_bins: number of bins in which itd space will be divided
    :param freq_band_width: bandwidth of the frequency bins in Hz
    :param head_diameter: diameter of head (in cm)
    :param show_centroid: whether to show the centroid (in ipd/itd) or not
    :param n_clusters: number of gaussian functions fitted to estimate centroids
    :return: figure handle
    """

    h = np.empty((cfs.shape[0], n_bins))
    _edge_binned = None
    _edge = None
    if weights is None:
        weights = np.ones(best_itds.shape)
    if isinstance(sigma, float):
        sigma = np.ones(best_itds.shape) * sigma
    if make_symmetric_itd:
        sym_best_itds = np.vstack((best_itds, -best_itds))
        sym_weights = np.vstack((weights, weights))
        sym_sigmas = np.vstack((sigma, sigma))
    else:
        sym_best_itds = best_itds
        sym_weights = weights
        sym_sigmas = sigma

    for _idx in range(best_itds.shape[1]):
        _sym_best_itds = sym_best_itds[:, _idx].flatten()
        if isinstance(_sym_best_itds, np.ma.masked_array):
            _sym_best_itds = _sym_best_itds.data[~_sym_best_itds.mask]
        _sym_weights = sym_weights[:, _idx].flatten()
        if isinstance(_sym_weights, np.ma.masked_array):
            _sym_weights = _sym_weights.data[~_sym_weights.mask]

        _counts, _edge = np.histogram(_sym_best_itds, bins=n_bins,
                                      range=(-4000e-6, 4000e-6))
        h[_idx, :] = _counts
    h_total = np.sum(h, axis=0)

    cfs_binned, optimal_itds_binned, optimal_weights_binned, optimal_sigmas_binned = frequency_binning_itd(
        f_low=cfs.min(),
        f_high=cfs.max(),
        band_width=freq_band_width,
        cfs=np.repeat(cfs[:, None], [sym_best_itds.shape[0]], axis=1).T,
        itds=sym_best_itds,
        sigmas=sym_sigmas,
        weights=sym_weights)
    h_binned = np.empty((cfs_binned.shape[0], n_bins))
    for _idx in range(optimal_itds_binned.shape[1]):
        _sym_best_binned_itds = optimal_itds_binned[:, _idx].flatten()
        if isinstance(_sym_best_binned_itds, np.ma.masked_array):
            _sym_best_binned_itds = _sym_best_binned_itds.data[~_sym_best_binned_itds.mask]
        _sym_binned_weights = optimal_weights_binned[:, _idx].flatten()
        if isinstance(_sym_binned_weights, np.ma.masked_array):
            _sym_binned_weights = _sym_binned_weights.data[~_sym_binned_weights.mask]
        _counts_binned, _edge_binned = np.histogram(_sym_best_binned_itds, bins=n_bins,
                                                    range=(-4000e-6, 4000e-6))
        h_binned[_idx, :] = _counts_binned
    _edge_binned = _edge_binned[0:-1]
    _edge += np.mean(np.diff(_edge)) / 2
    _max_itd = None

    if fig_out is None:
        inch = 2.54
        fig_out = plt.figure(constrained_layout=False)
        fig_out.set_size_inches(18 / inch, 14 / inch)
        fig_out.subplots_adjust(hspace=0.4, top=0.98, bottom=0.08, left=0.15, right=.85, wspace=0.05)

    widths = [0.8, 0.02, 0.1, 0.6]
    heights = [1, 1, 1]
    gs = fig_out.add_gridspec(ncols=4, nrows=3, width_ratios=widths,
                              height_ratios=heights, wspace=0.1)
    axs = []
    ax = plt.subplot(gs[0, 0])

    x = np.linspace(-4000e-6, 4000e-6, 500)
    neural_activity = np.zeros(x.shape)
    for _i in range(best_itds.shape[1]):
        srf = np.array([np.max(h_total) * _w * g_itd(itd=x,
                                                     best_itd=_b_itd,
                                                     sigma=_sigma,
                                                     can_function=can_function,
                                                     cf=_cf) / 100
                        for _b_itd, _w, _cf, _sigma in zip(sym_best_itds[:, _i],
                                                           sym_weights[:, _i],
                                                           cfs[_i] * np.ones(sym_best_itds[:, _i].shape),
                                                           sym_sigmas[:, _i])]).T
        neural_activity += np.nansum(srf, axis=1)
    itd_axis = _edge[0:-1] * 1e6
    bin_width = np.mean(np.diff(itd_axis))
    ax.bar(itd_axis, h_total,
           color='r',
           width=bin_width,
           edgecolor="black",
           alpha=0.8)

    ax.plot(x * 1e6, np.max(h_total) * neural_activity / np.max(neural_activity),
            color='k',
            linestyle=':',
            alpha=1,
            linewidth=3)
    if show_centroid:
        _idx_left = np.argwhere(itd_axis <= 0)
        _idx_right = np.argwhere(itd_axis >= 0)
        all_itds = optimal_itds_binned.flatten() * 1e6
        features = all_itds[all_itds <= 0][:, None]
        gm = GaussianMixture(n_components=n_clusters,
                             covariance_type='full',
                             random_state=0,
                             max_iter=1000).fit(features)
        centroids = np.sort(gm.means_[:, 0])
        for _centroid in centroids:
            ax.axvline(_centroid, color='greenyellow')
            ax.axvline(-_centroid, color='greenyellow')

    ax.set_ylabel('Count', fontsize=8)
    ax.set_xlim(-4000, 4000)
    ax.set_xticks(np.array([-4000, 0, 4000]))
    ax.tick_params(axis='both', labelsize=8)
    ax.set_ylim(0, None)
    ax.set_xlabel(r'Best-ITD [$\mu$s]', fontsize=8)

    axs += [ax]
    ax = plt.subplot(gs[1, 0])
    xx, yy = np.meshgrid(_edge[0:-1] * 1e6, cfs_binned)
    ax.pcolor(xx, yy, h_binned.astype(int), cmap='jet', vmin=np.min(h_binned), vmax=np.minimum(50, np.max(h_binned)))
    _freq_range = ax.get_ylim()
    _freq_range_plot = np.arange(_freq_range[0], _freq_range[1], 10)

    if show_centroid:
        centroid_left = np.zeros((yy.shape[0], n_clusters))
        for _idx_freq in range(yy.shape[0]):
            _idx_left = np.argwhere(xx[_idx_freq, :] <= 0)
            all_itds = optimal_itds_binned[:, _idx_freq].flatten() * 1e6
            features = all_itds[all_itds <= 0][:, None]
            gm = GaussianMixture(n_components=n_clusters,
                                 covariance_type='full',
                                 random_state=0,
                                 max_iter=1000).fit(features)
            centroid_left[_idx_freq, :] = np.sort(gm.means_[:, 0])

        for i_c in range(n_clusters):
            ax.plot(centroid_left[:, i_c], cfs_binned, 'greenyellow')
            ax.plot(-centroid_left[:, i_c], cfs_binned, 'greenyellow')

    for _n in np.arange(1, 2):
        pi_lim = pi_limit(cf=_freq_range_plot, period_n=_n)
        ax.plot(pi_lim * 1e6, _freq_range_plot, 'tab:orange')
        ax.plot(-pi_lim * 1e6, _freq_range_plot, 'tab:orange')
    pi_half = pi_limit(cf=_freq_range_plot, period_n=0.5)
    ax.plot(pi_half * 1e6, _freq_range_plot, 'yellow')
    ax.plot(-pi_half * 1e6, _freq_range_plot, 'yellow')

    if head_diameter is not None:
        _max_itd = max_ecological_itd(head_diameter=head_diameter)
        ax.plot(_max_itd * 1e6 * np.ones(_freq_range_plot.shape),
                _freq_range_plot, 'red',
                linewidth=2)
        ax.plot(-_max_itd * 1e6 * np.ones(_freq_range_plot.shape),
                _freq_range_plot, 'red',
                linewidth=2)

    ax.set_xlabel(r'Best-ITD [$\mu$s]', fontsize=8)
    ax.set_ylabel('CF', fontsize=8)
    ax.set_xlim(-4000, 4000)
    ax.set_xticks(np.array([-4000, 0, 4000]))
    ax.tick_params(axis='both', labelsize=8)
    # color bar

    ax_cmap2 = plt.subplot(gs[1, 1])
    cmap = mpl.cm.jet
    norm = mpl.colors.Normalize(vmin=0, vmax=h_binned.max())
    cb2 = mpl.colorbar.ColorbarBase(ax_cmap2, cmap=cmap,
                                    norm=norm,
                                    orientation='vertical')
    cb2.set_label('Neurons', fontsize=8)
    cb2.ax.tick_params(labelsize=8)

    axs += [ax]
    ax = plt.subplot(gs[2, 0])
    _label = ''
    if show_fitted:
        _label = 'model'
    ax.plot(itds * 1e6, ic_population_response_itd(cfs=cfs,
                                                   best_itds=best_itds,
                                                   weights=weights,
                                                   itds=itds,
                                                   can_function=can_function,
                                                   make_symmetric_itd=make_symmetric_itd,
                                                   sigma=sigma),
            label=_label,
            color="salmon")
    if target_response is not None:
        ax.plot(itds*1e6, target_response, label=target_label, color="indigo")
    ax.set_xlabel(r'ITD [$\mu$s]', fontsize=8)
    ax.set_xticks(np.array([-4000, 0, 4000]))
    ax.set_ylabel('Normalized Amplitude', fontsize=8)
    ax.set_ylim(0, 1.4)
    ax.tick_params(axis='both', labelsize=8)
    plt.legend(frameon=False, loc='upper right', prop={'size': 6})
    axs += [ax]
    ax = plt.subplot(gs[:, 3:])
    for _i in range(optimal_itds_binned.shape[1]):
        srf = np.array([_w * g_itd(itd=x,
                                   best_itd=_b_itd,
                                   sigma=_sigma,
                                   can_function=can_function,
                                   cf=_cf)
                        for _b_itd, _w, _cf, _sigma in zip(optimal_itds_binned[:, _i],
                                                           optimal_weights_binned[:, _i],
                                                           cfs_binned[_i] * np.ones(optimal_itds_binned[:, _i].shape),
                                                           optimal_sigmas_binned[:, _i])]).T
        srf = srf / np.nanmax(srf)
        mask = np.isnan(srf)
        srf[mask] = 0
        srf_thr = 0.1
        srf[srf < srf_thr] = None
        ax.plot(x * 1e6, srf + _i - srf_thr,
                color='k',
                alpha=0.05)

    ax.set_facecolor('xkcd:white')
    ax.set_xlabel(r'Best-ITD [$\mu$s]', fontsize=8)
    ax.set_yticks(np.arange(0, len(cfs_binned)))
    ax.set_yticklabels(cfs_binned.astype(int))
    ax.set_ylim(0, len(cfs_binned))
    ax.yaxis.tick_right()
    ax.set_ylabel('CF [Hz]', fontsize=8)
    ax.yaxis.set_label_position("right")
    ax.set_xticks(np.array([-4000, 0, 4000]))
    ax.tick_params(axis='both', labelsize=8)

    return fig_out


def generate_population(n_ac_neuron_per_band: int = 20,
                        cfs: np.array = None,
                        type: str = 'rand'):
    """
    this function generate specifics ipd distritions
    :param n_ac_neuron_per_band: int indicating number of neurons per cf
    :param cfs: numpy array indicating the cfs to be used
    :param type: str indicating the population to be modeled, 'rand', 'ramp_up', 'ramp_down', 'straight', gaussian
    :return: simulated population best ipds array (n x m indicating the neuron and the cf respectively)
    """
    best_ipds = None
    if type == 'rand':
        best_ipds = np.pi * np.random.random((n_ac_neuron_per_band, cfs.shape[0]))

    if type == 'gaussian':
        best_ipds = np.pi * np.random.randn(n_ac_neuron_per_band, cfs.shape[0])

    if type == 'ramp_up':
        best_ipds = np.ones((n_ac_neuron_per_band, cfs.shape[0]))
        for _i in range(best_ipds.shape[1]):
            best_ipds[:, _i] = (_i + 1) * (np.pi / 2) / best_ipds.shape[1]

    if type == 'ramp_down':
        best_ipds = np.ones((n_ac_neuron_per_band, cfs.shape[0]))
        for _i in range(best_ipds.shape[1]):
            best_ipds[:, _i] = (best_ipds.shape[1] - _i + 1) * (np.pi / 2) / best_ipds.shape[1]

    if type == 'straight':
        best_ipds = -np.pi / 2 * np.ones((n_ac_neuron_per_band // 2, cfs.shape[0]))
        best_ipds = np.concatenate((best_ipds, np.pi / 2 * np.ones((n_ac_neuron_per_band // 2, cfs.shape[0]))))
    if type == 'GuineaPig':
        n_stright = cfs.shape[0]//2
        n_random = cfs.shape[0] - n_stright
        best_ipds = np.pi * np.random.random((n_ac_neuron_per_band, n_random))
        straight = -np.pi / 2 * np.ones((n_ac_neuron_per_band // 2, n_stright))
        straight = np.concatenate((straight, np.pi / 2 * np.ones((n_ac_neuron_per_band // 2, n_stright))))
        best_ipds = np.hstack((straight, best_ipds))

    return best_ipds


def plot_ipd_activity_and_ac_population_response(cfs: np.array = None,
                                                 best_ipds: np.array = None,
                                                 itd: np.array = None,
                                                 fig_out: matplotlib.figure.Figure = None,
                                                 make_symmetric_ipd=True,
                                                 can_function: str = 'exp',
                                                 sigma: float = np.pi / 8,
                                                 n_bins=40,
                                                 grid=300j,
                                                 ):
    """
    plot neural activation and AC output as function of ipd
    :param cfs: numpy array with central frequencies
    :param best_ipds: nxm numpy array with best ipds of cochlear nucleus neurons where n is the corresponding cf and m
    total of best ipds
    :param itd: float with itd of target response
    :param fig_out: handle to pass figure
    :param make_symmetric_ipd: if true, best_ipds are assume to represent one hemisphere, therefore the used best_ipd
    array will be make symmetric.
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :param n_bins: number of bins in which ipd space will be divided
    :param grid: resolution of 3d grid
    :return: figure handle
    """

    sym_best_ipds, spikes = spikes_counts(cfs=cfs,
                                          itd=itd,
                                          best_ipds=best_ipds,
                                          can_function=can_function,
                                          sigma=sigma)

    h = np.empty((cfs.shape[0], n_bins))
    _edge = None
    for _idx in range(cfs.shape[0]):
        _counts, _edge = np.histogram(sym_best_ipds[:, _idx], bins=n_bins,
                                      range=(-np.pi, np.pi))
        h[_idx, :] = _counts

    _max_itd = max_ecological_itd(head_diameter=22/100)
    _max_ipd = max_ipd(cf=cfs, max_itd=_max_itd)
    _input_ipds = itd_to_ipd(cf=cfs, itd=itd)
    if fig_out is None:
        inch = 2.54
        fig_out = plt.figure(constrained_layout=False)
        fig_out.set_size_inches(10 / inch, 14 / inch)
        fig_out.subplots_adjust(hspace=0.5, top=0.98, bottom=0.08, left=0.15, right=.9)

    widths = [0.9]
    heights = [0.5, 1, 1.0]
    gs = fig_out.add_gridspec(ncols=1, nrows=3, width_ratios=widths,
                              height_ratios=heights)
    axs = []
    ax = plt.subplot(gs[0, 0])
    ax.plot(_edge[0:-1] / (2 * np.pi), np.sum(h, axis=0))
    ax.set_ylabel('Count')
    ax.set_xlim(-0.5, 0.5)
    axs += [ax]
    # plot distribution
    ax = plt.subplot(gs[1, 0])
    xx, yy = np.meshgrid(_edge * 0.5 / np.pi, cfs)
    c = ax.pcolor(xx, yy, h.astype(int))
    ax.plot(_max_ipd / (2 * np.pi), cfs, 'r')
    ax.plot(-_max_ipd / (2 * np.pi), cfs, 'r')
    ax.plot(_input_ipds / (2 * np.pi), cfs, color='b', marker='.', linestyle='None')
    ax.set_xlabel('Best IPD [cycles]')
    ax.set_ylabel('CF')
    ax.set_xlim(-0.5, 0.5)
    # color bar
    c_bar = fig_out.colorbar(c, ax=ax, format='%1d', aspect=40)
    c_bar.set_label('Neurons', fontsize=8)
    axs += [ax]
    # plot activity
    ax = plt.subplot(gs[2, 0])
    x = sym_best_ipds.reshape(-1, 1, order='F') / (2 * np.pi)
    y = np.repeat(cfs, sym_best_ipds.shape[0]).reshape(-1, 1)
    z = spikes.reshape(-1, 1, order='F')
    # max_distance = np.max(np.linalg.norm((x, y), axis=0))
    grid_x, grid_y = np.mgrid[-0.5:0.5:grid, y.min():y.max():grid]
    int_data = interpolate.griddata(points=np.hstack((x, y)), values=z,
                                    xi=(grid_x, grid_y),
                                    method='cubic', rescale=True)
    c = ax.pcolor(grid_x, grid_y, int_data.squeeze())
    ax.plot(_max_ipd / (2 * np.pi), cfs, 'r')
    ax.plot(-_max_ipd / (2 * np.pi), cfs, 'r')
    ax.set_xlabel('Best IPD [cycles]')
    ax.set_ylabel('CF')
    ax.set_xlim(-0.5, 0.5)
    ax.plot(_input_ipds / (2 * np.pi), cfs, color='b', marker='.', linestyle='None')
    axs += [ax]
    # color bar
    c_bar = fig_out.colorbar(c, ax=ax, format='%1d', aspect=40)
    c_bar.set_label('Spikes/sec', fontsize=8)
    #
    if make_symmetric_ipd:
        n_neurons_per_cf = best_ipds.shape[0] * 2
    else:
        n_neurons_per_cf = best_ipds.shape[0]
    ax.text(-0.45, 350,
            r"$N_{AC/freq}$:" + "{:} \n".format(n_neurons_per_cf) +
            r"$N_{CFs}$:" + "{:} \n".format(best_ipds.shape[1]) +
            r"ITD[ms]:" + "{:}".format(np.round(itd*1e3, decimals=1)),
            fontsize=8)

    return fig_out


def spikes_counts(cfs: np.array = None,
                  best_ipds: np.array = None,
                  itd: np.array = None,
                  make_symmetric_ipd=True,
                  can_function: str = 'exp',
                  sigma: float = np.pi / 8,
                  ):
    """
    compute spikes output for each neuron
    :param cfs: numpy array with central frequencies
    :param best_ipds: nxm numpy array with best ipds of cochlear nucleus neurons where n is the corresponding cf and m
    total of best ipds
    :param itd: float with itd of target response
    :param make_symmetric_ipd: if true, best_ipds are assume to represent one hemisphere, therefore the used best_ipd
    array will be make symmetric.
    :param can_function: either "cos" or "exp"
    :param sigma: float indicating the standard deviation for the exponential canonical function
    :return: figure handle
    """

    if make_symmetric_ipd:
        sym_best_ipds = np.empty((best_ipds.shape[0]*2, cfs.shape[0]))
    else:
        sym_best_ipds = np.empty((best_ipds.shape[0], cfs.shape[0]))

    for _idx in range(cfs.shape[0]):
        if make_symmetric_ipd:
            sym_best_ipds[:, _idx] = np.sort(np.concatenate((best_ipds[:, _idx].flatten(),
                                                             -best_ipds[:, _idx].flatten())))
        else:
            sym_best_ipds[:, _idx] = best_ipds[:, _idx].flatten()
    spikes = np.zeros(sym_best_ipds.shape)
    for _idx_cf, _cf in enumerate(cfs):
        spike_rate, _ipd = g_p_ipd(cf=_cf, itd=itd,
                                   best_ipd=sym_best_ipds[:, _idx_cf],
                                   can_function=can_function,
                                   sigma=sigma)
        spikes[:, _idx_cf] = spike_rate
    return sym_best_ipds, spikes


def frequency_binning(f_low: float = 150,
                      f_high: float = 850,
                      band_width: float = None,
                      cfs: np.array = None,
                      ipds: np.array = None,
                      sigmas: np.array = None,
                      weights: np.array = None):
    # bin best ipds within a bandwidth
    if sigmas is None:
        sigmas = np.ones(ipds.shape)

    _cfs = cfs.copy()
    if _cfs.ndim == 1:
        _cfs = _cfs[:, None]
    _ipds = ipds.copy()
    if _ipds.ndim == 1:
        _ipds = _ipds[:, None]

    _sigmas = sigmas.copy()
    if _sigmas.ndim == 1:
        _sigmas = _sigmas[:, None]

    cfs_binned = _cfs

    if weights is None:
        weights = np.ones(cfs_binned.shape)

    weights_binned = weights.copy()
    if band_width is None:
        band_width = np.mean(np.diff(cfs))
    for f in np.arange(f_low, f_high, band_width):
        _idx = np.logical_and(_cfs[0, :] >= f, _cfs[0, :] < f + band_width)
        if np.any(_idx):
            cfs_binned[:, _idx] = np.mean(cfs_binned[:, _idx], axis=1)[:, None]
            weights_binned[:, _idx] = np.mean(weights_binned[:, _idx], axis=1)[:, None]

    weights_binned = weights.copy()
    # clean cfs and ipds
    _idx = np.logical_and(cfs_binned >= f_low, _cfs < f_high + band_width)
    cfs_binned = cfs_binned[_idx]
    weights_binned = weights_binned[_idx]
    ipds_binned = _ipds[_idx]
    sigmas_binned = _sigmas[_idx]
    # convert to matrix format
    cfs_matrix,  ipds_matrix, weights_matrix = binned_cfs_ipds_to_matrix(cfs_binned, ipds_binned, weights_binned)
    _, sigmas_matrix, _ = binned_cfs_ipds_to_matrix(cfs_binned, sigmas_binned, weights_binned)
    return cfs_matrix, ipds_matrix, weights_matrix, sigmas_matrix


def frequency_binning_itd(f_low: float = 150,
                          f_high: float = 850,
                          band_width: float = 100,
                          cfs: np.array = None,
                          itds: np.array = None,
                          sigmas: np.array = None,
                          weights: np.array = None):
    # bin best ipds within a bandwidth
    _itds = itds.copy()
    _sigmas = sigmas.copy()
    cfs_binned = cfs.copy()
    weights_binned = weights.copy()
    for f in np.arange(f_low, f_high, band_width):
        _idx = np.logical_and(cfs[0, :] >= f, cfs[0, :] < f + band_width)
        if np.any(_idx):
            cfs_binned[:, _idx] = np.mean(cfs_binned[:, _idx], axis=1)[:, None]
            weights_binned[:, _idx] = np.mean(weights_binned[:, _idx], axis=1)[:, None]
    # clean cfs and ipds
    _idx = np.logical_and(cfs_binned >= f_low, cfs < f_high + band_width)
    cfs_binned = cfs_binned[_idx]
    weights_binned = weights_binned[_idx]
    itds_binned = _itds[_idx]
    sigmas_binned = _sigmas[_idx]
    # convert to matrix format
    cfs_matrix,  itds_matrix, weights_matrix = binned_cfs_itds_to_matrix(cfs_binned, itds_binned, weights_binned)
    _, sigmas_matrix, _ = binned_cfs_itds_to_matrix(cfs_binned, sigmas_binned, weights_binned)
    return cfs_matrix, itds_matrix, weights_matrix, sigmas_matrix


def binned_cfs_ipds_to_matrix(cfs_binned: np.array = None,
                              ipds_binned: np.array = None,
                              weights_binned: np.array = None):
    max_n_neurons = 0
    unique_cfs = np.unique(cfs_binned)
    for _cf in unique_cfs:
        _count = np.argwhere(cfs_binned == _cf).size
        max_n_neurons = _count if _count > max_n_neurons else max_n_neurons

    ipds_matrix = np.zeros((max_n_neurons, unique_cfs.shape[0]))
    ipds_matrix[:] = None
    weights_matrix = np.zeros((max_n_neurons, unique_cfs.shape[0]))
    weights_matrix[:] = 0

    for _k, _cf in enumerate(unique_cfs):
        _idx = np.argwhere(cfs_binned == _cf)
        ipds_matrix[np.arange(0, _idx.size), _k] = ipds_binned[_idx].squeeze()
        weights_matrix[np.arange(0, _idx.size), _k] = weights_binned[_idx].squeeze()
    return unique_cfs, ipds_matrix, weights_matrix


def binned_cfs_itds_to_matrix(cfs_binned: np.array = None,
                              itds_binned: np.array = None,
                              weights_binned: np.array = None):
    max_n_neurons = 0
    unique_cfs = np.unique(cfs_binned)
    for _cf in unique_cfs:
        _count = np.argwhere(cfs_binned == _cf).size
        max_n_neurons = _count if _count >= max_n_neurons else max_n_neurons

    itds_matrix = np.zeros((max_n_neurons, unique_cfs.shape[0]))
    itds_matrix[:] = None
    weights_matrix = np.zeros((max_n_neurons, unique_cfs.shape[0]))
    weights_matrix[:] = 0

    for _k, _cf in enumerate(unique_cfs):
        _idx = np.argwhere(cfs_binned == _cf)
        itds_matrix[np.arange(0, _idx.size), _k] = itds_binned[_idx].squeeze()
        weights_matrix[np.arange(0, _idx.size), _k] = weights_binned[_idx].squeeze()
    return unique_cfs, itds_matrix, weights_matrix


def neural_weight(central_frequencies: np.array = None,
                  max_itd: float = None,
                  model: str = 'stern_shear',
                  n_neurons: int = 200
                  ):
    p = np.zeros((n_neurons, central_frequencies.size))
    allowed_thau = np.zeros((n_neurons, central_frequencies.size))
    area = 0
    thau = np.linspace(-max_itd, max_itd, n_neurons)
    if model == 'pi_limit':
        # Nature McAlpine 2001
        for _j, _cf in enumerate(central_frequencies):
            _allowed_thau = np.logical_and(-0.5 / _cf <= thau, thau <= 0.5 / _cf)
            p[:, _j] = np.array(1. * _allowed_thau)
            allowed_thau[_allowed_thau, _j] = 1
        area = np.nansum(p, axis=0)

    elif model == 'hancock_delgutte':
        w = 0.19
        mu_1 = 0.23
        sigma_1 = 0.04
        mu_2 = 0.16
        sigma_2 = 0.19
        sigma_cf = 0.51
        mean_cf = 6.5
        f_bf = np.zeros(central_frequencies.shape)
        for _j, _cf in enumerate(central_frequencies):
            # mask = np.array(1. * (np.abs(thau) <= 0.5 / _cf))
            bp = np.abs(thau * _cf)
            p[:, _j] = (w / (sigma_1 * np.sqrt(2 * np.pi)) * np.exp(-(bp - mu_1) ** 2.0 / (2 * sigma_1 ** 2)) +
                        (1 - w) / (sigma_2 * np.sqrt(2 * np.pi)) * np.exp(
                        -(bp - mu_2) ** 2.0 / (2 * sigma_2 ** 2))
                        )

            f_bf[_j] = (1 / (_cf * sigma_cf * np.sqrt(2 * np.pi)) *
                        np.exp(-(np.log(_cf) - mean_cf) ** 2.0 / (2 * sigma_cf ** 2.0)))
            _allowed_thau = np.logical_and(-0.5 / _cf <= thau, thau <= 0.5 / _cf)
            allowed_thau[_allowed_thau, _j] = 1
            # p[:, _j] * p[:, _j] * mask
        # area = np.sum(p, axis=0)
        p = p * f_bf
        area = 1
    else:
        for _i, _t in enumerate(thau):
            if model == 'stern_shear':
                # JASA Stern and Shear 1996
                kh = 3000
                thau_lim = 0.0002
                kl = np.zeros(central_frequencies.shape)
                for _j, _cf in enumerate(central_frequencies):
                    kl[_j] = 0.1 * _cf ** 1.1 if _cf <= 1200 else 0.1 * 1200 ** 1.1

                if np.abs(_t) <= thau_lim:
                    p1 = np.exp(-2 * np.pi * kl * np.abs(thau_lim))
                    p2 = np.exp(-2 * np.pi * kh * np.abs(thau_lim))
                    p[_i, :] = (p1 - p2) / np.abs(thau_lim)
                else:
                    p1 = np.exp(-2 * np.pi * kl * np.abs(_t))
                    p2 = np.exp(-2 * np.pi * kh * np.abs(_t))
                    p[_i, :] = (p1 - p2) / np.abs(_t)
            if model == 'colburn':
                _c = 1
                # JASA Colburn 1977
                if np.abs(_t) <= 0.00015:
                    p[_i, :] = _c
                elif np.abs(_t) <= 0.0022:
                    p[_i, :] = _c * np.exp(-(np.abs(_t) - 0.00015) / 0.0006)
                else:
                    p[_i, :] = _c * 0.033 * np.exp(-(np.abs(_t) - 0.0022) / 0.0023)
            area = area + p[_i, :]
            allowed_thau[:] = 1
    # area = np.sum(p, axis=0)
    p = p / area
    allowed_thau[allowed_thau == 0] = None
    distribution = np.ones(p.shape) * thau[:, None] * allowed_thau
    return p, distribution
