import computational_model.modeling_distributions_tools as tools
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pickle
from environment import data_path_model, figures_path_model
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')


files = [data_path_model + 'BH-L-BFGS-B_100_ramp_down_fix_weights_True_cfs_15_results_itd.pkl',
         data_path_model + 'BH-L-BFGS-B_100_GuineaPig_fix_weights_True_cfs_15_results_itd.pkl',
         data_path_model + 'BH-L-BFGS-B_100_straight_fix_weights_True_cfs_15_results_itd.pkl',
         data_path_model + 'BH-L-BFGS-B_100_ramp_up_fix_weights_True_cfs_15_results_itd.pkl']

inch = 2.54

for _file in files:
    with open(_file, 'rb') as handle:
        results = pickle.load(handle)

    cfs = results.cfs[0]
    type = str(results.case[0])
    can_function = str(results.can_function[0])
    sigma = results.sigma[0]
    optimal_itds = results.best_itds[0]
    n_ac_neuron_per_band = results.n_ac_neuron_per_band[0]
    itds = np.linspace(-4000e-6, 4000e-6, 100)
    weights = results.weights[0]

    best_ipds = tools.generate_population(n_ac_neuron_per_band=n_ac_neuron_per_band,
                                          cfs=cfs, type=type)
    best_itds = best_ipds / (2 * np.pi * cfs)
    target_response = tools.ic_population_response_itd(cfs=cfs,
                                                       itds=itds,
                                                       best_itds=best_itds,
                                                       make_symmetric_itd=True,
                                                       can_function=can_function,
                                                       sigma=np.mean(sigma))

    optimal_response = tools.ic_population_response_itd(cfs=cfs,
                                                        itds=itds,
                                                        best_itds=optimal_itds,
                                                        make_symmetric_itd=False,
                                                        can_function=can_function,
                                                        sigma=sigma,
                                                        weights=weights)

    _max_pop = optimal_response.max()
    _min_pop = optimal_response.min()
    optimal_response /= _max_pop
    _max = target_response.max()
    _min = target_response.min()
    target_response /= _max
    _itd_penalization = 1
    error = np.sum(np.square(np.diff((optimal_response - target_response))))
    unexplained_var = np.var(optimal_response - target_response)
    total_var = np.var(target_response)
    RSquared = 1 - unexplained_var / total_var

    f_out_target = tools.plot_itd_distribution_and_ac_population_response(
        cfs=cfs,
        best_itds=best_itds,
        itds=itds,
        sigma=np.mean(sigma),
        target_response=None,
        make_symmetric_itd=True,
        head_diameter=None,
        freq_band_width=50,
        show_centroid=False
    )
    _condition = 'distribution_{:}_neurons_{:}_can_fun_{:}_target_itd'.format(type,
                                                                              best_itds.size * 2,
                                                                              can_function)

    axes = f_out_target.get_axes()
    if can_function == 'exp':
        axes[-2].text(-4000, 0.0, r'{:}, $\sigma$ = {:.1f}'.format(can_function,
                                                                   round(np.mean(sigma) * 180 / np.pi, ndigits=1)),
                      fontsize=8)
    else:
        axes[-2].text(-4000, 0.0, r'{:}'.format(can_function), fontsize=8)

    f_out_target.set_size_inches(8 / inch, 10 / inch)
    f_out_target.subplots_adjust(top=0.98, bottom=0.1, wspace=1, left=0.14, right=.86)

    f_out_target.savefig(figures_path_model + _condition + '.png')
    f_out_target.savefig(figures_path_model + _condition + '.pdf')
    plt.close(f_out_target)

    f_out = tools.plot_itd_distribution_and_ac_population_response(cfs=cfs,
                                                                   best_itds=optimal_itds,
                                                                   itds=itds,
                                                                   target_response=target_response,
                                                                   weights=weights,
                                                                   sigma=sigma,
                                                                   make_symmetric_itd=False,
                                                                   head_diameter=None,
                                                                   freq_band_width=50,
                                                                   show_centroid=False
                                                                   )

    axes = f_out.get_axes()
    axes[-2].text(-4000, 0.1, '$R^2$ = {:.2f}'.format(RSquared), fontsize=8)
    if can_function == 'exp':
        axes[-2].text(-4000, 0.0, r'{:}, $\sigma$ = {:.1f}'.format(can_function,
                                                                   round(np.mean(sigma) * 180 / np.pi, ndigits=1)),
                      fontsize=8)
    else:
        axes[-2].text(-4000, 0.0, r'{:}'.format(can_function), fontsize=8)

    error = tools.error_measure_itd(fitted=optimal_itds,
                                    cfs=cfs,
                                    target_response=target_response,
                                    itds=itds,
                                    weights=weights,
                                    do_plots=False,
                                    can_function=can_function,
                                    make_symmetric_itd=False,
                                    sigma=sigma)

    _condition = 'distribution_{:}_neurons_{:}_can_fun_{:}_itd'.format(type,
                                                                       optimal_itds.size,
                                                                       can_function)
    f_out.set_size_inches(8 / inch, 10 / inch)
    f_out.subplots_adjust(top=0.98, bottom=0.1, wspace=1, left=0.14, right=.86)
    f_out.savefig(figures_path_model + _condition + '.png')
    f_out.savefig(figures_path_model + _condition + '.pdf')
    plt.show()
    plt.close(f_out)
