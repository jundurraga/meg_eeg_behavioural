import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import computational_model.modeling_distributions_tools as model_tools
import pandas as pd
from scipy.optimize import curve_fit
from scipy.stats import pearsonr
from environment import data_path_model, figures_path_model, data_path_meg
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')
color = list(plt.cm.tab10.colors)


def func(x, a, b, c):
    return a * np.exp(-b * abs(x)**1) + c


files = []
data_path = data_path_model + \
            'original_BH-L-BFGS-B_100_interpolated_fitting_data_meg_exp_True_cfs_15_results_model_itd_domain.pkl'
target_path = data_path_meg + 'interpolated_fitting_data_meg.csv'


# read data to fit
ipd_data = np.genfromtxt(target_path,
                         delimiter=',',
                         skip_header=True)
_idx = np.argsort(ipd_data[:, 1])
fitted_target_response = ipd_data[_idx, 2]
itds = ipd_data[_idx, 1] / 1000

with open(data_path, 'rb') as handle:
    results = pd.read_pickle(handle)

v1 = np.arange(0, itds.size // 2)
v2 = np.arange(itds.size // 2, itds.size)
popt, pcov = curve_fit(func, itds[v1], fitted_target_response[v1], maxfev=10000)
target_response_l = func(itds[v1], *popt)
popt, pcov = curve_fit(func, itds[v2], fitted_target_response[v2], maxfev=10000)
target_response_r = func(itds[v2], *popt)
errors = []
sigmas = []

inch = 2.54
fig_histo = plt.figure(constrained_layout=False)
fig_histo.set_size_inches(12 / inch, 14 / inch)
fig_histo.subplots_adjust(hspace=0.4, top=0.98, bottom=0.15, left=0.15, right=.95, wspace=0.05)
gs = fig_histo.add_gridspec(ncols=1, nrows=1, wspace=0.1)
axh = plt.subplot(gs[0, 0])
axins = inset_axes(axh, width=.5, height=0.3)
axins.spines['right'].set_visible(False)
axins.spines['top'].set_visible(False)
axins.set_yticks([])
axins.axes.get_yaxis().set_visible(True)
axins.set_ylabel("Spikes/second", fontdict={'size': 6})
axins.set_xlabel("Best-ITD [cycles]", fontdict={'size': 6})
axins.set_xlim(-0.5, 0.5)
axins.set_xticks(np.linspace(-0.5, 0.5, 3))
axins.tick_params(axis='both', labelsize=6)

for _i, (_ir, _row) in enumerate(results.iterrows()):
    cfs = _row.cfs
    type = str(_row.case)
    can_function = str(_row.can_function)
    sigma = _row.sigma
    optimal_itds = _row.best_itds
    optimal_weights = _row.weights
    itds = _row.itds
    optimal_ipds = model_tools.itd_to_ipd(cf=cfs, itd=optimal_itds)

    fitted_target_response = _row.target_response
    target_response = fitted_target_response
    if _row.case == 'original':
        target_response = fitted_target_response
    if _row.case == 'only_damping':
        target_response = fitted_target_response - np.concatenate((target_response_l, target_response_r))
        target_response += 1 - target_response.max()

    if _row.case == 'only_linear':
        target_response = np.concatenate((target_response_l, target_response_r))
        target_response += 1 - target_response.max()

    if _row.case == 'control_only_damping':
        target_response = -np.concatenate((target_response_l, target_response_r))
        target_response += 1 - target_response.max()
    if _row.case == 'control_only_linear':
        target_response -= np.concatenate((target_response_l, target_response_r))
        target_response *= -1
        target_response += 1 - target_response.max()
    if _row.case == 'control_original':
        target_response *= -1
        target_response += 1 - target_response.max()

    optimal_response = model_tools.ic_population_response_ipd(cfs=cfs,
                                                              itds=itds,
                                                              best_ipds=optimal_ipds,
                                                              make_symmetric_ipd=False,
                                                              can_function=can_function,
                                                              sigma=sigma)
    _max_pop = optimal_response.max()
    _min_pop = optimal_response.min()
    optimal_response /= _max_pop
    _max = target_response.max()
    _min = target_response.min()
    target_response /= _max
    _itd_penalization = 1

    # compute explained variance
    _target_itd = [0, 0.0005, 0.001, 0.0015, 0.002, 0.0025, 0.003, 0.0035, 0.004]
    itd_idx = np.array([np.argmin(np.abs(itds - _t)) for _t in _target_itd])
    itd_idx_data = np.array([np.argmin(np.abs(itds - _t)) for _t in _target_itd])
    error = np.sum(np.square(np.diff((optimal_response - target_response)[itd_idx_data])))
    unexplained_var = np.var((optimal_response - target_response)[itd_idx_data])
    total_var = np.var(target_response[itd_idx_data])
    RSquared = 1 - unexplained_var / total_var
    pcorr_1 = pearsonr(optimal_response[itd_idx_data], target_response[itd_idx_data])
    RSquared = pcorr_1.statistic ** 2
    p_value = pcorr_1.pvalue
    print('p-value: {:}'.format(p_value))

    # generate plot
    human_diameter = 19 / 100
    f_out, _data = model_tools.plot_ipd_distribution_and_ac_population_response(
        cfs=cfs,
        best_ipds=optimal_ipds,
        weights=optimal_weights,
        itds=itds,
        target_response=target_response,
        target_label='MEG-GFP',
        can_function=can_function,
        sigma=sigma,
        make_symmetric_ipd=False,
        head_diameter=human_diameter,
        freq_band_width=50,
        show_centroid=False
    )

    axes = f_out.get_axes()
    axes[-2].text(-4000, 0.2, '$R^2$ = {:.2f}'.format(RSquared), fontsize=6)
    if can_function == 'exp':
        axes[-2].text(-4000, 0.1, r'$\sigma$ = {:.1f}'.format(round(np.mean(sigma) * 180 / np.pi, ndigits=1)),
                      fontsize=6)
    else:
        axes[-2].text(-4000, 0.2, r'{:}'.format(can_function), fontsize=6)

    _error = model_tools.error_measure(fitted=optimal_ipds,
                                       cfs=cfs,
                                       target_response=target_response,
                                       itds=itds,
                                       do_plots=False,
                                       can_function=can_function,
                                       sigma=sigma,
                                       fix_weights=True,
                                       make_symmetric_ipd=False,
                                       weights=optimal_weights)
    errors.append(_error)
    sigmas.append(np.mean(sigma))
    _condition = 'meg_data_fitting_{:}_{:}_can_fun_{:}_sigma_{:}_itd_to_ipd'.format(
        _row.case,
        optimal_ipds.size,
        can_function, round(np.mean(sigma) * 180 / np.pi))
    f_out.set_size_inches(11.4 / inch, 10 / inch)
    f_out.subplots_adjust(top=0.98, bottom=0.1, wspace=1, left=0.1, right=.9)
    f_out.savefig(figures_path_model + _condition + '.png')
    f_out.savefig(figures_path_model + _condition + '.pdf')
    h = np.empty((cfs.shape[0], 70))
    for _idx in range(optimal_ipds.shape[1]):
        _counts, _edge = np.histogram(optimal_ipds, bins=70,
                                      range=(-np.pi, np.pi))
        h[_idx, :] = _counts
    x = np.linspace(-0.5, 0.5, 100)
    _edge += np.mean(np.diff(_edge)) / 2
    ipd_axis = _edge[0:-1] / (2 * np.pi)
    h_total = np.sum(h, axis=0)
    neural_activity = np.zeros(x.shape)
    weights = np.ones(optimal_ipds.shape)
    for _ix in range(optimal_ipds.shape[1]):
        srf = np.array([h_total.max() * _w * model_tools.g_p(ipd=np.pi * x / 0.5,
                                                             best_ipd=_b_ipd,
                                                             sigma=_sigma,
                                                             can_function=can_function) / 100
                        for _b_ipd, _w, _sigma in zip(optimal_ipds[:, _ix],
                                                      weights[:, _ix],
                                                      sigma[:, _ix])]).T
        neural_activity += np.nansum(srf, axis=1)
    bin_width = np.mean(np.diff(ipd_axis))
    axh.bar(ipd_axis, h_total / h_total.max(),
            color=color[_i],
            width=bin_width,
            edgecolor=color[_i],
            alpha=0.2)
    label = r'$R^2$ = {:.2f}, $\sigma$ = {:.1f}, p {:} {:.3f}'.format(
        RSquared,
        round(np.mean(sigma) * 180 / np.pi, ndigits=1),
        '$<$' if p_value < 0.001 else '=',
        0.001 if p_value < 0.001 else p_value
    )
    axh.plot(x, neural_activity / neural_activity.max(),
             color=color[_i],
             linestyle=':',
             alpha=1,
             linewidth=3,
             label=label)
    axh.set_ylim(0, 1.5)
    axins.plot(x,
               model_tools.g_p(ipd=np.pi * x / 0.5,
                               best_ipd=np.pi / 2,
                               sigma=np.mean(sigma),
                               can_function=can_function) / 100,
               color=color[_i])
axh.legend(loc='upper left', fontsize=8, frameon=False, prop={'size': 6})

axh.set_xlabel("Best-ITD [cycles]", fontsize=8)
axh.set_ylabel("Normalized Counts", fontsize=8)
axh.set_yticks([])
axh.set_xlim(-0.5, 0.5)
axh.set_xticks(np.linspace(-0.5, 0.5, 5))
axh.tick_params(axis='both', labelsize=6)
axh.spines['right'].set_visible(False)
axh.spines['top'].set_visible(False)
fig_histo.set_size_inches(12 / inch, 10 / inch)
fig_histo.subplots_adjust(top=0.98, bottom=0.1, wspace=1, left=0.1, right=.95)

fig_histo.savefig(figures_path_model + 'histograms_sigmas_itd_to_ipd.png')
fig_histo.savefig(figures_path_model + 'histograms_sigmas_itd_to_ipd.pdf')
sigmas = np.array(sigmas) * 180 / np.pi
plt.figure()
plt.plot(sigmas, errors)
plt.show()
