import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import computational_model.modeling_distributions_tools as model_tools
import pandas as pd
from scipy.stats import pearsonr
from environment import data_path_model, figures_path_model, data_path_meg
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')
color = list(plt.cm.tab10.colors)


def func(x, a, b, c):
    return a * np.exp(-b * abs(x)**1) + c


files = []
data_path_itd = data_path_model + \
                'original_BH-L-BFGS-B_100_interpolated_fitting_data_meg_exp_True_cfs_15_results_model_itd_domain.pkl'
data_path_ipd = data_path_model + \
                'original_BH-L-BFGS-B_100_interpolated_fitting_data_meg_exp_True_cfs_15_results_model_ipd_domain.pkl'
target_path = data_path_meg + 'interpolated_fitting_data_meg.csv'


# read data to fit
ipd_data = np.genfromtxt(target_path,
                         delimiter=',',
                         skip_header=True)
_idx = np.argsort(ipd_data[:, 1])
fitted_target_response = ipd_data[_idx, 2]
itds = ipd_data[_idx, 1] / 1000

with open(data_path_itd, 'rb') as handle:
    results_itd = pd.read_pickle(handle)
with open(data_path_ipd, 'rb') as handle:
    results_ipd = pd.read_pickle(handle)

errors = []
sigmas = []

inch = 2.54
results_itd = results_itd.query('case == "original"')
results_itd['sigma_itd'] = results_itd['sigma'].apply(lambda x: np.round(np.unique(x) * 180 / np.pi, decimals=1)[0])
results_itd = results_itd.query('sigma_itd == 36.0')

results_ipd = results_ipd.query('case == "original"')
results_ipd['sigma_ipd'] = results_ipd['sigma'].apply(lambda x: np.round(np.unique(x) * 180 / np.pi, decimals=1)[0])
results_ipd = results_ipd.query('sigma_ipd == 22.5')

result = pd.merge(results_itd, results_ipd,
                  on=['n_ac_neuron_per_band', 'can_function', 'case', 'method',
                      ], how='left')
for _i, (_ir, _row) in enumerate(result.iterrows()):
    cfs = _row.cfs_x
    type = str(_row.case)
    can_function = str(_row.can_function)
    itds_data = ipd_data[_idx, 1] / 1e6
    target_response = ipd_data[_idx, 2]
    sigma_itd = _row.sigma_x
    sigma_ipd = _row.sigma_y
    optimal_itds = _row.best_itds
    optimal_ipds = _row.best_ipds
    optimal_weights = _row.weights_x
    itds = np.arange(-0.004, 0.004, 0.00001)
    optimal_ipds_from_itds = model_tools.itd_to_ipd(cf=cfs, itd=optimal_itds)

    fitted_target_response = _row.target_response_x
    target_response = fitted_target_response
    if _row.case == 'original':
        target_response = fitted_target_response

    optimal_response_itd_to_ipd = model_tools.ic_population_response_ipd(cfs=cfs,
                                                                         itds=itds,
                                                                         best_ipds=optimal_ipds_from_itds,
                                                                         make_symmetric_ipd=False,
                                                                         can_function=can_function,
                                                                         sigma=sigma_itd)
    optimal_response_ipd = model_tools.ic_population_response_ipd(cfs=cfs,
                                                                  itds=itds,
                                                                  best_ipds=optimal_ipds,
                                                                  make_symmetric_ipd=False,
                                                                  can_function=can_function,
                                                                  sigma=sigma_ipd)
    _target_itd = [0, 0.0005, 0.001, 0.0015, 0.002, 0.0025, 0.003, 0.0035, 0.004]
    itd_idx = np.array([np.argmin(np.abs(itds - _t)) for _t in _target_itd])
    itd_idx_data = np.array([np.argmin(np.abs(itds_data - _t)) for _t in _target_itd])
    plt.figure()
    plt.plot(itds, optimal_response_ipd)
    plt.plot(itds[itd_idx], optimal_response_ipd[itd_idx], 'o')
    plt.plot(itds, optimal_response_itd_to_ipd)
    plt.plot(itds[itd_idx], optimal_response_itd_to_ipd[itd_idx], 's')
    plt.plot(itds_data, target_response)
    plt.plot(itds_data[itd_idx_data], target_response[itd_idx_data], 'v')
    plt.show()
    t = lambda n, r: r * np.sqrt(n - 2) / np.sqrt(1 - r ** 2.0)
    pcorr_1 = pearsonr(optimal_response_ipd[itd_idx], optimal_response_itd_to_ipd[itd_idx])
    pcorr_2 = pearsonr(target_response[itd_idx_data], optimal_response_itd_to_ipd[itd_idx])
    print('sigma ipd: {:}, sigma itd: {:}, R2: {:}, p_val: {:}, t: {:}'.format(_row.sigma_ipd,
                                                                               _row.sigma_itd,
                                                                               pcorr_1.statistic ** 2,
                                                                               pcorr_1.pvalue,
                                                                               t(pcorr_1._n, pcorr_1.statistic)))
    print('sigma data, sigma itd: {:}, R2: {:}, p_val: {:}, t: {:}'.format(_row.sigma_itd,
                                                                           pcorr_2.statistic ** 2,
                                                                           pcorr_2.pvalue,
                                                                           t(pcorr_2._n, pcorr_2.statistic)))

