# Introduction

This repository contains all processing and statistical analysis scripts to reproduce the results
of the manuscript entitled **"The neural representation of an auditory
spatial cue in primate cortex"** as close as possible.

# Data structure

Each directory in the root directory contains sub-directories with the scripts, data, and figures pertinent to the 
modality defined by the directory name. 
All the statistical analyses can be found in the 'statistical_analyses' folder, in which the analyses for each specific 
modality are given by the folder name.

All .sqlite databases are structured hierarchically by the following tables: 'subjects', 'measurement_info', 
and 'stimuli'.
All other tables will share the same level of hierarchy, in which the specific rows are linked to each subject,
measurement, and stimuli by the columns **'id_subject'**, **'id_measurement'**, and **'id_stimuli'**.

All R, python, and matlab scripts will access the different databases (or csv files) directly to generate the different figures and analyses.


# Get ready
Before starting, make sure to install python 3.11 and all the requirements listed in requirements.txt

```commandline
pip install -r requirements.txt 
```

You also need to install [R](https://www.r-project.org/) and all the packages used in each R file.

# Precomputed data
The entire collection of scripts alongside with generated data and figures can be accessed in the following link
https://doi.org/10.25949/25585946

The generated data can be found (for each modality) within the folder 'data'.
For example, the data from the MEG analyses will be located within './meg_processing/data/', whilst the data from the EEG
analyses will be located within './eeg_processing/data/'

# Full analysis 
Once you have obtained all the data, and you have set up the environment for MEG and EEG (see below)
data you can run the following script to perform a full analysis.

```commandline
python run_analyses.py
```

Alternatively, you can run each script step by step as described below.

## MEG Data
### To reproduce the results you should access the raw data (approx. 200GB)
The data can be downloaded from the following link. Alternatively you can contact the authors of the associated 
article.

https://doi.org/10.25949/25585946

### Once you have obtained the data
Configure the environment by editing environment.py file so the subjects_dir and subjects_mri_dir aim the correct 
folders

```commandline
subjects_dir = '/your_data_directory' + '/MEG/ITD-FR/ME178/'
subjects_mri_dir = '/your_data_directory' + '/MEG/ITD-FR/ME178/mri/'
```

### Run MEG pipeline at sensor level for all subjects

#### ITD-FR data
Generate ITD-FR database
```commandline
python ./meg_processing/meg_sensor_database.py
```
This will generate the database with the data at the sensor level.
You can now run the following scripts to generate figures

- Figure 1G and 1H
```commandline
python ./meg_processing/plot_freq_time_itd_fr_meg.py
```
- Figure 1F
```commandline
python ./meg_processing/plots_topo_itm_fr_meg.py
```

#### PLV analysis at sensor level for sub-harmonic, ITD-FR, and ASSR
- Figure S1A
```commandline
python ./meg_processing/plot_plv_frequency_domain_waveforms_meg.py
```

- Figure S1B
```commandline
python ./meg_processing/plots_topo_plv_meg.py
```

#### MEG Sinusoidal + exponential fitting 
To fit and obtain fitting parameters for statistical test of damping pattern you can now run
```commandline
python ./meg_processing/damping_fitting_meg.py
```

#### ASSR data
Generate ASSR database (original epoch length)
```commandline
python ./meg_processing/meg_sensor_database.py
```


- Figure S2E
```commandline
python ./meg_processing/plots_topo_assr_meg.py
```

- Figure S2F and G
```commandline
python ./meg_processing/plot_freq_time_assr_meg.py
```

#### Temporal phase analysis
Generate database

```commandline
python ./meg_processing/meg_sensor_assr_temporal_phase.py
```

- Figure S2H
```commandline
python ./meg_processing/plot_freq_time_assr_meg_phase.py
```

### MEG stats at sensor level
You should have all the necessary data to run the statistical analysis at sensor level.
You will need [R](https://www.r-project.org/) and all the packages used in the file. 
```commandline 
./statistical_analyses/meg/meg_stats_sensor_level.R
```
The code performs all the statistical analysis, fittings, and tests on the MEG data at the sensor level
It will also generate figure 2B and figure 3B.


### Source analysis and plots
#### Save preprocess data
The code below will generate all the MEG data needed to generate the source space.
Some data will be already generated in the MEG data directory to speed up the process. 
It can be removed to re-analyse everything from scratch.
The sensor data will be processed and saved into a sqlite database in the data folder

```commandline
python ./meg_processing/meg_preparation_pipeline.py
``` 
This may take a while, but it will make the preprocessed data available for the source reconstruction, 
speeding up everything significantly.

#### Z-Score from time series
Now you can estimate the source activation and generate the group average plots by running the following script
```commandline
python ./meg_processing/meg_group_average_from_time_sources.py
``` 
In this case, the time-course of each source is computed to estimate the response amplitude by the RMS
of each source across time.
This script will generate all panels shown in figure 2C and figure 3C

#### Z-Score, PLV, and Phase from the frequency-domain
Next, you can run the following script to estimate the z-scores, phase-locking values, and
the phase at the source level. 
The scripts will compute the DFT of the frequency of interest in each epoch for each subject. 
The DFT is then projected to the source space and passed to statistical tests providing PLV, phase, and p-values.

```commandline
python ./meg_processing/meg_group_average_phase_locking_and_latency.py
```

The results of the analysis will be saved into a sqlite database. 
Now you can run plot the results by running the following R script in R studio.
This R script will generate figure 2D and E as well as figures 3D and E.
```commandline
./statistical_analyses/meg/meg_sources_phase_locking_phase_latency_analysis.R
```
#### Time series Videos
You have already run the following command, but if not, then it needs to be run

```commandline
python ./meg_processing/meg_preparation_pipeline.py
``` 

Next, generate the videos by running
```commandline
python ./meg_processing/meg_group_average_time_course_videos.py
```
These are the videos shown in SV1.

#### Feature extraction at anatomical level
To reduce the information contained within each anatomical region (many sources per region) and obtain
a single feature per anatomical region for each subject and condition, you can run the following script.

```commandline
python ./meg_processing/meg_group_average_time_course_roi.py
```
This script will extract the first principal component for each anatomical region, scaled to match the average
power within the anatomical region.
Then, the FFT is computed and the amplitude of the frequencies of interest are then saved into a sqlite database.

Next you can run the statistical analyses at the source level by running the following script

```commandline
./statistical_analyses/meg/meg_sources_analysis.R
```
This script will generate the statistical tests on the sources reported in the manuscript.


## EEG Data
### To reproduce the results you should access the raw data (approx. 180GB)
The data can be downloaded from the following link. Alternatively you can contact the authors of the associated 
article.

https://doi.org/10.25949/25585946
 
### Once you have got the data
Configure environment.py file so the eeg_data_directory aim the correct folder

```commandline
eeg_data_directory = '/your_eeg_data_directory/'
```

### Run EEG pipeline for all subjects
By running the code below you will generate all the EEG data used for the analysis and figures.
#### ITD-FR database 

```commandline
python ./eeg_processing/itd_fr_pipeline.py
```
This will generate the sqlite database with the data at the sensor level.
You can now run the following scripts to generate figures

- Figure 1D and 1E
```commandline
python ./eeg_processing/plot_freq_time_itd_fr_eeg.py
```
- Figure 1C
```commandline
python ./eeg_processing/plots_potentials_itd_fr_eeg.py
```

#### EEG Sinusoidal + exponential fitting
To obtain the fitting parameters for statistical test of damping pattern you can now run
```commandline
python ./eeg_processing/damping_fitting_eeg.py
```

#### ASSR database
Generate ASSR database (full epoch length)
```commandline
python ./eeg_processing/assr_pipeline.py
```

#### ASSR figures

- Figure 2SA
```commandline
python ./eeg_processing/plots_potentials_assr_eeg.py
```

- Figure 2SB and C
```commandline
python ./eeg_processing/plot_freq_time_assr_eeg.py
```

#### Temporal phase
Generate database
```commandline
python ./eeg_processing/itd_fr_pipeline_temporal_phase.py
```

- Figure S2D

```commandline
python ./eeg_processing/plot_freq_time_assr_phase_eeg.py
```

### EEG stats

You should have all the necessary data to run the statistical analysis at sensor level.
You will need [R](https://www.r-project.org/) and all the packages used in the file. 
```commandline 
./statistical_analyses/eeg/eeg_stats.R
```
The code performs all the statistical analysis, fittings, and tests on EG data at sensor level
It will also generate figure 2a and figure S1a.


## Behavioural analysis

### Fit sin-exponential model first

```commandline
python ./behavioural_processing/damping_fitting_behavioural.py
``` 

### Run the statistical analysis
The following script (to be run in R studio) will perform the statistical tests and generate figure 4B.  

```commandline
./statistical_analyses/behavioural/beahvioural_ranking_analysis.R
```

## Comparison between neural and behavioural responses
To perform the statistical analyses and produce figure 4C and D you can run the 
following script in R studio.

```commandline
./statistical_analyses/group_analysis/meg_eeg_behavioural_analysis.R
```

## Computational model

Now you should have generated the data to fit the neural distribution. 
To proceed, you can run the following script


### Fitting human data in the IPD space
To fit the data in the IPD domain, 'pi-limit' model, you need to run the following script

```commandline
python ./computational_model/modeling_distributions_damping_ipd_space.py
```


### Fitting human data in the ITD space
To fit the data in the ITD domain, 'delay-line' model, you need to run the following script

```commandline
python ./computational_model/modeling_distributions_damping_itd_space.py
```

### Generating plots for different canonical functions

To generate the panels shown in figure 5A-C and the histograms shown in Figure S4A you can run 
```commandline
python ./computational_model/plot_results_neural_data_model_ipd.py
```

To generate the panels shown in figure 5D-F and the histograms shown in Figure S4B you can run

```commandline
python ./computational_model/plot_results_neural_data_model_itd.py
```
To generate the panels shown in figure 5G-I you can run

```commandline
python ./computational_model/plot_results_neural_data_model_itd_in_ipd.py
```

### Fitting simulated neural distributions
To assess how well the fitting algorithm converged to the general solution, we generated 
several target distributions.
The scripts illustrating these simulations are the following 

```commandline
python ./computational_model/plot_results_neural_data_model_itd_to_ipd.py
```
This script will fit the model in the ipd-space. 

To plot the results, you can now run 
```commandline
python ./computational_model/plot_results_simulated_distributions_ipd.py
```
This script will generate the panels of figure S5

Similarly, the test and plots for the itd-space you can run
```commandline
python ./computational_model/modeling_distributions_damping_test_itd.py
```
and to generate the plots
```commandline
python ./computational_model/plot_results_simulated_distributions_itd.py
```


## Comparison with Macaque data
The comparisons between the human data and the macaque data can be obtained by running the next
script.

```commandline
python ./macaque_comparison/plot_results_macaque_data.py
python ./macaque_comparison/plot_results_macaque_data_itd_to_ipd.py
```
This script will generate the panels reported in figures 6A-H

## Comparison with IPD-FR (Undurraga et al. 2016)
To perform the comparison between the model prediction and the data from Undurraga et al. 2016 you can run the 
following script in R studio.

```commandline
./statistical_analyses/comparison_undurraga_2016/comparison_with_undurraga_2016.R
```