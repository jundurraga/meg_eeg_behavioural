import pathlib
import os
this_file_path = str(pathlib.Path(__file__).parent.resolve())
# R settings
r_script_binary_path = '/usr/bin/Rscript'

# MEG settings
subjects_dir = '/media/jundurraga/90720477720463F61/Measurements/damping_data_test/MEG/ITD-FR/ME178/'
subjects_mri_dir = '/media/jundurraga/90720477720463F61/Measurements/damping_data_test/MEG/ITD-FR/ME178/mri/'
root_directory = str(pathlib.Path(subjects_dir).parent.parent.parent) + os.sep
data_path_meg = this_file_path + os.sep + 'meg_processing' + os.sep + 'data' + os.sep
figures_path_meg = this_file_path + os.sep + 'meg_processing' + os.sep + 'figures' + os.sep

# EEG settings
eeg_data_directory = '/media/jundurraga/90720477720463F61/Measurements/damping_data_test/EEG/'
data_path_eeg = this_file_path + os.sep + 'eeg_processing' + os.sep + 'data' + os.sep
figures_path_eeg = this_file_path + os.sep + 'eeg_processing' + os.sep + 'figures' + os.sep

# Behavioural settings
data_path_behavioural = this_file_path + os.sep + 'behavioural_processing' + os.sep + 'data' + os.sep
figures_path_behavioural = this_file_path + os.sep + 'behavioural_processing' + os.sep + 'figures' + os.sep

# Computational model settings
data_path_model = this_file_path + os.sep + 'computational_model' + os.sep + 'data' + os.sep
figures_path_model = this_file_path + os.sep + 'computational_model' + os.sep + 'figures' + os.sep

# Macaque comparisons settings
data_path_macaque = this_file_path + os.sep + 'macaque_comparison' + os.sep + 'data' + os.sep
figures_path_macaque = this_file_path + os.sep + 'macaque_comparison' + os.sep + 'figures' + os.sep


if not os.path.exists(data_path_meg):
    os.makedirs(data_path_meg)
if not os.path.exists(data_path_eeg):
    os.makedirs(data_path_eeg)
if not os.path.exists(data_path_behavioural):
    os.makedirs(data_path_behavioural)
if not os.path.exists(data_path_model):
    os.makedirs(data_path_model)
if not os.path.exists(data_path_macaque):
    os.makedirs(data_path_macaque)

if not os.path.exists(figures_path_meg):
    os.makedirs(figures_path_meg)
if not os.path.exists(figures_path_eeg):
    os.makedirs(figures_path_eeg)
if not os.path.exists(figures_path_behavioural):
    os.makedirs(figures_path_behavioural)
if not os.path.exists(figures_path_model):
    os.makedirs(figures_path_model)
if not os.path.exists(figures_path_macaque):
    os.makedirs(figures_path_macaque)
