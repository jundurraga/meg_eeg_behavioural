from computational_model import modeling_distributions_tools as tools
from meg_processing.tools import fdr_pvalue
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import numpy as np
import pandas as pd
import scipy.io
from scipy.stats import pearsonr
from scipy.signal import savgol_filter
from environment import data_path_model, data_path_macaque, figures_path_macaque, data_path_meg
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')


def t_value(r, n):
    t = r * np.sqrt(n - 2) / np.sqrt(1 - r ** 2)
    return t


model_data_path = data_path_model +\
                  'original_BH-L-BFGS-B_100_interpolated_fitting_data_meg_exp_True_cfs_15_results_model_itd_to_ipd_domain.pkl'
macaque_data = scipy.io.loadmat(data_path_macaque + 'Macaque_pdata.mat')
_data_file = data_path_meg + 'interpolated_fitting_data_meg.csv'

# read data
ipd_data = np.genfromtxt(_data_file,
                         delimiter=',',
                         skip_header=True)

_idx = np.argsort(ipd_data[:, 1])
target_response = ipd_data[_idx, 2]
# read model data
with open(model_data_path, 'rb') as handle:
    results = pd.read_pickle(handle)

f_low = 200
f_high = 850
band_width = 50
plots_band_width = 100
macaque_diameter = 2 * 0.35 / (2 * np.pi)
human_diameter = 19/100
macaque_max_itd = 420
human_max_itd = 700
n_bins = 30
inch = 2.54
results['unique_sigma'] = results['sigma'].apply(lambda x: np.round(np.unique(x) * 180 / np.pi, decimals=1)[0])
for _i, _row in results.iterrows():
    if _row.case == 'original' and _row.unique_sigma == 22.5:
        cfs = _row.cfs
        type = str(_row.case)
        can_function = str(_row.can_function)
        sigma = _row.sigma
        mean_sigma = np.mean(_row.sigma)
        optimal_itds = _row.best_itds
        optimal_ipds = tools.itd_to_ipd(cf=cfs, itd=optimal_itds)
        n_cn_neuron_per_band = _row.n_ac_neuron_per_band
        optimal_weights = _row.weights
        itds = _row.itds

        # first we bin the responses to a same resolution as the macaque data
        cfs_binned, optimal_ipds_binned, optimal_weights_binned, _ = tools.frequency_binning(
            f_low=f_low,
            f_high=f_high,
            band_width=band_width,
            cfs=np.repeat(cfs[:, None], [optimal_ipds.shape[0]], axis=1).T,
            ipds=optimal_ipds)

        # read the macaque data
        cfs_macaque = macaque_data['Fm'].astype(float).squeeze()
        ipds_macaque = macaque_data['IPDm'].astype(float).squeeze() * 2 * np.pi
        ipds_macaque = np.concatenate((ipds_macaque, -ipds_macaque))
        cfs_macaque = np.concatenate((cfs_macaque, cfs_macaque))

        # bin macaque data for frequencies within the model range
        cfs_macaque_binned, ipds_macaque_binned, weights_macaque_binned, _ = tools.frequency_binning(
            f_low=f_low,
            f_high=f_high,
            band_width=band_width,
            cfs=cfs_macaque,
            ipds=ipds_macaque)

        # get histograms for both human and macaque data
        _counts_macaque, _edge = np.histogram(ipds_macaque_binned.squeeze(),
                                              bins=70,
                                              range=(-np.pi, np.pi))
        _edge /= np.pi

        _counts_model, _edge = np.histogram(optimal_ipds, bins=70,
                                            range=(-np.pi, np.pi))
        _edge /= np.pi
        # compute optimal squeeze value
        scorrs = []
        for _squeeze_factor in np.arange(0, 1, 0.001):
            optimal_ipds_squeezed = optimal_ipds * _squeeze_factor
            _counts_model_squeezed, _edge = np.histogram(optimal_ipds_squeezed,
                                                         bins=70,
                                                         range=(-np.pi, np.pi))
            _values = *pearsonr(_counts_model_squeezed, _counts_macaque), _squeeze_factor
            scorrs.append(_values)
        scorrs = np.asarray(scorrs)
        c_alpha = fdr_pvalue(alpha_level=0.05, p_values=scorrs[:, 1])
        idx_sig_corr = np.argwhere(scorrs[:, 1] < c_alpha)
        s_factor = scorrs[:, 2]
        human_macaque_correlation = scorrs[:, 0] ** 2.0
        human_macaque_correlation_smooth = savgol_filter(human_macaque_correlation,
                                                         human_macaque_correlation.size // 20 + 1, 2)
        _idx = np.argmax(human_macaque_correlation_smooth)
        optimal_squeeze_factor = s_factor[_idx]

        # compute squeezed ipds
        optimal_ipds_squeezed = optimal_ipds * optimal_squeeze_factor
        optimal_cfs_squeezed_binned, optimal_ipds_squeezed_binned, optimal_weights_squeezed_binned_binned, _ = \
            tools.frequency_binning(
                f_low=f_low,
                f_high=f_high,
                band_width=band_width,
                cfs=np.repeat(cfs, optimal_ipds_squeezed.shape[0]),
                ipds=optimal_ipds_squeezed.reshape(-1, ))

        macaque_model = tools.ic_population_response_ipd(cfs=cfs_macaque_binned,
                                                         best_ipds=ipds_macaque_binned,
                                                         weights=weights_macaque_binned,
                                                         itds=itds,
                                                         can_function=can_function,
                                                         make_symmetric_ipd=True,
                                                         sigma=mean_sigma)

        squeeze_human_model = tools.ic_population_response_ipd(cfs=optimal_cfs_squeezed_binned,
                                                               best_ipds=optimal_ipds_squeezed_binned,
                                                               weights=optimal_weights_squeezed_binned_binned,
                                                               itds=itds,
                                                               can_function=can_function,
                                                               make_symmetric_ipd=False,
                                                               sigma=mean_sigma)
        human_original = tools.ic_population_response_ipd(cfs=cfs_binned,
                                                          best_ipds=optimal_ipds_binned,
                                                          weights=optimal_weights_binned,
                                                          itds=itds,
                                                          can_function=can_function,
                                                          make_symmetric_ipd=False,
                                                          sigma=mean_sigma)
        corr_damping_functions = pearsonr(squeeze_human_model, macaque_model)
        p_value_sq_vs_mq = corr_damping_functions.pvalue
        t_value_sq_vs_mq = t_value(corr_damping_functions.statistic, corr_damping_functions._n)
        print(corr_damping_functions)

        corr_damping_functions_orig = pearsonr(human_original, macaque_model)
        p_value_hum_vs_mq = corr_damping_functions_orig.pvalue
        t_value_hum_vs_mq = t_value(corr_damping_functions_orig.statistic, corr_damping_functions_orig._n)
        print(corr_damping_functions_orig)

        # plot cortical model prediction
        fig_model = plt.figure()
        ax = fig_model.add_subplot(111)
        axins = inset_axes(ax, width=1.3, height=0.7)
        axins.spines['right'].set_visible(False)
        axins.spines['top'].set_visible(False)

        axins.plot(s_factor, human_macaque_correlation_smooth, color=[0.0, 0.0, 0.0])

        # axins.plot(optimal_squeeze_factor, human_macaque_correlation_smooth[_idx],
        #            '+',
        #            color='black',
        #            markersize=10,
        #            alpha=0.4,
        #            markeredgewidth=1.5)
        axins.axvline(optimal_squeeze_factor,
                      ymin=human_macaque_correlation_smooth.min(),
                      ymax=0.95,
                      color='k',
                      linestyle=":")
        axins.set_xlabel('sqz. factor', fontdict={'size': 8})
        axins.set_ylabel('H vs. M $R^2$', fontdict={'size': 8})
        _idiameter = np.argmin(np.abs(s_factor - macaque_diameter / human_diameter))
        _line = axins.plot(macaque_diameter / human_diameter, 0.025,
                           marker="v", color='r', markersize=2)[0]
        _line.set_clip_on(False)
        _iitd = np.argmin(np.abs(s_factor - macaque_max_itd / human_max_itd))
        _line = axins.plot(macaque_max_itd / human_max_itd, -0.025,
                           marker="^", color='m', markersize=2)[0]
        _line.set_clip_on(False)
        axins.set_ylim(0, None)
        ax.plot(itds * 1e6, human_original, label='H', color='brown', linestyle=":")
        ax.plot(itds * 1e6, squeeze_human_model, label='H-sqz.', color='salmon')
        ax.plot(itds * 1e6, macaque_model, label='M', color='indigo')
        ax.set_xlabel("ITD [$\mu$s]", fontdict={'size': 8})
        ax.set_ylabel("Normalized cortical response", fontdict={'size': 8})
        _text = "H-sqz. vs. M \n $R^2$={:.2f} \n p$<$0.001".format(corr_damping_functions[0] ** 2)
        ax.text(x=-4000, y=0.30, s=_text,
                fontdict={'family': "serif", 'size': 8, 'style': "italic", 'rotation': 0},
                style='italic')
        ax.text(x=2000, y=0.30, s="H vs. M \n $R^2$={:.2f} \n p$<$0.001".format(
            corr_damping_functions_orig[0] ** 2.0),
                fontdict={'family': "serif", 'size': 8, 'style': "italic", 'rotation': 0},
                style='italic')

        ax.set_ylim(0.25, 1.5)
        ax.tick_params(axis='both', labelsize=8)
        ax.legend(bbox_to_anchor=(0.35, 0.5), frameon=False)
        fig_model.set_size_inches(5.4, 3.2)
        fig_model.set_size_inches(10 / inch, 6 / inch)
        fig_model.subplots_adjust(top=0.98, bottom=0.16, wspace=1, left=0.1, right=1)
        fig_model.savefig(figures_path_macaque + 'optimal_damping_macaque_human_corr_model_itd_to_ipd.png')
        fig_model.savefig(figures_path_macaque + 'optimal_damping_macaque_human_corr_model_itd_to_ipd.pdf')

        # plot distributions
        f_out_model, _ = tools.plot_ipd_distribution_and_ac_population_response(
            cfs=cfs,
            best_ipds=optimal_ipds,
            weights=optimal_weights,
            itds=itds,
            target_response=target_response,
            can_function=can_function,
            sigma=mean_sigma,
            make_symmetric_ipd=False,
            n_bins=70,
            head_diameter=human_diameter,
            freq_band_width=50
        )
        f_out_model.show()
        f_out_model.set_size_inches(11.4 / inch, 10 / inch)
        f_out_model.subplots_adjust(top=0.98, bottom=0.1, wspace=1, left=0.1, right=.9)
        f_out_model.savefig(figures_path_macaque + 'original_model_full_resolution_itd_to_ipd.png')
        f_out_model.savefig(figures_path_macaque + 'original_model_full_resolution_itd_to_ipd.pdf')

        f_out_model_binned, _ = tools.plot_ipd_distribution_and_ac_population_response(
            cfs=cfs_binned,
            best_ipds=optimal_ipds_binned,
            weights=optimal_weights_binned,
            itds=itds,
            target_response=target_response,
            can_function=can_function,
            sigma=mean_sigma,
            make_symmetric_ipd=False,
            n_bins=n_bins,
            head_diameter=human_diameter,
            freq_band_width=plots_band_width
        )
        f_out_model_binned.show()
        f_out_model_binned.set_size_inches(11.4 / inch, 10 / inch)
        f_out_model_binned.subplots_adjust(top=0.98, bottom=0.1, wspace=1, left=0.1, right=.9)
        f_out_model_binned.savefig(figures_path_macaque + 'original_model_itd_to_ipd.png')
        f_out_model_binned.savefig(figures_path_macaque + 'original_model_itd_to_ipd.pdf')

        f_out_model_squeezed, _ = tools.plot_ipd_distribution_and_ac_population_response(
            cfs=optimal_cfs_squeezed_binned,
            best_ipds=optimal_ipds_squeezed_binned,
            weights=optimal_weights_squeezed_binned_binned,
            itds=itds,
            target_response=None,
            can_function=can_function,
            sigma=mean_sigma,
            make_symmetric_ipd=False,
            n_bins=n_bins,
            head_diameter=macaque_diameter,
            freq_band_width=plots_band_width
        )
        f_out_model_squeezed.show()
        f_out_model_squeezed.set_size_inches(11.4 / inch, 10 / inch)
        f_out_model_squeezed.subplots_adjust(top=0.98, bottom=0.1, wspace=1, left=0.1, right=.9)
        f_out_model_squeezed.savefig(figures_path_macaque + 'squeezed_model_itd_to_ipd.png')
        f_out_model_squeezed.savefig(figures_path_macaque + 'squeezed_model_itd_to_ipd.pdf')

        # ############### macaque plot ####################
        f_out_macaque, _ = tools.plot_ipd_distribution_and_ac_population_response(
            cfs=cfs_macaque_binned,
            best_ipds=ipds_macaque_binned,
            weights=weights_macaque_binned,
            itds=itds,
            target_response=None,
            can_function=can_function,
            sigma=mean_sigma,
            make_symmetric_ipd=True,
            n_bins=n_bins,
            head_diameter=macaque_diameter,
            freq_band_width=plots_band_width
        )
        f_out_macaque.show()
        f_out_macaque.set_size_inches(11.4 / inch, 10 / inch)
        f_out_macaque.subplots_adjust(top=0.98, bottom=0.1, wspace=1, left=0.1, right=.9)
        f_out_macaque.savefig(figures_path_macaque + 'macaque_data_model_itd_to_ipd.png')
        f_out_macaque.savefig(figures_path_macaque + 'macaque_data_model_itd_to_ipd.pdf')

