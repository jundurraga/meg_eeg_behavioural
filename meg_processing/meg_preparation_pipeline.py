import itertools
import json
from meg_processing.tools import run_case
from environment import subjects_dir
from os.path import sep
import matplotlib
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')


if __name__ == "__main__":
    subjects = ['3205', '3239', '3249', '3252', '3341', '3504', '3540', '3544', '3581', '3587']
    conditions = ["B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9"]
    conditions = list(itertools.product(subjects, conditions))
    method = 'dSPM'
    all_analyses = ['FULL', 'ITM-FR', 'ASSR']
    for _current_analysis in all_analyses:
        for _auto_events in [False]:
            if _current_analysis == 'FULL':
                f_low = 2.0
                f_high = 48.0
            if _current_analysis == 'ITM-FR':
                #  filters for ITM-FR
                f_low = 2.0
                f_high = 8.0
            if _current_analysis == 'SUB_HARMONIC':
                #  filters for ITM-FR
                f_low = 1.5
                f_high = 4.5
            if _current_analysis == 'ASSR':
                #  filters for ASSR
                f_low = 37.0
                f_high = 48.0

            for _subject, _condition in conditions:
                with open(subjects_dir + sep + _subject + sep + 'processing.json') as json_file:
                    processing = json.load(json_file)
                run_case(subject=_subject,
                         subjects_dir=subjects_dir,
                         condition_id=_condition,
                         method=method,
                         bad_channels=processing['bad_channels'],
                         event_channel=processing['trigger_channel'],
                         f_low=f_low,
                         f_high=f_high,
                         auto_events=_auto_events
                         )

