import copy
import tempfile
import pandas as pd
from peegy.io.storage.data_storage_tools import PandasDataTable, store_data, MeasurementInformation, SubjectInformation
from peegy.processing.statistics.eeg_statistic_tools import discrete_phase_locking_value, freq_bin_phase_locking_value
from meg_processing.tools import basic_pipe_line, source_by_epochs
from meg_processing.tools import get_file, get_sources_data, fdr_pvalue, plot_surface, read_source_by_epochs
import mne
import os
from os import listdir
import numpy as np
import astropy.units as u
import json
from environment import subjects_mri_dir, subjects_dir, data_path_meg, root_directory
# mne.viz.set_3d_backend('notebook')
# if mne.viz.get_3d_backend() == 'mayavi':
#     os.environ['ETS_TOOLKIT'] = 'qt4'
#     os.environ['QT_API'] = 'pyqt5'


if __name__ == "__main__":
    conditions_id = ['B{:d}'.format(_i) for _i in list(range(1, 10))]
    conditions_labels = ['±500 µs', '±1000 µs', '±1500 µs', '±2000 µs',
                         '±2500 µs', '±3000 µs', '±3500 µs', '±4000 µs', 'Diotic']
    subjects = listdir(subjects_dir)

    # method = 'sLORETA'
    # method = 'eLORETA'
    method = 'dSPM'
    source_type = 'surface'
    auto_events = False
    alpha = 0.05
    plot_brain_activity = False
    save_plots = False
    solve_in_frequency_domain = True
    database_path = data_path_meg + 'data_meg_sources_individual_phase_angle_latency.sqlite'
    new_fs = 6.4102564102564106 * 15
    mfreqs = [6.4102564102564106 / 2, 6.4102564102564106, 44.871794871794876]

    for _idx_s, subject in enumerate(subjects):
        for condition, label in zip(conditions_id, conditions_labels):
            tables = pd.DataFrame()
            for mf in mfreqs:
                # ####sub harmonic ######
                _file_filter = 'data_fsaverage_1_4_{:}_auto_events_{:}'.format(method, auto_events)
                f_low = 1.5
                f_high = 4.5
                # # ####FULL ######
                # _file_filter = 'data_fsaverage_2_48_{:}_auto_events_{:}'.format(method, auto_events)
                # f_low = 2.0
                # f_high = 48.0

                if not os.path.isdir(subjects_dir + os.path.sep + subject + os.path.sep + condition):
                    continue
                file_source = get_file(subjects_directory=subjects_dir,
                                       subject=subject,
                                       folder=condition,
                                       condition=_file_filter,
                                       type='.pkl')
                if file_source is None:
                    with open(subjects_dir + os.path.sep + subject + os.path.sep + 'processing.json') as json_file:
                        processing = json.load(json_file)
                    data = basic_pipe_line(subject=subject,
                                           subjects_dir=subjects_dir,
                                           bads=processing['bad_channels'],
                                           condition_id=condition,
                                           remove_eoc_ecg=False,
                                           eog_ref_ch='MEG 102',
                                           ecg_ref_ch='MEG 052',
                                           event_channel=processing['trigger_channel'],
                                           f_low=f_low,
                                           f_high=f_high,
                                           auto_events=False
                                           )
                    file_source = get_file(subjects_directory=subjects_dir,
                                           subject=subject,
                                           folder=condition,
                                           condition=_file_filter,
                                           type='.pkl')
                    all_stcs = source_by_epochs(data=data,
                                                method=method,
                                                solve_in_frequency_domain=solve_in_frequency_domain,
                                                sampling_rate=new_fs,
                                                freqs_of_interest=[mf],
                                                source_type=source_type)
                else:
                    all_stcs = read_source_by_epochs(
                        data_path=file_source,
                        method=method,
                        sampling_rate=new_fs,
                        freqs_of_interest=[mf],
                        source_type=source_type,
                        root_path=root_directory,
                        solve_in_frequency_domain=solve_in_frequency_domain)
                all_data = []
                _stc = []
                for _stc in all_stcs:
                    if not isinstance(_stc, list):
                        _stc = [_stc]
                    pulled_data = np.array([_s.data for _s in _stc])
                    pulled_data = np.transpose(pulled_data, axes=[2, 1, 0])
                    all_data.append(pulled_data)
                _stc_dummy = _stc[0]
                f = tempfile.TemporaryFile()
                all_data = np.dstack(all_data)
                tmp_data = np.memmap(f, dtype=np.complex64, shape=all_data.shape)
                tmp_data[:] = all_data[:] * u.dimensionless_unscaled
                del all_data

                if solve_in_frequency_domain:
                    amp, plv, z, z_crit, p_values, angles, dof, rn = freq_bin_phase_locking_value(yfft=tmp_data)
                else:
                    amp, plv, z, z_crit, p_values, angles, dof, rn = discrete_phase_locking_value(
                        data=np.real(tmp_data),
                        alpha=alpha,
                        fs=_stc_dummy.sfreq,
                        frequency=mf)

                amp_z_value = amp / rn
                n_sources = plv.shape[1]
                alpha_fdr = fdr_pvalue(alpha_level=alpha, p_values=p_values.reshape(-1, 1))

                # p-values
                _stc_dummy_p_values = mne.SourceEstimate(p_values.reshape(-1, 1),
                                                         _stc_dummy.vertices,
                                                         _stc_dummy.tmin, _stc_dummy.tstep)
                # amplitudes values; data is scaled by residual noise
                # noise variance is equal to signal variance

                _stc_dummy_z_values_amp = mne.SourceEstimate(amp_z_value.reshape(-1, 1),
                                                             _stc_dummy.vertices,
                                                             _stc_dummy.tmin, _stc_dummy.tstep)
                # z-values from plv
                _stc_dummy_z_values = mne.SourceEstimate(z.reshape(-1, 1),
                                                         _stc_dummy.vertices,
                                                         _stc_dummy.tmin, _stc_dummy.tstep)
                # _stc_dummy_z_values.subject = 'fsaverage'
                # phase-values
                _stc_dummy_phase_values = mne.SourceEstimate(angles.reshape(-1, 1),
                                                             _stc_dummy.vertices,
                                                             _stc_dummy.tmin, _stc_dummy.tstep)
                # _stc_dummy_phase_values.subject = 'fsaverage'
                # correct delay of tube
                _phase = angles.reshape(-1, 1) * u.rad
                _phase_c = copy.copy(_phase)
                _phase_c[_phase_c < 0] = np.pi * u.rad + _phase_c[_phase_c < 0]
                phase_correction = (6.096 / 344) * 2 * np.pi * u.rad * mf
                _phase_c = _phase_c + phase_correction
                phase_delay = _phase_c + np.pi * u.rad / 2 * (mf > 30)
                latency = (phase_delay + 2 * np.pi * u.rad * (mf > 30)) / (2 * np.pi * u.rad * mf) * 1000
                _stc_dummy_latency_values = mne.SourceEstimate(latency,
                                                               _stc_dummy.vertices,
                                                               _stc_dummy.tmin, _stc_dummy.tstep)
                # plv
                _stc_dummy_plv = mne.SourceEstimate(plv.reshape(-1, 1),
                                                    _stc_dummy.vertices,
                                                    _stc_dummy.tmin, _stc_dummy.tstep)
                # _stc_dummy_plv.subject = 'fsaverage'

                _folder = subjects_dir + os.path.sep + subject + os.path.sep + condition + os.path.sep + 'figures'
                # Do plots #####
                _fname = _folder + os.path.sep + '{:}_{:}_{:}_{:}_{:}_plv'.format(subject,
                                                                                  method,
                                                                                  condition,
                                                                                  _file_filter,
                                                                                  round(mf))
                _lim = [0, 0.3, 1]
                out = get_sources_data(stc_in=_stc_dummy_plv,
                                       stc_p_values=_stc_dummy_p_values,
                                       stc_z_values=_stc_dummy_z_values,
                                       subjects_mri_dir=subjects_mri_dir,
                                       measurement_type='plv',
                                       frequency_tested=mf,
                                       alpha=alpha_fdr,
                                       dof=dof,
                                       n_sources=n_sources,
                                       label=label)
                tables = pd.concat([tables, out])
                if save_plots:
                    plot_surface(stc_in=_stc_dummy_plv,
                                 stc_p_values=_stc_dummy_p_values,
                                 significant_only=False,
                                 file_name=_fname,
                                 lims=_lim,
                                 label=label,
                                 surface='inflated',
                                 subjects_mri_dir=subjects_mri_dir,
                                 alpha=alpha_fdr)

                _fname = _folder + os.path.sep + '{:}_{:}_{:}_{:}_{:}_amplitude_z_value'.format(subject,
                                                                                                method,
                                                                                                condition,
                                                                                                _file_filter,
                                                                                                round(mf))
                _lim = [0, 0.05, 0.2]
                out = get_sources_data(stc_in=_stc_dummy_z_values_amp,
                                       stc_p_values=_stc_dummy_p_values,
                                       stc_z_values=_stc_dummy_z_values,
                                       subjects_mri_dir=subjects_mri_dir,
                                       measurement_type='amp_z_value',
                                       frequency_tested=mf,
                                       alpha=alpha_fdr,
                                       dof=dof,
                                       n_sources=n_sources,
                                       label=label)
                tables = pd.concat([tables, out])
                if save_plots:
                    plot_surface(stc_in=_stc_dummy_z_values_amp,
                                 stc_p_values=_stc_dummy_p_values,
                                 significant_only=False,
                                 file_name=_fname,
                                 lims=_lim,
                                 label=label,
                                 surface='inflated',
                                 subjects_mri_dir=subjects_mri_dir,
                                 alpha=alpha_fdr)

                _fname = _folder + os.path.sep + '{:}_{:}_{:}_{:}_{:}_phase'.format(subject,
                                                                                    method,
                                                                                    condition,
                                                                                    _file_filter,
                                                                                    round(mf))
                out = get_sources_data(stc_in=_stc_dummy_phase_values,
                                       stc_p_values=_stc_dummy_p_values,
                                       stc_z_values=_stc_dummy_z_values,
                                       subjects_mri_dir=subjects_mri_dir,
                                       measurement_type='phase',
                                       frequency_tested=mf,
                                       alpha=alpha_fdr,
                                       dof=dof,
                                       n_sources=n_sources,
                                       label=label)
                tables = pd.concat([tables, out])
                if save_plots:
                    plot_surface(stc_in=_stc_dummy_phase_values,
                                 stc_p_values=_stc_dummy_p_values,
                                 significant_only=True,
                                 file_name=_fname,
                                 color_map='seismic',
                                 lims=[-np.pi, 0, np.pi],
                                 label=label,
                                 surface='inflated',
                                 subjects_mri_dir=subjects_mri_dir,
                                 alpha=alpha_fdr)

                _fname = _folder + os.path.sep + '{:}_{:}_{:}_{:}_{:}_latency'.format(subject,
                                                                                      method,
                                                                                      condition,
                                                                                      _file_filter,
                                                                                      round(mf))
                _lim = [50, 70, 90] if mf < 30 else [20, 40, 60]
                out = get_sources_data(stc_in=_stc_dummy_latency_values,
                                       stc_p_values=_stc_dummy_p_values,
                                       stc_z_values=_stc_dummy_z_values,
                                       subjects_mri_dir=subjects_mri_dir,
                                       measurement_type='latency',
                                       frequency_tested=mf,
                                       alpha=alpha_fdr,
                                       dof=dof,
                                       n_sources=n_sources,
                                       label=label)
                tables = pd.concat([tables, out])
                if save_plots:
                    plot_surface(stc_in=_stc_dummy_latency_values,
                                 stc_p_values=_stc_dummy_p_values,
                                 significant_only=True,
                                 file_name=_fname,
                                 color_map='hsv',
                                 lims=_lim,
                                 label=label,
                                 surface='inflated',
                                 subjects_mri_dir=subjects_mri_dir,
                                 alpha=alpha_fdr)
            if not tables.size:
                continue
            measure_table = PandasDataTable(table_name='measure_table',
                                            pandas_df=tables)
            subject_info = SubjectInformation(subject_id=subject)
            measurement_info = MeasurementInformation(experiment="itd_damping", condition=label)
            _recording = {'type': 'MEG', 'fs': _stc_dummy_latency_values.sfreq}
            _parameters = {'itm_rate': 6.4102564102564106,
                           'modulation_frequency': 44.871794871794876,
                           'frequency_tested': mf,
                           'measurement_type': label}
            # # now we save our data to a database
            store_data(database_path=database_path,
                       subject_info=subject_info,
                       measurement_info=measurement_info,
                       recording_info=_recording,
                       stimuli_info=_parameters,
                       pandas_df=[measure_table])
