from meg_processing.tools import set_file_name, basic_pipe_line
from peegy.processing.pipe.io import ChannelItem, GenericInputData
from peegy.processing.pipe.general import FilterData
from peegy.processing.pipe.pipeline import PipePool
from peegy.processing.pipe.epochs import AverageEpochsFrequencyDomain, AverageEpochs
from peegy.processing.pipe.storage import MeasurementInformation, SubjectInformation, SaveToDatabase
from peegy.processing.pipe.transform import InstantaneousPhaseChange, RelativeInstantaneousPhaseFrequency
from peegy.processing.tools.detection.definitions import TimePeakWindow, PeakToPeakMeasure, TimeROI
from peegy.processing.pipe.detection import PeakDetectionTimeDomain
from peegy.processing.pipe.plot import PlotWaveforms, PlotTopographicMap
import astropy.units as u
import numpy as np
import itertools
import json
import matplotlib.pyplot as plt
import pandas as pd
from os.path import sep
import os
from environment import subjects_dir, data_path_meg
import matplotlib
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')


def run_case(subject: str = '',
             condition_id: str = '',
             database_path: str = '',
             bad_channels: [str] = [],
             event_channel: str = '',
             f_low: float = 2.0,
             f_high: float = 48.0
             ):
    assr_rate = 44.871794871794876 * u.Hz
    itd_fr_rate = 6.4102564102564106 * u.Hz
    data_file = set_file_name(subjects_directory=subjects_dir, subject=subject, condition='',
                              name='results.json'.format(subject))

    data = basic_pipe_line(subject=subject,
                           subjects_dir=subjects_dir,
                           bads=bad_channels,
                           condition_id=condition_id,
                           remove_eoc_ecg=False,
                           eog_ref_ch='MEG 102',
                           ecg_ref_ch='MEG 052',
                           event_channel=event_channel,
                           f_low=f_low,
                           f_high=f_high,
                           auto_events=True,
                           auto_events_rate=itd_fr_rate / 2
                           )

    fs = data.epochs.info['sfreq'] * u.Hz
    epochs = data.epochs._data.T
    epoch_length = epochs.shape[0] / fs
    dummy_process = GenericInputData(data=epochs,
                                     fs=fs,
                                     channel_labels=np.array([_l for _l in data.epochs.ch_names]))
    dummy_process.run()

    test_frequencies = np.array([itd_fr_rate.value, assr_rate.value]) * u.Hz
    pipeline = PipePool()
    #
    pipeline['filter_data_assr'] = FilterData(dummy_process,
                                              high_pass=37.0 * u.Hz)
    # filter data for time-domain plot
    pipeline['time_average_assr'] = AverageEpochs(pipeline['filter_data_assr'],
                                                  weighted_average=True,
                                                  weight_across_epochs=False)
    pipeline['time_average_assr_itd_fr'] = AverageEpochs(dummy_process,
                                                         weighted_average=True,
                                                         weight_across_epochs=False)

    # frequency-domain average of ITD-FR and ASSR
    pipeline['fft_ave_assr_freq'] = AverageEpochsFrequencyDomain(dummy_process,
                                                                 weighted_average=True,
                                                                 weight_across_epochs=False,
                                                                 weighted_frequency_domain=False,
                                                                 test_frequencies=test_frequencies)

    pipeline.run()
    #  #### phase analysis ####
    reference_window = TimeROI(ini_time=100 * u.ms, end_time=140 * u.ms)
    pipeline['phase_dss'] = RelativeInstantaneousPhaseFrequency(pipeline['time_average_assr'],
                                                                relative_difference=True,
                                                                reference_frequency=assr_rate,
                                                                reference_roi_window=reference_window,
                                                                wavelet_half_amplitude_time=4 / assr_rate,
                                                                align_reference_phase=True,
                                                                align_reference_time=120 * u.ms)
    pipeline.run()
    tw_phase = np.array([TimePeakWindow(ini_time=0 * u.ms,
                                        end_time=epoch_length.to(u.ms) / 8,
                                        label='min_phase_1',
                                        positive_peak=False,
                                        exclude_channels=['GFP']),
                         TimePeakWindow(ini_ref='min_phase_1',
                                        end_time=epoch_length.to(u.ms) / 4,
                                        label='max_phase_1',
                                        positive_peak=True,
                                        exclude_channels=['GFP']),
                         TimePeakWindow(ini_time=epoch_length.to(u.ms) / 2,
                                        end_time=epoch_length.to(u.ms) / 2 + epoch_length.to(u.ms) / 8,
                                        label='min_phase_2',
                                        positive_peak=False,
                                        exclude_channels=['GFP']),
                         TimePeakWindow(ini_ref='min_phase_2',
                                        end_time=3 * epoch_length.to(u.ms) / 4,
                                        label='max_phase_2',
                                        positive_peak=True,
                                        exclude_channels=['GFP']),
                         # ############## control ################################
                         TimePeakWindow(ini_time=epoch_length.to(u.ms) / 4,
                                        end_time=epoch_length.to(u.ms) / 4 + epoch_length.to(u.ms) / 8,
                                        label='min_control_1',
                                        positive_peak=False,
                                        exclude_channels=['GFP']),
                         TimePeakWindow(ini_ref='min_control_1',
                                        end_time=epoch_length.to(u.ms) / 2,
                                        label='max_control_1',
                                        positive_peak=True,
                                        exclude_channels=['GFP']),
                         TimePeakWindow(ini_time=3 * epoch_length.to(u.ms) / 4,
                                        end_time=3 * epoch_length.to(u.ms) / 4 + epoch_length.to(u.ms) / 8,
                                        label='min_control_2',
                                        positive_peak=False,
                                        exclude_channels=['GFP']),
                         TimePeakWindow(ini_ref='min_control_2',
                                        end_time=epoch_length.to(u.ms),
                                        label='max_control_2',
                                        positive_peak=True,
                                        exclude_channels=['GFP']),
                         ])
    pm_phase = np.array([PeakToPeakMeasure(ini_peak='max_phase_1', end_peak='min_phase_1'),
                         PeakToPeakMeasure(ini_peak='max_phase_2', end_peak='min_phase_2'),
                         PeakToPeakMeasure(ini_peak='max_control_1', end_peak='min_control_1'),
                         PeakToPeakMeasure(ini_peak='max_control_2', end_peak='min_control_2')
                         ])
    pipeline['phase_peaks_dss'] = PeakDetectionTimeDomain(pipeline['phase_dss'],
                                                          time_peak_windows=tw_phase,
                                                          peak_to_peak_measures=pm_phase)
    pipeline.run()
    # tmp_plot = PlotWaveforms(pipeline['phase_peaks_dss'],
    #                          save_to_file=False,
    #                          return_figures=True,
    #                          ch_to_plot=np.array(['MEG 125']))
    #
    # tmp_plot.run()
    subject_info = SubjectInformation(subject_id=subject)
    conditions = np.array([0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 0.0])
    codes = np.array(["B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9"])
    _condition = float(conditions[np.where(condition_id == codes)[0]])
    measurement_info = MeasurementInformation(experiment="itd_damping",
                                              condition=_condition)
    _recording = {'type': 'MEG', 'fs': fs}
    _parameters = {'itm_rate': itd_fr_rate,
                   'modulation_frequency': assr_rate}
    # now we save our data to a database
    pipeline['saver'] = SaveToDatabase(database_path=database_path,
                                       subject_information=subject_info,
                                       measurement_information=measurement_info,
                                       stimuli_information=_parameters,
                                       recording_information=_recording,
                                       processes_list=[pipeline['time_average_assr'],
                                                       pipeline['time_average_assr_itd_fr'],
                                                       pipeline['fft_ave_assr_freq'],
                                                       pipeline['phase_peaks_dss'],
                                                       # pipeline['phase_dss_trials']
                                                       ],
                                       include_waveforms=True)
    pipeline.run()

    evoked = data.epochs.average()
    yfft = np.abs(np.fft.fft(evoked.data, axis=1)) * 2 / evoked.data.shape[1]
    freq = np.arange(0, evoked.data.shape[1]) * fs / evoked.data.shape[1]
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(freq, yfft.T)
    ax.set_xlim(0, 60)
    fig.savefig(data.figures_path + sep + 'fft.png')
    fig.clf()

    index = np.argmin(np.abs((freq - data.freq_of_interest[0] * u.Hz)))
    ampl = yfft[:, index]
    gfp = np.std(ampl)

    results = {'max_amp': [np.max(ampl)], 'gfp': [gfp], 'condition': [condition_id], 'subject': [subject]}
    df = pd.DataFrame.from_dict(results)
    if not os.path.isfile(data_file):
        df.to_csv(data_file, mode='a', sep='\t', index=False)

    else:
        df.to_csv(data_file, mode='a', header=False, sep='\t', index=False)


if __name__ == "__main__":
    database_path = data_path_meg + 'data_meg_assr_short_epochs.sqlite'
    subjects = ['3205', '3239', '3249', '3252', '3341', '3504', '3540', '3544', '3581', '3587']
    all_conditions = ["B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9"]
    conditions_subject = list(itertools.product(subjects, all_conditions))
    for _subject, _condition in conditions_subject:
        with open(subjects_dir + sep + _subject + sep + 'processing.json') as json_file:
            processing = json.load(json_file)
        run_case(subject=_subject, condition_id=_condition, database_path=database_path,
                 bad_channels=processing['bad_channels'],
                 event_channel=processing['trigger_channel'],
                 f_low=2.0,
                 f_high=48.0
                 )
