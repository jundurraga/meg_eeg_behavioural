import numpy as np
import matplotlib
from peegy.io.storage.data_storage_reading_tools import sqlite_waveforms_to_pandas
from peegy.io.storage.plot_tools import plot_time_frequency_responses
from environment import figures_path_meg, data_path_meg
import  astropy.units as u
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')

database_path = data_path_meg + 'data_meg_assr_short_epochs.sqlite'

channels = [#  right side red
    'MEG 125'
]
df = sqlite_waveforms_to_pandas(database_path=database_path,
                                group_factors=['condition',
                                               'data_source',
                                               'itm_rate',
                                               'modulation_frequency'],
                                channels=channels)
df = df[np.isin(df.data_source, ['phase_peaks_dss'])]
df['ITD'] = df.loc[:, 'condition']
df = df.sort_values(by=['ITD'], ascending=False)
df['itd'] = (df['ITD'] * 1e3).astype(int).astype('category')
df['diotic'] = df['itd'] == 0
df['itd_condition'] = ('$\pm$' + (df['itd']).astype(str) + ' $\mu$s')
df.loc[df['itd_condition'] == '$\pm$0 $\mu$s', ['itd_condition']] = 'Diotic'
df['itd_condition'] = df['itd_condition'].astype('category')
df['itd_condition'] = df['itd_condition'].cat.remove_unused_categories()
df['itd_condition'] = df['itd_condition'].cat.reorder_categories(['$\pm$500 $\mu$s',
                                                                  '$\pm$1000 $\mu$s',
                                                                  '$\pm$1500 $\mu$s',
                                                                  '$\pm$2000 $\mu$s',
                                                                  '$\pm$2500 $\mu$s',
                                                                  '$\pm$3000 $\mu$s',
                                                                  '$\pm$3500 $\mu$s',
                                                                  '$\pm$4000 $\mu$s',
                                                                  'Diotic'])
# df['domain'] = df.loc[:, 'domain'].astype('category')
# df['domain'] = df['domain'].cat.reorder_categories(['time', 'frequency'])

n_cycles_to_plot = 2
x_fs = np.mean(df['x_fs'])
samples_per_cycle = np.mean(df['x_fs'] / df['itm_rate'])
sub_average_time_buffer_size = int(samples_per_cycle * n_cycles_to_plot)
time_vmarkers = np.arange(0, sub_average_time_buffer_size, samples_per_cycle) / x_fs
itm_rate = np.mean(df['itm_rate'])
am_rate = np.mean(df['modulation_frequency'])
freq_vmarkers = np.array([itm_rate, am_rate])

dataset = df[np.isin(df.data_source,  ['time_average_assr',  'phase_peaks_dss'])]
# dataset = df[np.isin(df.data_source,  ['time_average_assr'])]
# dataset = dataset[np.isin(dataset['itd_condition'], ['$\pm$0 $\mu$s', '$\pm$500 $\mu$s'])]
dataset = dataset.query('diotic == False')
dataset['itd_condition'] = dataset['itd_condition'].cat.remove_unused_categories()
dataset = dataset.reset_index(drop=True)

fig_out = plot_time_frequency_responses(dataframe=dataset,
                                        # group_by=['itd_condition'],
                                        rows_by='itd_condition',
                                        title_by='',
                                        sub_average_time_buffer_size=sub_average_time_buffer_size,
                                        time_xlim=[0, sub_average_time_buffer_size / x_fs],
                                        time_ylim=[-45, 45],
                                        freq_xlim=[0, 48],
                                        freq_ylim=[0, 19],
                                        time_vmarkers=time_vmarkers,
                                        freq_vmarkers=freq_vmarkers,
                                        freq_vmarkers_style='v',
                                        show_sem=False,
                                        y_unit_to=u.deg,
                                        ylabel='Phase [degrees]',
                                        show_individual_waveforms=True,
                                        individual_waveforms_alpha=0.03)
inch = 2.54
fig_out.set_size_inches(6.5/2/inch, 12/inch)
fig_out.subplots_adjust(top=0.98, bottom=0.07, wspace=0.38, left=0.3, right=0.80)
# fig_out.tight_layout()
fig_out.savefig(figures_path_meg + 'figure_S2_h.pdf', dpi=600)
fig_out.savefig(figures_path_meg + 'figure_S2_h.png', dpi=600)

# ############### additional checks ###########################################################################
# pick sensor above auditory cortex for example
channels = [#  right side red blue
    'MEG 125', 'MEG 106',
    # left side red blue
    'MEG 043', 'MEG 058'
]
df = sqlite_waveforms_to_pandas(database_path=database_path,
                                group_factors=['condition',
                                               'data_source',
                                               'itm_rate',
                                               'channel',
                                               'modulation_frequency'],
                                channels=channels)
df['ITD'] = df.loc[:, 'condition']
df = df.sort_values(by=['ITD'], ascending=False)
df['itd'] = (df['ITD'] * 1e3).astype(int).astype('category')
df['diotic'] = df['itd'] == 0
df['itd_condition'] = ('$\pm$' + (df['itd']).astype(str) + ' $\mu$s')
df.loc[df['itd_condition'] == '$\pm$0 $\mu$s', ['itd_condition']] = 'Diotic'
df['itd_condition'] = df['itd_condition'].astype('category')
df['itd_condition'] = df['itd_condition'].cat.remove_unused_categories()
df['itd_condition'] = df['itd_condition'].cat.reorder_categories(['$\pm$500 $\mu$s',
                                                                  '$\pm$1000 $\mu$s',
                                                                  '$\pm$1500 $\mu$s',
                                                                  '$\pm$2000 $\mu$s',
                                                                  '$\pm$2500 $\mu$s',
                                                                  '$\pm$3000 $\mu$s',
                                                                  '$\pm$3500 $\mu$s',
                                                                  '$\pm$4000 $\mu$s',
                                                                  'Diotic'])

dataset = df[np.isin(df.data_source,  ['time_average_assr'])]
dataset = dataset[np.isin(dataset['itd_condition'], ['Diotic', '$\pm$500 $\mu$s'])]
dataset['itd_condition'] = dataset['itd_condition'].cat.remove_unused_categories()
dataset = dataset.reset_index(drop=True)

fig_out = plot_time_frequency_responses(dataframe=dataset,
                                        cols_by='data_source',
                                        group_by=['channel', 'itd_condition'],
                                        rows_by='channel',
                                        title_by='',
                                        sub_average_time_buffer_size=sub_average_time_buffer_size,
                                        time_xlim=[0, sub_average_time_buffer_size / x_fs],
                                        time_ylim=[-10, 10],
                                        freq_xlim=[0, 48],
                                        freq_ylim=[0, 19],
                                        time_vmarkers=time_vmarkers,
                                        freq_vmarkers=freq_vmarkers,
                                        freq_vmarkers_style='v',
                                        show_sem=False,
                                        show_sd=True,
                                        y_unit_to=u.fT,
                                        ylabel='Amplitude [fT]',
                                        show_individual_waveforms=True,
                                        individual_waveforms_alpha=0.03)
inch = 2.54
fig_out.set_size_inches(12/inch, 18/inch)
fig_out.subplots_adjust(top=0.98, bottom=0.07, wspace=0.22, left=0.16, right=0.90)

fig_out.tight_layout()
fig_out.savefig(figures_path_meg + 'assr-itd_fr-time-freq_meg_{:}_500vsDiotic_amp.pdf'.format(channels), dpi=600)
fig_out.savefig(figures_path_meg + 'assr-itd_fr-time-freq_meg_{:}_500vsDiotic_amp.png'.format(channels), dpi=600)

dataset = df[np.isin(df.data_source,  ['phase_peaks_dss'])]
dataset = dataset[np.isin(dataset['itd_condition'], ['Diotic', '$\pm$500 $\mu$s'])]
dataset['itd_condition'] = dataset['itd_condition'].cat.remove_unused_categories()
dataset = dataset.reset_index(drop=True)
dataset.drop(['diotic', 'itd', 'ITD', 'condition'], axis=1, inplace=True)

fig_out = plot_time_frequency_responses(dataframe=dataset,
                                        cols_by='data_source',
                                        group_by=['channel', 'itd_condition'],
                                        rows_by='channel',
                                        title_by='',
                                        sub_average_time_buffer_size=sub_average_time_buffer_size,
                                        time_xlim=[0, sub_average_time_buffer_size / x_fs],
                                        time_ylim=[-45, 45],
                                        freq_xlim=[0, 48],
                                        freq_ylim=[0, 19],
                                        time_vmarkers=time_vmarkers,
                                        freq_vmarkers=freq_vmarkers,
                                        freq_vmarkers_style='v',
                                        show_sem=False,
                                        show_sd=True,
                                        y_unit_to=u.deg,
                                        ylabel='Phase [degrees]',
                                        show_individual_waveforms=True,
                                        individual_waveforms_alpha=0.03)
inch = 2.54
fig_out.set_size_inches(12/inch, 18/inch)
fig_out.subplots_adjust(top=0.98, bottom=0.07, wspace=0.22, left=0.16, right=0.90)
fig_out.savefig(figures_path_meg + 'assr-itd_fr-time-freq_meg_{:}_500vsDiotic_phase.pdf'.format(channels), dpi=600)
fig_out.savefig(figures_path_meg + 'assr-itd_fr-time-freq_meg_{:}_500vsDiotic_phase.png'.format(channels), dpi=600)

