import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sqlite3
from scipy.optimize import basinhopping
from scipy.stats import pearsonr
from environment import data_path_meg
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')


def t_value(r, n):
    t = r * np.sqrt(n - 2) / np.sqrt(1 - r ** 2)
    return t


def func(x, p):
    return p[0] * np.exp((500 - x) / p[1]) + p[2] * np.sin(2 * np.pi * x / 1500 + p[4]) + p[3]


def error(p, x, target):
    e = np.mean((target - func(x, p)) ** 2.0)
    return e


def gfp(x):
    x = x*1e15
    out = 20 * np.log10(np.sqrt(np.mean((x - np.mean(x)) ** 2) * x.size))
    print(x.size)
    return out


database_path = data_path_meg + 'data_meg_fft.sqlite'
db = sqlite3.connect(database_path)
df = pd.read_sql_query('SELECT S.subject_id, '
                       'M.condition, '
                       'STI.*, '
                       'FP.* '
                       'FROM subjects as S '
                       'JOIN measurement_info M ON M.id_subject = S.id '
                       'JOIN stimuli STI ON STI.id_measurement = M.id '
                       'JOIN hotelling_t2_freq FP  ON  FP.id_stimuli = STI.id '
                       , db)

df = df.query('data_source == "fft_ave_full_freq"')
df = df[np.round(df.frequency_tested, decimals=1) == 6.4]
df = df[df.condition > 0]
df_gfp = df.groupby(['subject_id', 'condition'])['mean_amplitude'].apply(gfp).reset_index(name='gfp')
group = df_gfp.groupby('condition').mean(numeric_only=True)
group = group.reset_index()
pdopt = []
xdata = group.condition.values * 1e3
ydata = group.gfp.values
ydata = ydata - np.max(ydata)

x0 = np.array([0, 0.1, 0, 0, 0])
bounds = ((0, 100),
          (10, 10000),
          (0, 100),
          (-15, 15),
          (-np.pi, np.pi),
          )
minimizer_kwargs = {"method": "L-BFGS-B", "bounds": bounds, "tol": 1e-10,
                    "args": (xdata, ydata)}
res_1 = basinhopping(error, x0=x0,
                     niter=2000, stepsize=0.2, T=1e-8,
                     minimizer_kwargs=minimizer_kwargs)
optimal_fit = res_1.x
plt.plot(xdata, ydata)
plt.plot(xdata, func(xdata, optimal_fit))
corr = pearsonr(ydata, func(xdata, optimal_fit))
t_val = t_value(corr.statistic, corr._n)
print('R2 = {:}'.format(corr[0] ** 2))
print('Thau: {:}, Phi {:}'.format(optimal_fit[1], optimal_fit[4] * 180 / np.pi))
data = pd.DataFrame([{'thau': optimal_fit[1], 'phi': optimal_fit[4]}])
data.to_csv(data_path_meg + 'meg_fitted_sin_exp.csv', index=False)
plt.show()
