import os
import re
import copy
from os.path import isdir, sep
from os import listdir, makedirs
import seaborn as sns
import mne
import numpy as np
import astropy.units as u
import matplotlib.pyplot as plt
from mne.preprocessing import ICA
from mne.preprocessing import create_eog_epochs, create_ecg_epochs
import mne.viz
from distutils.dir_util import copy_tree
from peegy.io.external_tools.aep_gui import extsys_tools as et
from peegy.processing.pipe.spatial_filtering import CreateAndApplySpatialFilter
from peegy.processing.pipe.io import GenericInputData
from peegy.definitions.events import Events, SingleEvent
from peegy.processing.pipe.general import RemoveEvokedResponse, FilterData
from peegy.processing.pipe.epochs import DeEpoch, EpochData
from peegy.processing.tools.epochs_processing_tools import get_discrete_frequencies_weights
from scipy.optimize import leastsq
from matplotlib.pyplot import cm
from mayavi import mlab
import pandas as pd
import shutil
from nilearn.plotting import plot_stat_map
from nilearn.image import index_img
import math
import pickle
# os.environ['ETS_TOOLKIT'] = 'qt4'
# os.environ['QT_API'] = 'pyqt5'


def lcm(a, b):
    return abs(a*b) // math.gcd(a, b)


class PipeOut(object):
    def __init__(self,
                 subjects_mri_dir=None,
                 subject_mri_name=None,
                 condition_path=None,
                 raw=None,
                 bem_sol=None,
                 forward=None,
                 freq_of_interest=None,
                 epoch_duration=None,
                 picks=None,
                 figures_path=None,
                 power=None,
                 csd_signal=None,
                 epochs=None,
                 weights=None,
                 evoked=None,
                 events=None,
                 noise_cov=None,
                 fname_trans=None,
                 source_space_file=None,
                 volume_source_space_file=None,
                 bem_data=None,
                 bem_sol_file_name=None,
                 fsaverage_path=None,
                 stimuli_file=None,
                 forward_sol_file_name_surface=None,
                 forward_sol_file_name_volumen=None,
                 forward_sol_file_name_surface_volume=None,
                 raw_file_name=None,
                 auto_events=None
                 ):
        self.subjects_mri_dir = subjects_mri_dir
        self.subject_mri_name = subject_mri_name
        self.condition_path = condition_path
        self.raw = raw
        self.bem_sol = bem_sol
        self.forward = forward
        self.freq_of_interest = freq_of_interest
        self.epoch_duration = epoch_duration
        self.picks = picks
        self.figures_path = figures_path
        self.power = power
        self.csd_signal = csd_signal
        self.epochs = epochs
        self.weights = weights
        self.evoked = evoked
        self.events = events
        self.noise_cov = noise_cov
        self.fname_trans = fname_trans
        self.source_space_file = source_space_file
        self.volume_source_space_file = volume_source_space_file
        self.bem_data = bem_data
        self.bem_sol_file_name = bem_sol_file_name
        self.fsaverage_path = fsaverage_path
        self.stimuli_file = stimuli_file
        self.forward_sol_file_name_surface = forward_sol_file_name_surface
        self.forward_sol_file_name_volumen = forward_sol_file_name_volumen
        self.forward_sol_file_name_surface_volume = forward_sol_file_name_surface_volume
        self.raw_file_name = raw_file_name
        self.auto_events = auto_events


def get_file(subjects_directory='', subject='', folder='', condition='', type=''):
    path = os.path.join(subjects_directory, subject, folder)
    _file = None
    if not isdir(path):
        print('could not find {:}'.format(path))
    else:
        for filename in listdir(path):
            p = re.compile(r".*({:}).*({:})$".format(condition, type))
            _math = p.findall(filename)
            if _math:
                _file = os.path.join(path, filename)
                break
    return _file


def set_path(subjects_directory='', subject='', condition='', name=''):
    _path = os.path.join(subjects_directory, subject, condition, name)
    if not isdir(_path):
        makedirs(_path)
    return _path


def set_file_name(subjects_directory='', subject='', condition='', name=''):
    _path = os.path.join(subjects_directory, subject, condition)
    if not isdir(_path):
        makedirs(_path)
    return os.path.join(_path, name)


def remove_eog_ecg_artifacts(raw: mne.io.read_raw_kit,
                             picks: mne.pick_types,
                             eog_ref_ch='',
                             ecg_ref_ch='') -> mne.io.read_raw_kit:
    method = 'fastica'
    # Choose other parameters
    n_components = 25  # if float, select n_components by explained variance of PCA
    decim = 3  # we need sufficient statistics, not all time points -> saves time

    # we will also set state of the random number generator - ICA is a
    # non-deterministic algorithm, but we want to have the same decomposition
    # and the same order of components each time this tutorial is run
    random_state = 23

    # 1Hz high pass is often helpful for fitting ICA (already lowpassed @ 40 Hz)
    raw.filter(1., None, n_jobs=1, fir_design='firwin')

    reject = dict(mag=5e-12, grad=4000e-13)
    ica = ICA(n_components=n_components, method=method, random_state=random_state)
    ica.fit(inst=raw, picks=picks, decim=decim, reject=reject)
    ica.plot_components()  # can you spot some potential bad guys?
    ica.plot_sources(raw)  # look at source time course
    # ica.plot_properties(raw, picks=0)

    # plot raw data to pick references for detection of ECG and EOG
    raw.plot()
    ica_to_exclude = []
    # EOG
    if eog_ref_ch != '':
        eog_inds, scores = ica.find_bads_eog(raw, ch_name=eog_ref_ch)
        if eog_inds:
            ica.plot_scores(scores, exclude=eog_inds, title='eog', labels='eog')
            show_picks = np.abs(scores).argsort()[::-1][:5]

            ica.plot_sources(raw, show_picks, title='eog')
            ica.plot_components(eog_inds, title='eog', colorbar=True)
            n_max_eog = 1
            eog_inds = eog_inds[:n_max_eog]

            eog_epochs = create_eog_epochs(raw, tmin=-.5, tmax=.5, picks=picks, ch_name=eog_ref_ch)
            eog_evoked = eog_epochs.average()
            # ica.plot_sources(eog_evoked, exclude=eog_inds)  # plot ECG sources + selection
            # ica.plot_overlay(eog_evoked, exclude=eog_inds)  # plot ECG cleaning

            # check the amplitudes do not change
            # ica.plot_overlay(raw)  # EOG artifacts remain
            ica_to_exclude += eog_inds
    # ECG
    if ecg_ref_ch != '':
        ecg_epochs = create_ecg_epochs(raw, reject=None, picks=picks)
        ecg_evoked = ecg_epochs.average()
        ecg_inds, scores = ica.find_bads_ecg(ecg_epochs, method='correlation', ch_name=ecg_ref_ch)
        if ecg_inds:
            ica.plot_scores(scores, exclude=ecg_inds, title='ecg')
            n_max_ecg = 1
            ecg_inds = ecg_inds[:n_max_ecg]
            show_picks = np.abs(scores).argsort()[::-1][:5]
            ica.plot_sources(raw, picks=show_picks, title='ecg')
            ica.plot_sources(ecg_evoked)  # plot ECG sources + selection
            # ica.plot_overlay(ecg_evoked, exclude=ecg_inds)  # plot ECG cleaning

            # check the amplitudes do not change
            # ica.plot_overlay(raw)  # EOG artifacts remain
            ica_to_exclude += ecg_inds

    # remove both ECG and EOG artifact components
    ica.exclude = ica_to_exclude
    ica.apply(raw)
    return raw


def basic_pipe_line(subject='',
                    subjects_dir='',
                    remove_eoc_ecg=False,
                    bads: list = [],
                    condition_id='',
                    eog_ref_ch='',
                    ecg_ref_ch='',
                    f_low=2.0,
                    f_high=8.0,
                    event_channel='MISC 017',
                    check_plots=False,
                    auto_events=True,
                    auto_events_rate: float = None,
                    force_files=False
                    ):
    elp = get_file(subjects_dir, subject, condition='', type='.elp')
    hsp = get_file(subjects_dir, subject, condition='', type='.hsp')
    fname_trans = set_file_name(subjects_directory=subjects_dir, subject=subject, condition='',
                                name='fsaverage-{:}-trans.fif'.format(subject))
    source_space_file = set_file_name(subjects_directory=subjects_dir, subject=subject, condition='',
                                      name='oct6-src.fif')
    volume_source_space_file = set_file_name(subjects_directory=subjects_dir, subject=subject, condition='',
                                             name='vol-src.fif')
    subject_mri_name = 'fsaverage-{:}'.format(subject)
    subjects_mri_dir = set_path(subjects_directory=subjects_dir, subject='', condition='', name='mri')

    fsaverage_source_space_file = set_file_name(subjects_directory=subjects_mri_dir,
                                                subject='fsaverage', condition='',
                                                name='oct6-src.fif')
    fsaverage_volume_source_space_file = set_file_name(subjects_directory=subjects_mri_dir,
                                                       subject='fsaverage', condition='',
                                                       name='vol-src.fif')
    subject_mri_dir = os.path.join(subjects_mri_dir, 'fsaverage-{:}'.format(subject))
    fsaverage_mri_dir = os.path.join(subjects_mri_dir, 'fsaverage')
    bem_data = os.path.join(subject_mri_dir, 'bem', 'fsaverage-{:}-inner_skull-bem.fif'.format(subject))
    bem_sol_file_name = os.path.join(subject_mri_dir, 'bem', 'trans-inner_skull-bem-sol.fif'.format(subject))
    fsaverage_path = os.path.join(subjects_mri_dir, 'fsaverage')
    fsaverage_bem_sol_file_name = os.path.join(os.path.join(subjects_mri_dir, 'fsaverage'), 'bem', 'fsaverage-inner_skull-bem.fif')
    condition_path = set_path(subjects_directory=subjects_dir, subject=subject,
                              condition=condition_id,
                              name='')
    input_file_name = get_file(subjects_dir, subject, condition=condition_id, type='.con')
    mrk = get_file(subjects_dir, subject, condition=condition_id, type='.mrk')
    # read parameters from stimuli file
    stimuli_file = get_file(subjects_dir, subject, condition='', type='.extsys')
    stimuli_parameters = et.get_measurement_info_from_zip(stimuli_file)
    itm_rate = stimuli_parameters['Measurement']['StimuliModule']['Stimulus'][0]['Parameters']['ITDModRate']
    assr_rate = stimuli_parameters['Measurement']['StimuliModule']['Stimulus'][0]['Parameters']['ModulationFrequency']
    epoch_duration = stimuli_parameters['Measurement']['StimuliModule']['Stimulus'][0]['Parameters']['Duration']

    figures_path = set_path(subjects_directory=subjects_dir, subject=subject, condition=condition_id,
                            name='figures')
    # if using a different marker coil file, the forward solution needs to be created for that marker file
    forward_sol_file_name_surface = set_file_name(subjects_directory=subjects_dir, subject=subject,
                                                  condition=condition_id,
                                                  name='5120-5120-5120-surface-fwd-sol.fif')
    forward_sol_file_name_volumen = set_file_name(subjects_directory=subjects_dir, subject=subject,
                                                  condition=condition_id,
                                                  name='5120-5120-5120-volumen-fwd-sol.fif')
    forward_sol_file_name_surface_and_volume = set_file_name(subjects_directory=subjects_dir, subject=subject,
                                                             condition=condition_id,
                                                             name='5120-5120-5120-surface-volumen-fwd-sol.fif')
    raw_file_name = set_file_name(subjects_directory=subjects_dir, subject=subject, condition=condition_id,
                                  name='raw.fif')

    if not os.path.isdir(fsaverage_path):
        copy_tree(os.path.join(os.environ['SUBJECTS_DIR'], 'fsaverage'), os.path.join(subjects_mri_dir, 'fsaverage'))
        # scalp can be generated as below
        os.system("mne make_scalp_surfaces -d {:} -s fsaverage --overwrite --force".format(subjects_mri_dir))
        # these can be copied from mne/data/fsaverage
        copy_tree(os.path.join(os.path.dirname(mne.__file__), 'data', 'fsaverage'), os.path.join(fsaverage_path, 'bem'))
        # these can also be generated as below
        os.system("mne watershed_bem  -d {:} -s fsaverage --overwrite".format(subjects_mri_dir))
        source = os.path.join(subjects_mri_dir, 'fsaverage', 'bem', 'watershed')
        destination = os.path.join(subjects_mri_dir, 'fsaverage', 'bem')
        files = os.listdir(source)
        r = re.compile(r"fsaverage_*")
        r2 = re.compile(r"_surface*")
        for _file in files:
            if _file.endswith('_surface'):
                _new_file = r2.sub('', r.sub('', _file))
                shutil.move(os.path.join(source, _file), os.path.join(destination, _new_file + '.surf'))

    if not os.path.isdir(figures_path):
        os.mkdir(figures_path)

    # create the reconstruction geometry
    mne.set_config('SUBJECTS_DIR', subjects_mri_dir)

    if not os.path.exists(raw_file_name):
        raw = mne.io.read_raw_kit(input_fname=input_file_name,
                                  mrk=mrk,
                                  elp=elp,
                                  hsp=hsp,
                                  slope='+',
                                  stimthresh=0.8,
                                  stim_code='binary',
                                  preload=True,
                                  )
        raw.save(raw_file_name)
    else:
        raw = mne.io.read_raw_fif(raw_file_name, preload=True)

    fs = raw.info['sfreq']
    raw.info['bads'] = bads

    picks = mne.pick_types(raw.info, meg=True, eeg=False, stim=False, eog=False, exclude='bads')

    if not os.path.exists(fname_trans):
        # trans file must be save in participant folder as fsaverage-<SUBJECT NAME>
        mne.gui.coregistration(subject='fsaverage', inst=raw_file_name)

    # setup surface-based source space
    if not os.path.exists(source_space_file) or force_files:
        src = mne.setup_source_space(subject_mri_dir, spacing='oct6')
        mne.write_source_spaces(source_space_file, src, overwrite=True)
    else:
        src = mne.read_source_spaces(source_space_file)

    if not os.path.exists(fsaverage_source_space_file) or force_files:
        src_fs = mne.setup_source_space(fsaverage_mri_dir, spacing='oct6')
        mne.write_source_spaces(fsaverage_source_space_file, src_fs, overwrite=True)

    if not os.path.exists(fsaverage_volume_source_space_file) or force_files:
        volume_fs = mne.setup_volume_source_space(
            'fsaverage',
            mri=os.path.join(fsaverage_mri_dir, 'mri', 'aseg.mgz'),
            pos=10.0,
            bem=fsaverage_bem_sol_file_name,
            subjects_dir=subjects_mri_dir,
            add_interpolator=True,
            verbose=True)
        mne.write_source_spaces(fsaverage_volume_source_space_file, volume_fs, overwrite=True)

    if check_plots:
        fig = mne.viz.plot_bem(subject=subject_mri_dir, subjects_dir=subjects_dir,
                               brain_surfaces='white', src=src, orientation='coronal')
        fig.savefig((figures_path + sep + 'brain.png'))
        brain = mne.Brain(subject_mri_dir, 'lh', 'white', subjects_dir=subjects_dir)
        surf = brain.geo['lh']

        vertidx = np.where(src[0]['inuse'])[0]

        mlab.points3d(surf.x[vertidx], surf.y[vertidx],
                      surf.z[vertidx], color=(1, 1, 0), scale_factor=1.5)
        mlab.savefig(figures_path + sep + 'source_space.png')
        mlab.show()
    # create boundary element model (bem) surfaces

    if not os.path.isfile(bem_data) or force_files:
        # compute bem solution
        model = mne.make_bem_model(subject_mri_dir, conductivity=(0.3, 0.006, 0.3))
        mne.write_bem_surfaces(bem_data, model, overwrite=True)  # doctest: +SKIP
    else:
        # read bem
        model = mne.read_bem_surfaces(bem_data)

    if check_plots:
        fig = mne.viz.plot_bem(subject=subject_mri_dir, subjects_dir=subjects_dir,
                               brain_surfaces='white', orientation='coronal')
        fig.savefig(figures_path + sep + 'bem.png')

    if not os.path.exists(bem_sol_file_name) or force_files:
        bem_sol = mne.make_bem_solution(model)
        mne.write_bem_solution(bem_sol_file_name, bem_sol, overwrite=True)
    else:
        bem_sol = mne.read_bem_solution(bem_sol_file_name)

    # setup surface-based source space
    if not os.path.exists(volume_source_space_file) or force_files:
        vol_src = mne.setup_volume_source_space(
            subject_mri_name, mri=os.path.join(subjects_mri_dir, subject_mri_name, 'mri', 'aseg.mgz'), pos=10.0,
            bem=bem_sol_file_name,
            subjects_dir=subjects_mri_dir,
            add_interpolator=True,
            verbose=True)
        mne.write_source_spaces(volume_source_space_file, vol_src, overwrite=True)
        # # Generate the mixed source space
        # # Visualize the source space.
        # src.plot(subjects_dir=subjects_mri_dir)
    else:
        vol_src = mne.read_source_spaces(volume_source_space_file)

    # Computing the forward solution in surface space
    if not os.path.exists(forward_sol_file_name_surface) or force_files:
        forward = mne.make_forward_solution(raw.info, fname_trans, src, bem_sol, meg=True, eeg=False)
        mne.write_forward_solution(forward_sol_file_name_surface, forward, overwrite=True)

    # Computing the forward solution in volume space
    if not os.path.exists(forward_sol_file_name_volumen) or force_files:
        forward = mne.make_forward_solution(raw.info, fname_trans, vol_src, bem_sol, meg=True, eeg=False)
        mne.write_forward_solution(forward_sol_file_name_volumen, forward, overwrite=True)

    if not os.path.exists(forward_sol_file_name_surface_and_volume) or force_files:
        forward = mne.make_forward_solution(raw.info, fname_trans, src + vol_src, bem_sol, meg=True, eeg=False)
        mne.write_forward_solution(forward_sol_file_name_surface_and_volume, forward, overwrite=True)

    if check_plots:
        mne.viz.plot_alignment(raw.info, fname_trans, subject=subject_mri_name, dig=True,
                               meg=['helmet', 'sensors', 'ref'], subjects_dir=subjects_mri_dir,
                               surfaces=['head', 'brain'],
                               fig=1)
        mlab.savefig(figures_path + sep + 'alignment.png')
        mlab.show()

    #  remove eoc and ecg artifacts
    if remove_eoc_ecg:
        raw = remove_eog_ecg_artifacts(raw=raw, picks=picks, eog_ref_ch=eog_ref_ch, ecg_ref_ch=ecg_ref_ch)

    freq_of_interest = np.array([itm_rate, assr_rate])
    freq_of_interest = freq_of_interest[f_low < freq_of_interest]
    freq_of_interest = freq_of_interest[f_high > freq_of_interest]
    idx_event_ch = np.argwhere(np.array(raw.ch_names) == event_channel)[0][0]
    # prepare trigger channel by keeping positive values only, and scaling waveform so all events are detected with
    # code 1
    raw._data[idx_event_ch, raw._data[idx_event_ch, :] < 0] = 0
    raw._data[idx_event_ch, :] = 1.95 * raw._data[idx_event_ch, :] / raw._data[idx_event_ch, :].max()
    # detect triggers in scaled event channel
    events = mne.find_events(raw, initial_event=True, stim_channel=event_channel, min_duration=0.007, output='onset')
    print('n_events: {:}, average separation: {:} sec'.format(events.shape[0], np.mean(np.diff(events[:, 0])) / fs))
    # Next delay correction was only used for temporal phase analysis and figures
    # delay correction (insert tube is 6.30 m long)
    _samples_offset = np.round(fs * 6.30 / 340).astype(int)
    for _idx in range(events.shape[0]):
        events[_idx][0] = events[_idx][0] + _samples_offset
    if auto_events:
        events_rate = auto_events_rate
        events = interpolate_events(events, events_rate, fs)
        epoch_duration = 1 / events_rate

    # raw.filter(l_freq=f_low, h_freq=f_high)
    # mne.viz.plot_events(events, raw.info['sfreq'])
    # apply sharp filter
    dummy_raw = GenericInputData(fs=fs,
                                 data=raw._data[picks, :].T * u.T,
                                 channel_labels=np.array([_l for _l in raw.ch_names]),
                                 figures_path=subjects_dir + sep + subject
                                 )
    dummy_raw.run()
    raw_filtered = FilterData(dummy_raw,
                              low_pass=f_high,
                              high_pass=f_low,
                              keep_input_node=False)
    raw_filtered.run()
    raw._data[picks, :] = raw_filtered.output_node.data.T.value
    # epoch data
    epoch_duration = epoch_duration - 1 / fs
    epochs = mne.Epochs(raw, events, 1, 0, epoch_duration, proj=True, picks=picks,
                        baseline=None, preload=True, reject=None)
    epochs_noise = mne.Epochs(raw, events, 1, 0, epoch_duration, proj=True, picks=picks,
                              baseline=None, preload=True, reject=None)
    # clean epochs using DSS
    dummy_process = GenericInputData(fs=fs,
                                     data=epochs._data.T * u.T,
                                     channel_labels=np.array([_l for _l in epochs.ch_names]),
                                     figures_path=subjects_dir + sep + subject
                                     )
    dummy_process.run()
    dummy_process_noise = GenericInputData(fs=fs,
                                           data=raw._data[picks, :].T * u.T,
                                           channel_labels=np.array([_l for _l in epochs.ch_names]),
                                           figures_path=subjects_dir + sep + subject
                                           )
    dummy_process_noise.run()
    dss_filtered = CreateAndApplySpatialFilter(dummy_process,
                                               weight_data=False,
                                               components_to_plot=None,
                                               plot_power=False
                                               )
    dss_filtered.run()
    epochs._data = dss_filtered.output_node.data.T

    # removed evoked response to have 'noisy' data with minimal evoked response
    events_array = [SingleEvent(code=1, time_pos=events[_i][0] / fs * u.s) for _i in range(events.shape[0])]
    dummy_process_noise.output_node.events = Events(events=events_array)
    null_data = RemoveEvokedResponse(dummy_process_noise,
                                     keep_input_node=False,
                                     post_stimulus_interval=epoch_duration + 1 / fs,
                                     weighted_average=False,
                                     fade_in_out=False,
                                     event_code=1)
    null_data.run()
    # For null hypothesis I generate random events from the data, in this way performing incoherent average
    null_data.output_node.randomize_events(epoch_length=(epoch_duration + 1 / fs) * u.s, event_code=1)
    noise_epochs = EpochData(null_data,
                             post_stimulus_interval=epoch_duration + 1 / fs,
                             event_code=1
                             )
    noise_epochs.run()
    epochs_noise._data = noise_epochs.output_node.data.T

    # compute noise covariance using noise before the first trigger
    # This was used in the original study. No differences between each approach, but current one is more elegant
    # noise_cov = mne.compute_raw_covariance(raw,
    #                                        tmax=events[0][0] / fs,
    #                                        tstep=1 / np.min(freq_of_interest),
    #                                        # mag=4e-12,
    #                                        method='shrunk',
    #                                        rank=None)
    # Note that in this version of MNE I needed to set copy='auto' in compute_covariance as there is a bug in MNE
    noise_cov = mne.compute_covariance(epochs=epochs_noise,
                                       method='shrunk',
                                       rank=None)

    out = {'subjects_mri_dir': subjects_mri_dir,
           'subject_mri_name': subject_mri_name,
           'condition_path': condition_path,
           'freq_of_interest': freq_of_interest,
           'epoch_duration': epoch_duration,
           'picks': picks,
           'figures_path': figures_path,
           'events': events,
           'epochs': epochs,
           'weights': dss_filtered.weights[0, ...].T,
           'fname_trans': fname_trans,
           'source_space_file': source_space_file,
           'volume_source_space_file': volume_source_space_file,
           'bem_sol_file_name': bem_sol_file_name,
           'fsaverage_path': fsaverage_path,
           'stimuli_file': stimuli_file,
           'forward_sol_file_name_surface': forward_sol_file_name_surface,
           'forward_sol_file_name_volumen': forward_sol_file_name_volumen,
           'forward_sol_file_name_surface_volume': forward_sol_file_name_surface_and_volume,
           'raw_file_name': raw_file_name,
           'auto_events': auto_events,
           'noise_cov': noise_cov
           }

    return PipeOut(**out)


def run_case(subject: str = '',
             subjects_dir: str = '',
             f_low: float = 2.0,
             f_high: float = 8.0,
             bad_channels: list = [],
             condition_id: str = '',
             event_channel: str = '',
             method: str = 'sLORETA',
             auto_events: bool = True):
    # run MEG pipeline for case
    data_1 = basic_pipe_line(subject=subject,
                             subjects_dir=subjects_dir,
                             bads=bad_channels,
                             condition_id=condition_id,
                             remove_eoc_ecg=False,
                             eog_ref_ch='MEG 102',
                             ecg_ref_ch='MEG 052',
                             event_channel=event_channel,
                             f_low=f_low,
                             f_high=f_high,
                             auto_events=auto_events,
                             force_files=False
                             )
    # save generated data
    _file_name_base = 'fsaverage_{:d}_{:d}_{:}_auto_events_{:}'.format(int(f_low),
                                                                       int(f_high),
                                                                       method,
                                                                       data_1.auto_events)
    _file_name_1 = data_1.condition_path + 'data_' + _file_name_base + '.pkl'

    with open(_file_name_1, 'wb') as _f:
        pickle.dump(data_1, file=_f, protocol=5)


def get_dics_surface_space(data: PipeOut):
    fs = data.raw.info['sfreq']
    # create noise events
    data.forward = mne.read_forward_solution(data.forward_sol_file_name_surface)
    noise_cov = mne.compute_raw_covariance(data.raw, tmax=data.events[0][0] / fs, method='shrunk', rank=None)
    # noise_cov.plot(noise_epochs.info, proj=True)

    # Calculating the inverse operator

    inv = mne.minimum_norm.make_inverse_operator(data.evoked.info, data.forward, noise_cov, loose=1)
    # compute FFT for all channels
    yfft = np.abs(np.fft.fft(data.evoked.data, axis=1)) * 2 / data.evoked.data.shape[1]
    freq = np.arange(0, data.evoked.data.shape[1]) * fs / data.evoked.data.shape[1]
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(freq, yfft.T)
    ax.set_xlim(0, 60)
    # plt.show()
    fig.savefig(data.figures_path + sep + 'fft.png')
    fig.clf()
    index = np.argmin(np.abs((freq - data.freq_of_interest[0])))

    # ######Take the root-mean square along the time dimension and plot the result.
    stc = mne.minimum_norm.apply_inverse(data.evoked, inv, pick_ori="normal")
    s_rms = np.sqrt((stc ** 2).mean())
    brain = s_rms.plot(data.subject_mri_name, subjects_dir=data.subjects_mri_dir, hemi='both', figure=1,
                       size=600)
    vertno_max_r, time_max = stc.get_peak(hemi='rh')
    vertno_max_l, time_max = stc.get_peak(hemi='lh')
    brain.add_foci(vertno_max_r, coords_as_verts=True, hemi='rh')
    brain.add_foci(vertno_max_l, coords_as_verts=True, hemi='lh')
    mlab.show()

    csd = mne.time_frequency.csd_morlet(data.epochs, frequencies=data.freq_of_interest, n_cycles=6)
    _power = []
    weight_norms = [None, 'nai']
    pick_ori = 'max-power'
    fwd_free = mne.convert_forward_solution(data.forward, surf_ori=True)
    conunt = 0
    for _i, _cds in enumerate(csd):
        _filters = []
        for _norm in weight_norms:
            if _norm is None:
                inversion ='single'
                normalize_fwd = True
            else:
                inversion = 'matrix'
                normalize_fwd = False

            _filters.append(mne.beamformer.make_dics(
                data.raw.info, data.forward, _cds, reg=0.05, pick_ori=pick_ori, normalize_fwd=normalize_fwd,
                inversion=inversion, weight_norm=_norm))
            print(_filters[-1])

        for _filter, _norm in zip(_filters, weight_norms):
            power, f = mne.beamformer.apply_dics_csd(_cds, _filter)

            vertno_max_r, time_max = power.get_peak(hemi='rh')
            vertno_max_l, time_max = power.get_peak(hemi='lh')

            brain = power.plot(data.subject_mri_name, subjects_dir=data.subjects_mri_dir, hemi='both',
                               size=600, figure=conunt)
            brain.add_foci(vertno_max_r, coords_as_verts=True, hemi='rh')
            brain.add_foci(vertno_max_l, coords_as_verts=True, hemi='lh')

            _power.append(power)
            conunt += 1

            # ####Rotate the view and add a title.
            mlab.view(0, 0, 550, [0, 0, 0])
            mlab.title('DICS power map, approach {:}, {: .1f} Hz'.format(_norm, _cds.frequencies[0]), height=0.9)
            mlab.show()
    data.power = _power
    data.csd = csd


def get_source(data: PipeOut,
               evoked=None,
               method='dSPM',
               source_type='surface'
               ):
    if source_type == 'surface':
        data.forward = mne.read_forward_solution(data.forward_sol_file_name_surface)
        pick_ori = "normal"
        loose = "auto"
    if source_type == 'mixed_source':
        data.forward = mne.read_forward_solution(data.forward_sol_file_name_surface_volume)
        loose = dict(surface=0.2, volume=1.)
        pick_ori = "vector"

    # Calculating the inverse operator
    snr = 3.0
    lambda2 = 1.0 / snr ** 2
    inv = mne.minimum_norm.make_inverse_operator(evoked.info, data.forward, data.noise_cov, loose=loose)

    stc = mne.minimum_norm.apply_inverse(evoked, inv, method=method, pick_ori=pick_ori, lambda2=lambda2)
    _, _stc_sub_name = os.path.split(stc.subject)
    stc.subject = _stc_sub_name

    # morph data to perform group analysis
    if source_type == 'mixed_source':
        src_fs_vol = mne.read_source_spaces(data.subjects_mri_dir + sep + 'fsaverage' + sep + 'vol-src.fif')
        src_fs_source = mne.read_source_spaces(data.subjects_mri_dir + sep + 'fsaverage' + sep + 'oct6-src.fif')
        src = src_fs_source + src_fs_vol
        for _s in src:
            _, _stc_sub_name = os.path.split(_s['subject_his_id'])
            _s['subject_his_id'] = _stc_sub_name
        for _s in inv['src']:
            _, _stc_sub_name = os.path.split(_s['subject_his_id'])
            _s['subject_his_id'] = _stc_sub_name
        stc.subject = data.subjects_mri_dir + os.sep + _stc_sub_name
        morph_vol = mne.compute_source_morph(inv['src'], subject_from=stc.subject,
                                             subjects_dir=data.subjects_mri_dir,
                                             niter_sdr=[10, 10, 5],
                                             niter_affine=[10, 10, 5],
                                             src_to=src, verbose=True,
                                             spacing=5)
        stc_fsaverage = morph_vol.apply(stc)
        # brain = stc.plot(
        #     hemi='both', src=inv['src'], views='coronal',
        #     initial_time=0,
        #     subjects_dir=data.subjects_mri_dir,
        #     surface='white',
        #     brain_kwargs=dict(silhouette=True)
        # )

    if source_type == 'surface':
        fsave_vertices = [s['vertno'] for s in inv['src']]
        morph = mne.compute_source_morph(stc, subject_from=stc.subject,
                                         subject_to='fsaverage', subjects_dir=data.subjects_mri_dir,
                                         spacing=fsave_vertices)

        stc_fsaverage = morph.apply(stc)
    return stc_fsaverage


def get_source_epochs(data: PipeOut, epochs=None, method='dSPM',
                      source_type='mixed_source'):
    if source_type == 'surface':
        data.forward = mne.read_forward_solution(data.forward_sol_file_name_surface)
        pick_ori = "normal"
        loose = "auto"
    if source_type == 'mixed_source':
        data.forward = mne.read_forward_solution(data.forward_sol_file_name_surface_volume)
        loose = dict(surface=0.2, volume=1.)
        pick_ori = None

    # Calculating the inverse operator
    snr = 3.0
    lambda2 = 1.0 / snr ** 2
    inv = mne.minimum_norm.make_inverse_operator(epochs.info, data.forward, data.noise_cov, loose=loose)
    stc = mne.minimum_norm.apply_inverse_epochs(epochs, inv, method=method, pick_ori=pick_ori, lambda2=lambda2)
    # morph data to perform group analysis
    ave_stc = []
    fsave_vertices = [s['vertno'] for s in inv['src']]
    for _i, _stc in enumerate(stc):
        _, _stc_sub_name = os.path.split(_stc.subject)
        _stc.subject = _stc_sub_name
        if source_type == 'mixed_source':
            src_fs_vol = mne.read_source_spaces(data.subjects_mri_dir + sep + 'fsaverage' + sep + 'vol-src.fif')
            src_fs_source = mne.read_source_spaces(data.subjects_mri_dir + sep + 'fsaverage' + sep + 'oct6-src.fif')
            src = src_fs_source + src_fs_vol
            for _s in src:
                _, _stc_sub_name = os.path.split(_s['subject_his_id'])
                _s['subject_his_id'] = _stc_sub_name
            for _s in inv['src']:
                _, _stc_sub_name = os.path.split(_s['subject_his_id'])
                _s['subject_his_id'] = _stc_sub_name
            morph_vol = mne.compute_source_morph(inv['src'], subject_from=_stc.subject,
                                                 subjects_dir=data.subjects_mri_dir,
                                                 niter_sdr=[10, 10, 5], niter_affine=[10, 10, 5],
                                                 src_to=src, verbose=True,
                                                 spacing=5)
            _stc_fsaverage = morph_vol.apply(_stc)
        if source_type == 'surface':
            print('epoch {:} morphing {:} to {:}'.format(_i, _stc_sub_name, 'fsaverage'))
            morph = mne.compute_source_morph(_stc, subject_from=_stc.subject,
                                             subject_to='fsaverage', subjects_dir=data.subjects_mri_dir,
                                             spacing=fsave_vertices)
            _stc_fsaverage = morph.apply(_stc)
        ave_stc.append(_stc_fsaverage)

    return ave_stc


def get_beam_source(data: PipeOut, evoked=None, method='dSPM', file_name='stc.fif'):
    # if (os.path.isfile(os.path.join(data.condition_path, file_name, '-lh.stc')) and
    #         os.path.isfile(os.path.join(data.condition_path, file_name, '-lh.stc'))):
    #     return None

    fs = data.raw.info['sfreq']
    # Calculating the inverse operator
    snr = 3.0
    lambda2 = 1.0 / snr ** 2
    inv = mne.minimum_norm.make_inverse_operator(evoked.info, data.forward, data.noise_cov, loose=1)
    data_cov = mne.compute_covariance(data.epochs, method='shrunk', tmin=0.132, tmax=0.137)
    filter = mne.beamformer.make_lcmv(data.raw.info, data.forward, data.noise_cov, reg=0.05, pick_ori='max-power',
                                      weight_norm='nai', rank=None)
    # stc = mne.beamformer.apply_lcmv(data.evoked, filter, max_ori_out='signed')
    stc = mne.minimum_norm.apply_inverse(evoked, inv, method=method, pick_ori=None, lambda2=lambda2)
    # compute FFT for all channels
    yfft = np.abs(np.fft.fft(evoked.data, axis=1)) * 2 / evoked.data.shape[1]
    freq = np.arange(0, evoked.data.shape[1]) * fs / evoked.data.shape[1]
    plt.ioff()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(freq, yfft.T)
    ax.set_xlim(0, 60)
    plt.show(block=False)
    fig.savefig(data.figures_path + sep + 'fft.png')

    index = np.argmin(np.abs((freq - data.freq_of_interest[0])))

    # ######Take the root-mean square along the time dimension and plot the result.
    s_rms = np.sqrt((stc ** 2).mean())
    # vertno_max_l, time_max_l = stc.get_peak(hemi='lh')
    # vertno_max_r, time_max_r = stc.get_peak(hemi='rh')
    #
    # brain = stc.plot(data.subject_mri_name, subjects_dir=data.subjects_mri_dir, hemi='both', figure=1, size=600,
    #                  initial_time=time_max_r)
    # brain.add_foci(vertno_max_r, coords_as_verts=True, hemi='rh')
    # brain.add_foci(vertno_max_l, coords_as_verts=True, hemi='lh')
    # mlab.show()
    src = inv['src']
    img = s_rms.as_volume(src, mri_resolution=False)  # set True for full MRI resolution
    t1_fname = os.path.join(data.subjects_mri_dir, data.subject_mri_name, 'mri', 'T1.mgz')
    plot_stat_map(index_img(img, 0), t1_fname, threshold=3,
                  title='{:} t= {:.3f} s.)'.format(method, s_rms.times[0]))
    plt.show()
    # # morph data to perform group analysis
    # morph = mne.compute_source_morph(stc, subject_from=stc.subject,
    #                                  subject_to='fsaverage', subjects_dir=data.subjects_mri_dir)
    #
    # stc_fsaverage = morph.apply(stc)
    # stc_fsaverage.save(os.path.join(data.condition_path, file_name))
    return s_rms


def interpolate_events(orig_events, interpolation_rate, fs):
    intervals = np.diff(orig_events, axis=0)
    m_interval = np.round(np.mean(intervals, axis=0))
    n_samples = int(1/interpolation_rate * fs)
    new_events = np.empty((0, 3))
    for _i, _int in enumerate(intervals):
        n_e = _int[0] // n_samples
        _new_events = np.vstack((orig_events[_i, 0] + np.arange(0, n_e) * n_samples,
                                 orig_events[_i, 1]*np.ones(np.arange(0, n_e).shape),
                                 orig_events[_i, 2] * np.ones(np.arange(0, n_e).shape))).T
        new_events = np.vstack((new_events, _new_events))
    # fill last event
    n_e = m_interval[0] // n_samples
    _new_events = np.vstack((orig_events[-1, 0] + np.arange(0, n_e) * n_samples,
                             orig_events[-1, 1] * np.ones(np.arange(0, n_e).shape),
                             orig_events[-1, 2] * np.ones(np.arange(0, n_e).shape))).T
    new_events = np.vstack((new_events, _new_events))
    return new_events.astype(int)


def get_sources_data(stc_in=None,
                     stc_p_values=None,
                     stc_z_values=None,
                     label='',
                     subjects_mri_dir='',
                     surface='inflated',
                     measurement_type: str = '',
                     frequency_tested: float = None,
                     alpha=0.05,
                     dof: float = None,
                     n_sources: int = None):
    if isinstance(stc_in, mne.SourceEstimate) or isinstance(stc_in, mne.MixedSourceEstimate):
        parc = 'aparc.a2009s'
        surface_regions = ['G_temp_sup-G_T_transv',  # primary auditory cortex
                           'G_temp_sup-Plan_tempo',  # planum temporale secondary auditory cortex
                           'S_temporal_transverse',  # transverse temporal sulcus
                           'Lat_Fis-post',
                           'S_circular_insula_inf',
                           'S_temporal_sup',
                           'G_temp_sup-Lateral',
                           'G_and_S_subcentral',
                           'G_pariet_inf-Supramar',
                           'S_postcentral',
                           'S_central'
                           ]
    if isinstance(stc_in, mne.VolSourceEstimate) or isinstance(stc_in, mne.MixedSourceEstimate):
        src_vol = mne.read_source_spaces(fname=subjects_mri_dir + 'fsaverage' + os.sep + 'vol-src.fif')
        volume_regions = ['Left-Amygdala',
                          'Left-Thalamus-Proper',
                          'Left-Cerebellum-Cortex',
                          'Left-Cerebral-Cortex',
                          'Left-Cerebral-White-Matter',
                          'Brain-Stem',
                          'Right-Amygdala',
                          'Right-Thalamus-Proper',
                          'Right-Cerebellum-Cortex',
                          'Right-Cerebral-Cortex',
                          'Right-Cerebral-White-Matter'
                          ]
    output = pd.DataFrame()
    if isinstance(stc_in, mne.SourceEstimate) or isinstance(stc_in, mne.MixedSourceEstimate):
        _stc_in = stc_in if isinstance(stc_in, mne.SourceEstimate) else stc_in.surface()
        _stc_p_values = stc_p_values if isinstance(stc_p_values, mne.SourceEstimate) else stc_p_values.surface()
        _stc_z_values = stc_z_values if isinstance(stc_z_values, mne.SourceEstimate) else stc_z_values.surface()
        for _hemi in ['rh', 'lh']:
            for _i, _region in enumerate(surface_regions):
                labels_parc = mne.read_labels_from_annot('fsaverage',
                                                         parc=parc,
                                                         subjects_dir=subjects_mri_dir,
                                                         regexp=_region,
                                                         surf_name=surface,
                                                         hemi=_hemi)
                stc_in_region = _stc_in.in_label(labels_parc[0])
                stc_p_values_in_region = _stc_p_values.in_label(labels_parc[0])
                stc_z_values_in_region = _stc_z_values.in_label(labels_parc[0])
                _data_values = np.hstack((stc_in_region.data,
                                          stc_p_values_in_region.data,
                                          stc_z_values_in_region.data))

                _data = pd.DataFrame(data=_data_values, columns=['value', 'p_value', 'z_value'])
                _data['anat_label'] = _region
                _data['type'] = measurement_type
                _data['hemisphere'] = _hemi
                _data['condition'] = label
                _data['frequency_tested'] = frequency_tested
                _data['n_sources'] = n_sources
                _data['dof'] = dof.min()
                _data['alpha_critic'] = alpha
                output = pd.concat([output, _data])
    if isinstance(stc_in, mne.MixedSourceEstimate):
        mri_path = os.path.join(subjects_mri_dir + 'fsaverage', 'mri', 'aseg.mgz')
        for _i, _region in enumerate(volume_regions):
            if _region.startswith('Left-'):
                _hemi = 'lh'
            elif _region.endswith('Right-'):
                _hemi = 'rh'
            else:
                _hemi = 'middle'

            stc_in_region = stc_in.volume().in_label(_region,
                                                     mri=mri_path,
                                                     src=src_vol)
            stc_p_values_in_region = stc_p_values.volume().in_label(_region,
                                                                    mri=mri_path,
                                                                    src=src_vol)
            stc_z_values_in_region = stc_z_values.volume().in_label(_region,
                                                                    mri=mri_path,
                                                                    src=src_vol)
            _data_values = np.hstack((stc_in_region.data,
                                      stc_p_values_in_region.data,
                                      stc_z_values_in_region.data))

            _data = pd.DataFrame(data=_data_values, columns=['value', 'p_value', 'z_value'])
            _data['anat_label'] = _region
            _data['type'] = measurement_type
            _data['hemisphere'] = _hemi
            _data['condition'] = label
            _data['frequency_tested'] = frequency_tested
            _data['n_sources'] = n_sources
            _data['dof'] = dof.min()
            _data['alpha_critic'] = alpha
            output = output.append(_data)
    return output


def fdr_pvalue(alpha_level: float = 0.05,
               p_values: np.array = None):
    #  Benjamini–Hochberg step-up procedure(1995)
    s_p_values = np.sort(p_values.flatten())
    _conditional = alpha_level * np.arange(1, s_p_values.size + 1) / s_p_values.size
    _idx = np.argwhere(s_p_values < _conditional)
    _p = 0.0
    if _idx.size:
        _p = s_p_values[_idx].max()
    return _p


def plot_surface(stc_in=None,
                 stc_p_values=None,
                 file_name=None,
                 lims=[2, 4, 10],
                 label='',
                 subjects_mri_dir='',
                 color_map='hot',
                 show_colorbar=False,
                 surface='white',
                 significant_only=False,
                 method: str = 'dSPM',
                 alpha=0.05,
                 save_video: bool = False,
                 plot_parcellation=False
                 ):
    views = 'lateral'
    domain = 'surface'
    parc = 'aparc.a2009s'
    regions = ['G_temp_sup-G_T_transv',  # primary auditory cortex
               'G_temp_sup-Plan_tempo',  # planum temporale secondary auditory cortex
               'S_temporal_transverse',  # transverse temporal sulcus
               'Lat_Fis-post',
               'G_and_S_subcentral',
               'G_pariet_inf-Supramar',
               'S_postcentral',
               'S_central',
               'S_temporal_sup']
    colors = sns.color_palette("Paired", len(regions))
    if isinstance(stc_in, mne.MixedSourceEstimate):
        views = 'rostral'
        domain = 'mixed_sources'

    # set non-significant values out of range of plot
    if significant_only:
        stc_in.data[stc_p_values.data > alpha] = min(lims) - 1000
    src_src = None
    if isinstance(stc_in, mne.SourceEstimate) or isinstance(stc_in, mne.MixedSourceEstimate):
        src_src = mne.read_source_spaces(fname=subjects_mri_dir + 'fsaverage' + os.sep + 'oct6-src.fif')
        src = src_src
    if isinstance(stc_in, mne.VolSourceEstimate) or isinstance(stc_in, mne.MixedSourceEstimate):
        src_vol = mne.read_source_spaces(fname=subjects_mri_dir + 'fsaverage' + os.sep + 'vol-src.fif')
        src = src_src + src_vol
        surface = 'white'

    brain_r = stc_in.plot(subjects_dir=subjects_mri_dir,
                          src=src,
                          subject='fsaverage',
                          hemi='rh',
                          size=600,
                          clim=dict(kind='value', lims=lims),
                          initial_time=0,
                          background='white',
                          surface=surface,
                          cortex='low_contrast',
                          colormap=color_map,
                          colorbar=show_colorbar,
                          views=views,
                          time_label=None,
                          alpha=1,
                          figure=1,
                          time_viewer=True,
                          show_traces=False)
    brain_r.add_text(0.2, 0.85, text=label, name='condition1_l', font_size=24, color='black')
    if plot_parcellation:
        for _i, _region in enumerate(regions[0:9]):
            labels_parc = mne.read_labels_from_annot('fsaverage',
                                                     parc=parc,
                                                     subjects_dir=subjects_mri_dir,
                                                     regexp=_region,
                                                     surf_name=surface)
            [brain_r.add_label(_label, borders=0, color=colors[_i], scalar_thresh=None, alpha=1)
             for _label in labels_parc if _label.name.endswith('-rh')]

    brain_l = stc_in.plot(subjects_dir=subjects_mri_dir,
                          src=src,
                          subject='fsaverage',
                          hemi='lh',
                          size=600,
                          clim=dict(kind='value', lims=lims),
                          initial_time=0,
                          background='white',
                          surface=surface,
                          cortex='low_contrast',
                          colormap=color_map,
                          colorbar=show_colorbar,
                          views=views,
                          time_label=None,
                          title=label,
                          alpha=1 if False else 1.0,
                          figure=2,
                          time_viewer=True,
                          show_traces=False)
    brain_l.add_text(0.2, 0.85, text=label, name='condition1_l', font_size=24, color='black')
    if plot_parcellation:
        for _i, _region in enumerate(regions[0:9]):
            labels_parc = mne.read_labels_from_annot('fsaverage',
                                                     parc=parc,
                                                     subjects_dir=subjects_mri_dir,
                                                     regexp=_region,
                                                     surf_name=surface)
            [brain_l.add_label(_label, borders=0, color=colors[_i], scalar_thresh=None)
             for _label in labels_parc if _label.name.endswith('-lh')]

    brain_both = stc_in.plot(subjects_dir=subjects_mri_dir,
                             size=(1200, 400),
                             src=src,
                             subject='fsaverage',
                             hemi='split',
                             clim=dict(kind='value', lims=lims),
                             initial_time=0,
                             background='white',
                             surface=surface,
                             cortex='low_contrast',
                             colormap=color_map,
                             colorbar=show_colorbar,
                             views=views,
                             time_label=None,
                             alpha=1,
                             figure=3,
                             time_viewer=True,
                             show_traces=False)
    brain_both.add_text(0.2, 0.85, text=label, name='condition', font_size=24, color='black')

    if isinstance(stc_in, mne.MixedSourceEstimate):
        for _s in src:
            _, _stc_sub_name = os.path.split(_s['subject_his_id'])
            _s['subject_his_id'] = stc_in.subject
        img = stc_in.volume().as_volume(src,
                                        mri_resolution=True)  # set True for full MRI resolution
        mri_path = os.path.join(subjects_mri_dir + 'fsaverage', 'mri', 'aseg.mgz')
        image = plot_stat_map(index_img(img, 0), mri_path, threshold=1.,
                              title='%s (t=%.1f s.)' % (method, stc_in.times[0]))
        image.savefig(file_name + '_{:}_volumne.png'.format(domain))
        image.close()

    print("saving figure: {:}".format(file_name))
    if save_video:
        brain_l.save_movie(file_name + '_time_course_lh.mov', tmin=0, tmax=None, time_dilation=16,
                           bitrate=1000000)
        brain_r.save_movie(file_name + '_time_course_rh.mov', tmin=0, tmax=None, time_dilation=16,
                           bitrate=1000000)
        brain_both.save_movie(file_name + '_time_course_l_and_h.mov', tmin=0, tmax=None, time_dilation=16,
                              bitrate=1000000)
    else:
        brain_l.show()
        brain_l.save_image(file_name + '_{:}_lh.png'.format(domain))
        brain_r.show()
        brain_r.save_image(file_name + '_{:}_rh.png'.format(domain))
        brain_both.show()
        brain_both.save_image(file_name + '_{:}_r_and_h.png'.format(domain))
    brain_r.close()
    brain_l.close()
    brain_both.close()


def read_source_by_epochs(data_path: str = None,
                          sampling_rate: float = 125,
                          method: str = '',
                          freqs_of_interest: [float] = None,
                          source_type="surface",
                          solve_in_frequency_domain=False,
                          root_path=None):
    data = pickle.load(open(data_path, "rb"))
    if not os.path.exists(data.forward_sol_file_name_surface) and root_path is not None:
        _new_path = root_path + data.forward_sol_file_name_surface[data.forward_sol_file_name_surface.find('/MEG')::]
        data.forward_sol_file_name_surface = _new_path
        _new_path = root_path + data.subjects_mri_dir[data.subjects_mri_dir.find('/MEG')::]
        data.subjects_mri_dir = _new_path

    data.epochs.resample(sampling_rate)
    all_stcs = source_by_epochs(data=data,
                                method=method,
                                solve_in_frequency_domain=solve_in_frequency_domain,
                                sampling_rate=sampling_rate,
                                freqs_of_interest=freqs_of_interest,
                                source_type=source_type)

    return all_stcs


def read_source_by_average(data_path: str = None,
                           sampling_rate: float = 125,
                           method: str = '',
                           subjects_mri_dir: str = None,
                           subjects_dir: str = None,
                           subject: str = 'fsaverage',
                           condition: str = '',
                           freqs_of_interest: [float] = None,
                           source_type="surface",
                           solve_in_frequency_domain=False):
    data = pickle.load(open(data_path, "rb"))
    data.epochs.resample(sampling_rate)
    out = []
    average = data.epochs.average()
    for _f in freqs_of_interest:
        if solve_in_frequency_domain:
            (w, spectral_magnitude, sig_plus_noise_var, rn, snr, exact_frequencies, y_fft) = \
                get_discrete_frequencies_weights(
                    epochs=average.data.T[:, :, None],
                    frequencies=np.array([_f]),
                    weighted_average=False,
                    fs=data.epochs.info['sfreq'])
            average.data = np.transpose(y_fft, [2, 1, 0]).reshape(-1, 1)
        stc_aux = get_source(data=data, evoked=average, method=method, source_type=source_type)
        src_src = mne.read_source_spaces(fname=subjects_mri_dir + 'fsaverage' + os.sep + 'oct6-src.fif')
        src_vol = mne.read_source_spaces(fname=subjects_mri_dir + 'fsaverage' + os.sep + 'vol-src.fif')
        src = src_src + src_vol
        # stc_aux.data = np.abs(stc_aux.data)
        for _s in src:
            _, _stc_sub_name = os.path.split(_s['subject_his_id'])
            _s['subject_his_id'] = _stc_sub_name
        # for _s in inv['src']:
        #     _, _stc_sub_name = os.path.split(_s['subject_his_id'])
        #     _s['subject_his_id'] = _stc_sub_name
        brain = stc_aux.plot(initial_time=0, hemi='both', subjects_dir=subjects_mri_dir, src=src)
        brain.save_image(os.path.join(subjects_dir, subject, condition, 'figures', 'vector_plot.png'))
        brain.close()
        # project to generate a dummy mixed_domain
        # stc_aux = stc_aux.project('pca', src=src)[0]
        # compute mean across vector magnitude to prevent vector cancellation (equivalent to average the spectral
        # magnitude)
        # stc_aux.data = np.mean(stc_aux.data, axis=1, keepdims=True)

        out.append(stc_aux)
    return out


def get_source_by_average(sampling_rate: float = 125,
                          method='dSPM',
                          subjects_mri_dir: str = None,
                          subjects_dir: str = None,
                          subject: str = 'fsaverage',
                          bads: list = [],
                          f_low=2.0,
                          f_high=8.0,
                          event_channel='MISC 017',
                          condition: str = '',
                          freqs_of_interest: [float] = [None],
                          source_type="surface",
                          solve_in_frequency_domain=False,
                          eog_ref_ch='MEG 102',
                          ecg_ref_ch='MEG 052',
                          remove_eoc_ecg: bool = False,
                          auto_events: bool = True,
                          auto_events_rate: float = None
                          ):

    data = basic_pipe_line(subject=subject,
                           subjects_dir=subjects_dir,
                           bads=bads,
                           condition_id=condition,
                           remove_eoc_ecg=remove_eoc_ecg,
                           eog_ref_ch=eog_ref_ch,
                           ecg_ref_ch=ecg_ref_ch,
                           event_channel=event_channel,
                           f_low=f_low,
                           f_high=f_high,
                           auto_events=auto_events,
                           auto_events_rate=auto_events_rate
                           )

    data.epochs.resample(sampling_rate)
    out = []
    average = data.epochs.average()
    for _f in freqs_of_interest:
        if solve_in_frequency_domain:
            (w, spectral_magnitude, sig_plus_noise_var, rn, snr, exact_frequencies, y_fft) = \
                get_discrete_frequencies_weights(
                    epochs=average.data.T[:, :, None],
                    frequencies=np.array([_f]),
                    weighted_average=False,
                    fs=data.epochs.info['sfreq'])
            average.data = np.transpose(y_fft, [2, 1, 0]).reshape(-1, 1)
        stc_aux = get_source(data=data, evoked=average, method=method, source_type=source_type)
        src_src = mne.read_source_spaces(fname=subjects_mri_dir + 'fsaverage' + os.sep + 'oct6-src.fif')
        src_vol = mne.read_source_spaces(fname=subjects_mri_dir + 'fsaverage' + os.sep + 'vol-src.fif')
        src = src_src + src_vol
        # stc_aux.data = np.abs(stc_aux.data)
        for _s in src:
            _, _stc_sub_name = os.path.split(_s['subject_his_id'])
            _s['subject_his_id'] = _stc_sub_name
        # for _s in inv['src']:
        #     _, _stc_sub_name = os.path.split(_s['subject_his_id'])
        #     _s['subject_his_id'] = _stc_sub_name
        brain = stc_aux.plot(initial_time=0, hemi='both', subjects_dir=subjects_mri_dir, src=src)
        brain.save_image(os.path.join(subjects_dir, subject, condition, 'figures', 'vector_plot.png'))
        brain.close()
        # project to generate a dummy mixed_domain
        # stc_aux = stc_aux.project('pca', src=src)[0]
        # compute mean across vector magnitude to prevent vector cancellation (equivalent to average the spectral
        # magnitude)
        # stc_aux.data = np.mean(stc_aux.data, axis=1, keepdims=True)

        out.append(stc_aux)
    return out


def source_by_epochs(data: PipeOut,
                     method='dSPM',
                     source_type='mixed_source',
                     sampling_rate: float = 125,
                     freqs_of_interest: [float] = None,
                     solve_in_frequency_domain=False,
                     root_path=None):
    data.epochs.resample(sampling_rate)

    if not os.path.exists(data.forward_sol_file_name_surface) and root_path is not None:
        _new_path = root_path + data.forward_sol_file_name_surface[data.forward_sol_file_name_surface.find('/MEG')::]
        data.forward_sol_file_name_surface = _new_path
        _new_path = root_path + data.subjects_mri_dir[data.subjects_mri_dir.find('/MEG')::]
        data.subjects_mri_dir = _new_path

    out = []
    epochs = copy.copy(data.epochs)
    # # apply weights and scale so mean corresponds to the weighted mean
    # # epochs._data = epochs._data.shape[0] * epochs._data * data.weights[..., None] / np.sum(
    # #     data.weights[..., None], axis=0, keepdims=True)
    for _f in freqs_of_interest:
        if solve_in_frequency_domain:
            _data_transposed = data.epochs._data.transpose([2, 1, 0])
            (w, spectral_magnitude, sig_plus_noise_var, rn, snr, exact_frequencies, y_fft) = \
                get_discrete_frequencies_weights(
                    epochs=_data_transposed,
                    frequencies=np.array([_f]),
                    weighted_average=False,
                    fs=data.epochs.info['sfreq'])
            epochs._data = np.transpose(y_fft, [2, 1, 0])
        stc_epochs = get_source_epochs(data=data, epochs=epochs, method=method,
                                       source_type=source_type)
        out.append(stc_epochs)
    return out


def get_sources_main_components(stc_in,
                                subjects_dir: str = '',
                                source=None,
                                figure_path='',
                                file_name='',
                                label='',
                                subjects_mri_dir='',
                                surface='inflated',
                                freq=None):

    parc = 'aparc.a2009s'
    regions = ['G_temp_sup-G_T_transv',  # primary auditory cortex
               'G_temp_sup-Plan_tempo',  # planum temporale secondary auditory cortex
               'S_temporal_transverse',  # transverse temporal sulcus
               'Lat_Fis-post',
               'S_circular_insula_inf',
               'S_temporal_sup',
               'G_temp_sup-Lateral',
               'G_and_S_subcentral',
               'G_pariet_inf-Supramar',
               'S_postcentral',
               'S_central'
               ]

    _data = pd.DataFrame()
    for _hemi in ['rh', 'lh']:
        # fig_out = plt.figure()
        # ax = fig_out.add_subplot(111)
        color = cm.rainbow(np.linspace(0, 1, len(regions)))
        for _i, _region in enumerate(regions):
            labels_parc = mne.read_labels_from_annot('fsaverage',
                                                     parc=parc,
                                                     subjects_dir=subjects_mri_dir,
                                                     regexp=_region,
                                                     surf_name=surface,
                                                     hemi=_hemi)
            stc_mean_label = stc_in.in_label(labels_parc[0])
            # data = np.abs(stc_mean_label.data)
            # stc_mean_label.data[data < 0.6 * np.max(data)] = 0.
            func_labels, _ = mne.stc_to_label(stc_mean_label,
                                              src=source,
                                              smooth=True,
                                              subjects_dir=subjects_dir,
                                              connected=True,
                                              verbose='error')
            anat_label = mne.read_labels_from_annot('fsaverage',
                                                    parc=parc,
                                                    subjects_dir=subjects_mri_dir,
                                                    regexp=_region,
                                                    surf_name=surface,
                                                    hemi=_hemi)[0]
            if func_labels != []:
                # take first as func_labels are ordered based on maximum values in stc
                func_label = func_labels[0]
                stc_func_label = stc_in.in_label(func_label)
                pca_func = stc_in.extract_label_time_course(func_label, source, mode='pca_flip')[0]
                pca_func *= np.sign(pca_func[np.argmax(np.abs(pca_func))])

            # extract the anatomical time course for each label
            if anat_label != []:
                # correct subject name that comes with full path and makes MNE to complain
                _stc_in = copy.copy(stc_in)
                anat_label.subject = source._subject
                _stc_in.subject = source._subject
                stc_anat_label = _stc_in.in_label(anat_label)
                pca_anat = _stc_in.extract_label_time_course(anat_label, source, mode='pca_flip')[0]

            # optimize_func = lambda x: x[0] * np.sin(2 * np.pi * freq * stc_anat_label.times + x[1]) + x[2] - pca_anat
            # guess_std = 3 * np.std(pca_anat) / (2 ** 0.5) / (2 ** 0.5)
            # est_amp, est_phase, est_mean = leastsq(optimize_func, [guess_std, 0, np.mean(pca_anat)])[0]
            # data_fit = est_amp * np.sin(2 * np.pi * freq * stc_anat_label.times + est_phase) + est_mean
            # ax.plot(1e3 * stc_anat_label.times, pca_anat,
            #         linestyle='-',
            #         label='Anatomical %s' % _region,
            #         color=color[_i])
            # ax.plot(1e3 * stc_anat_label.times, data_fit,
            #         linestyle=':',
            #         label='Anatomical %s' % _region,
            #         color=color[_i])
            # ax.set_xlabel('Time [ms]')
            _yfft = np.fft.rfft(pca_anat, axis=0)
            _freqs = np.arange(0, _yfft.shape[0]) * stc_in.sfreq / pca_anat.shape[0]
            _pos = np.nanargmin(np.abs(_freqs - freq))
            _phase = np.angle(_yfft[_pos])
            _yfft_mag = 2 * np.abs(_yfft) / pca_anat.shape[0]
            _amplitude = _yfft_mag[_pos]
            _results = {'condition': label,
                        'hemisphere': _hemi,
                        'spectral_magnitude': _amplitude,
                        'spectral_phase': _phase,
                        'anat_label': _region,
                        'frequency_tested': _freqs[_pos]}
            _data = pd.concat([_data, pd.DataFrame([_results])], ignore_index=True)
            # plt.plot(_freqs, _yfft_mag)
            # plt.plot(_freqs[_pos], _amplitude, 'o')
        # fig_out.legend()
        # fig_out.savefig(figure_path + os.sep + file_name + '_time_course_%s_roi.pdf' % _hemi)
        # plt.close(fig=fig_out)

        # ax.set_title(label + ' {:}'.format(_hemi))
    return _data
