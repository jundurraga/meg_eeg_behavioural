from meg_processing.tools import set_file_name, basic_pipe_line
from peegy.processing.pipe.io import ChannelItem, GenericInputData
from peegy.processing.pipe.general import FilterData
from peegy.processing.pipe.pipeline import PipePool
from peegy.processing.pipe.epochs import AverageEpochsFrequencyDomain, AverageEpochs
from peegy.processing.pipe.storage import MeasurementInformation, SubjectInformation, SaveToDatabase
from peegy.processing.pipe.statistics import PhaseLockingValue
import astropy.units as u
import numpy as np
import itertools
import json
import matplotlib.pyplot as plt
import pandas as pd
from os.path import sep
import os
from environment import subjects_dir, data_path_meg
import matplotlib
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')


def run_case(subject: str = '',
             condition_id: str = '',
             database_path: str = '',
             bad_channels: [str] = [],
             event_channel: str = '',
             f_low: float = 2.0,
             f_high: float = 48.0
             ):

    data_file = set_file_name(subjects_directory=subjects_dir, subject=subject, condition='',
                              name='results.json'.format(subject))

    data = basic_pipe_line(subject=subject,
                           subjects_dir=subjects_dir,
                           bads=bad_channels,
                           condition_id=condition_id,
                           remove_eoc_ecg=False,
                           eog_ref_ch='MEG 102',
                           ecg_ref_ch='MEG 052',
                           event_channel=event_channel,
                           f_low=f_low,
                           f_high=f_high,
                           auto_events=False
                           )

    fs = data.epochs.info['sfreq']
    epochs = data.epochs
    dummy_process = GenericInputData(data=epochs._data.T,
                                     fs=fs * u.Hz,
                                     channel_labels=np.array([_l for _l in epochs.ch_names]))
    dummy_process.run()

    test_frequencies = np.array([6.4102564102564106 / 2, 6.4102564102564106, 44.871794871794876])
    pipeline = PipePool()
    # filter data for time-domain plot
    pipeline['time_epochs_itd_fr'] = FilterData(dummy_process,
                                                high_pass=None,
                                                low_pass=8.0 * u.Hz)
    pipeline['time_epochs_sub_harmonic'] = FilterData(dummy_process,
                                                      high_pass=None,
                                                      low_pass=4.0 * u.Hz)
    pipeline.run()

    pipeline['time_average_ipm_fr'] = AverageEpochs(pipeline['time_epochs_itd_fr'],
                                                    weighted_average=True,
                                                    weight_across_epochs=False)
    pipeline['time_average_sub_harmonic'] = AverageEpochs(pipeline['time_epochs_sub_harmonic'],
                                                          weighted_average=True,
                                                          weight_across_epochs=False)

    # frequency-domain average of ITD-FR and ASSR
    pipeline['fft_ave_full_freq'] = AverageEpochsFrequencyDomain(dummy_process,
                                                                 weighted_average=True,
                                                                 weight_across_epochs=False,
                                                                 weighted_frequency_domain=False,
                                                                 test_frequencies=test_frequencies)

    pipeline['plv'] = PhaseLockingValue(dummy_process,
                                        weight_data=True,
                                        weight_across_epochs=False,
                                        test_frequencies=test_frequencies)

    pipeline.run()

    subject_info = SubjectInformation(subject_id=subject)
    conditions = np.array([0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 0.0])
    codes = np.array(["B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9"])
    _condition = float(conditions[np.where(condition_id == codes)[0]])
    measurement_info = MeasurementInformation(experiment="itd_damping",
                                              condition=_condition)
    _recording = {'type': 'MEG', 'fs': fs * u.Hz}
    _parameters = {'itm_rate': 6.4102564102564106 * u.Hz,
                   'modulation_frequency': 44.871794871794876 * u.Hz}
    # now we save our data to a database
    pipeline['saver'] = SaveToDatabase(database_path=database_path,
                                       subject_information=subject_info,
                                       measurement_information=measurement_info,
                                       stimuli_information=_parameters,
                                       recording_information=_recording,
                                       processes_list=[pipeline['time_average_ipm_fr'],
                                                       pipeline['time_average_sub_harmonic'],
                                                       pipeline['fft_ave_full_freq'],
                                                       pipeline['plv']
                                                       ],
                                       include_waveforms=True)
    pipeline.run()

    evoked = epochs.average()
    yfft = np.abs(np.fft.fft(evoked.data, axis=1)) * 2 / evoked.data.shape[1]
    freq = np.arange(0, evoked.data.shape[1]) * fs / evoked.data.shape[1]
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(freq, yfft.T)
    ax.set_xlim(0, 60)
    fig.savefig(data.figures_path + sep + 'fft.png')
    fig.clf()

    index = np.argmin(np.abs((freq - data.freq_of_interest[0])))
    ampl = yfft[:, index]
    gfp = np.std(ampl)

    results = {'max_amp': [np.max(ampl)], 'gfp': [gfp], 'condition': [condition_id], 'subject': [subject]}
    df = pd.DataFrame.from_dict(results)
    if not os.path.isfile(data_file):
        df.to_csv(data_file, mode='a', sep='\t', index=False)

    else:
        df.to_csv(data_file, mode='a', header=False, sep='\t', index=False)


if __name__ == "__main__":
    database_path = data_path_meg + 'data_meg_fft.sqlite'
    subjects = ['3205', '3239', '3249', '3252', '3341', '3504', '3540', '3544', '3581', '3587']
    all_conditions = ["B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9"]
    conditions_subject = list(itertools.product(subjects, all_conditions))
    for _subject, _condition in conditions_subject:
        with open(subjects_dir + sep + _subject + sep + 'processing.json') as json_file:
            processing = json.load(json_file)
        run_case(subject=_subject, condition_id=_condition, database_path=database_path,
                 bad_channels=processing['bad_channels'],
                 event_channel=processing['trigger_channel'],
                 f_low=2.0,
                 f_high=48.0
                 )
