from os import listdir
from meg_processing.tools import get_file, get_sources_main_components
import mne
import numpy as np
import os
import json
from meg_processing.tools import basic_pipe_line, get_source, get_source_by_average, read_source_by_average
from peegy.io.storage.data_storage_tools import PandasDataTable, store_data, MeasurementInformation, SubjectInformation
import matplotlib
from environment import subjects_mri_dir, subjects_dir, data_path_meg, figures_path_meg, root_directory
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')
mne.viz.set_3d_backend('pyvistaqt')
if mne.viz.get_3d_backend() == 'mayavi':
    os.environ['ETS_TOOLKIT'] = 'qt4'
    os.environ['QT_API'] = 'pyqt5'


if __name__ == "__main__":
    conditions_id = ['B{:d}'.format(_i) for _i in [9] + list(range(1, 9))]
    conditions_labels = ['Diotic', '±500 µs', '±1000 µs', '±1500 µs', '±2000 µs',
                         '±2500 µs', '±3000 µs', '±3500 µs', '±4000 µs']
    subjects = listdir(subjects_dir)
    # method = 'sLORETA'
    # method = 'eLORETA'
    method = 'dSPM'
    source_type = 'surface'
    auto_events = True
    new_fs = 6.4102564102564106 * 15
    mfreqs = [6.4102564102564106, 44.871794871794876, 6.4102564102564106 / 2]
    for mf in mfreqs:
        if mf < 4:
            #  ####IPM-FR sub harmonic ######
            _file_filter = 'data_fsaverage_1_4_{:}_auto_events_{:}'.format(method, auto_events)
            f_low = 1.5
            f_high = 4.5
        if mf < 7:
            #  ####IPM-FR ######
            _file_filter = 'data_fsaverage_2_8_{:}_auto_events_{:}'.format(method, auto_events)
            f_low = 2.0
            f_high = 8.0
        else:
            # ####ASSR ######
            _file_filter = 'data_fsaverage_37_48_{:}_auto_events_{:}'.format(method, auto_events)
            f_low = 37.0
            f_high = 48.0

        database_path = data_path_meg + 'data_meg_sources_fft_data.sqlite'
        for condition, label in zip(conditions_id, conditions_labels):
            all_stc = []
            measurement_info = MeasurementInformation(experiment="itd_damping", condition=label)
            for _idx_s, subject in enumerate(subjects):
                if subject in ['.', 'figures', '.data', 'group_averages', 'mri']:
                    continue
                if not os.path.isdir(subjects_dir + os.path.sep + subject):
                    continue
                subject_info = SubjectInformation(subject_id=subject)
                file_source = get_file(subjects_directory=subjects_dir,
                                       subject=subject,
                                       folder=condition,
                                       condition=_file_filter,
                                       type='.pkl')
                with open(subjects_dir + os.path.sep + subject + os.path.sep + 'processing.json') as json_file:
                    processing = json.load(json_file)
                _stc = get_source_by_average(subject=subject,
                                             subjects_dir=subjects_dir,
                                             subjects_mri_dir=subjects_mri_dir,
                                             sampling_rate=new_fs,
                                             bads=processing['bad_channels'],
                                             condition=condition,
                                             method=method,
                                             eog_ref_ch='MEG 102',
                                             ecg_ref_ch='MEG 052',
                                             event_channel=processing['trigger_channel'],
                                             f_low=f_low,
                                             f_high=f_high,
                                             auto_events=auto_events,
                                             auto_events_rate=6.4102564102564106,
                                             solve_in_frequency_domain=False,
                                             )
                all_stc.append(_stc[0])
                if all_stc:
                    _figure_path = subjects_dir + os.path.sep + subject + os.sep + condition + os.sep + 'figures'
                    _file_name = '{:}'.format(_file_filter)
                    _src_fname = subjects_dir + os.path.sep + subject + os.path.sep + 'oct6-src.fif'
                    _src = mne.read_source_spaces(_src_fname)
                    fsave_vertices = [s['vertno'] for s in _src]
                    src_fname = subjects_mri_dir + 'fsaverage/oct6-src.fif'
                    src = mne.read_source_spaces(src_fname)
                    _stc_dummy = mne.SourceEstimate(all_stc[-1].data,
                                                    all_stc[-1].vertices,
                                                    all_stc[-1].tmin, all_stc[-1].tstep)
                    _data = get_sources_main_components(stc_in=_stc_dummy,
                                                        source=src,
                                                        figure_path=_figure_path,
                                                        file_name=_file_name,
                                                        label=label,
                                                        subjects_mri_dir=subjects_mri_dir,
                                                        freq=mf)
                    _recording = {'type': 'MEG', 'fs': _stc_dummy.sfreq}
                    _parameters = {'itm_rate': 6.4102564102564106,
                                   'modulation_frequency': 44.871794871794876,
                                   'frequency_tested': mf}

                    frequency_table = PandasDataTable(table_name='freq_table',
                                                      pandas_df=_data)

                    # now we save our data to a database
                    store_data(database_path=database_path,
                               subject_info=subject_info,
                               measurement_info=measurement_info,
                               recording_info=_recording,
                               stimuli_info=_parameters,
                               pandas_df=[frequency_table])

            if all_stc:
                data_stc = np.mean(np.array([s.data for s in all_stc]), axis=0)
                stc = mne.SourceEstimate(data_stc, all_stc[0].vertices, all_stc[0].tmin, all_stc[0].tstep)
                _folder = subjects_dir + os.path.sep + 'group_averages' + os.path.sep + method
                if not os.path.isdir(_folder):
                    os.makedirs(_folder)
                _fname = _folder + os.path.sep
                _file_name = 'average_{:}'.format(_file_filter)
                _ave_data = get_sources_main_components(stc_in=stc,
                                                        source=src,
                                                        figure_path=_figure_path,
                                                        file_name=_file_name,
                                                        label=label,
                                                        subjects_mri_dir=subjects_mri_dir,
                                                        freq=mf)
                _recording = {'type': 'MEG', 'fs': _stc_dummy.sfreq}
                _parameters = {'itm_rate': 6.4102564102564106,
                               'modulation_frequency': 44.871794871794876,
                               'frequency_tested': mf}

                frequency_table = PandasDataTable(table_name='freq_table',
                                                  pandas_df=_ave_data)
                subject_info = SubjectInformation(subject_id='fsaverage')

                # now we save our data to a database
                store_data(database_path=database_path,
                           subject_info=subject_info,
                           measurement_info=measurement_info,
                           recording_info=_recording,
                           stimuli_info=_parameters,
                           pandas_df=[frequency_table])
