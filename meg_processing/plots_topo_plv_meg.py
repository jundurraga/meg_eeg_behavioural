from peegy.io.storage.plot_tools import plot_topographic_maps, get_topographic_maps_as_df
from peegy.io.storage.data_storage_reading_tools import sqlite_tables_to_pandas
from peegy.layouts import layouts
import numpy as np
import astropy.units as u
import matplotlib
from environment import figures_path_meg, data_path_meg
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')

lay = layouts.Layout(file_name='KIT-160.lay')
lay.plot_layout()
database_path = data_path_meg + 'data_meg_fft_3Hz.sqlite'
df = sqlite_tables_to_pandas(database_path=database_path,
                             tables=['rayleigh_test'])
df = df.rayleigh_test
df['ITD'] = df.loc[:, 'condition']
df = df.sort_values(by=['ITD'], ascending=False)
df['itd'] = (df['ITD'] * 1e3).astype(int).astype('category')
df['diotic'] = df['itd'] == 0
df['frequency_tested'] = np.round(df['frequency_tested'], decimals=1).astype('category')
# dataset = df.query('diotic == False and frequency_tested == 44.9')
dataset = df
dataset = dataset.reset_index(drop=True)
dataset['frequency_tested'] = dataset['frequency_tested'].apply(lambda x: x * u.Hz)
dataset['itd_condition'] = ('$\pm$' + (dataset['itd']).astype(str) + ' $\mu$s')
dataset.loc[dataset['itd_condition'] == '$\pm$0 $\mu$s', ['itd_condition']] = 'Diotic'
dataset['itd_condition'] = dataset['itd_condition'].astype('category')
dataset['itd_condition'] = dataset['itd_condition'].cat.remove_unused_categories()
dataset['itd_condition'] = dataset['itd_condition'].cat.reorder_categories(['$\pm$500 $\mu$s',
                                                                            '$\pm$1000 $\mu$s',
                                                                            '$\pm$1500 $\mu$s',
                                                                            '$\pm$2000 $\mu$s',
                                                                            '$\pm$2500 $\mu$s',
                                                                            '$\pm$3000 $\mu$s',
                                                                            '$\pm$3500 $\mu$s',
                                                                            '$\pm$4000 $\mu$s',
                                                                            'Diotic'])
# extract the spectral magnitude
plv_average = get_topographic_maps_as_df(dataframe=dataset,
                                         group_by=['itd_condition', 'frequency_tested'],
                                         channels_column='channel',
                                         topographic_value='plv',
                                         layout='KIT-160.lay',
                                         apply_function=lambda x: np.ma.mean(x, axis=2)
                                         )


fig_out = plot_topographic_maps(dataframe=plv_average,
                                rows_by='itd_condition',
                                cols_by='frequency_tested',
                                normalize=False,
                                channels_column='channel',
                                topographic_value='mean_amplitude',
                                layout='KIT-160.lay',
                                title_by='col',
                                title_v_offset=-0.05,
                                min_topographic_value=0,
                                max_topographic_value=1,
                                # title='ITD-FR',
                                color_map_label='PLV',
                                show_sensors=False,
                                )
inch = 2.54
fig_out.set_size_inches(3 * 2/inch, 13.5/inch)
fig_out.subplots_adjust(top=0.98, bottom=0.062, wspace=0.0, left=0.16, right=0.90)
fig_out.savefig(figures_path_meg + 'figure_S1b.png', dpi=600)
fig_out.savefig(figures_path_meg + 'figure_S1b.pdf', dpi=600)
