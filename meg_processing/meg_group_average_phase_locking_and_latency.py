import copy
import tempfile
import pandas as pd
import json
from peegy.io.storage.data_storage_tools import PandasDataTable, store_data, MeasurementInformation, SubjectInformation
from peegy.processing.statistics.eeg_statistic_tools import discrete_phase_locking_value, freq_bin_phase_locking_value
from meg_processing.tools import get_file, read_source_by_epochs, read_source_by_average, fdr_pvalue,\
    get_sources_data, plot_surface, basic_pipe_line, source_by_epochs
import mne
import os
from os import listdir
import numpy as np
import astropy.units as u
from environment import subjects_mri_dir, subjects_dir, data_path_meg, figures_path_meg, root_directory
import matplotlib
# if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
#     matplotlib.use('Qt5Agg')
mne.viz.set_3d_backend('pyvistaqt')

if __name__ == "__main__":
    # conditions_id = ['B{:d}'.format(_i) for _i in list(range(1, 10))]
    # conditions_labels = ['±500 µs', '±1000 µs', '±1500 µs', '±2000 µs',
    #                      '±2500 µs', '±3000 µs', '±3500 µs', '±4000 µs', 'Diotic']
    conditions_id = ['B{:d}'.format(_i) for _i in [9] + list(range(1, 9))]
    conditions_labels = ['Diotic', '±500 µs', '±1000 µs', '±1500 µs', '±2000 µs',
                         '±2500 µs', '±3000 µs', '±3500 µs', '±4000 µs']
    # conditions_id = conditions_id[8:9]
    # conditions_labels = conditions_labels[8:9]

    subjects = listdir(subjects_dir)

    # method = 'sLORETA'
    # method = 'eLORETA'
    method = 'dSPM'

    auto_events = False
    # source_type = 'mixed_source'
    source_type = 'surface'
    save_plots = True
    solve_in_frequency_domain = True
    database_path = data_path_meg + 'data_meg_group_sources_phase_angle_latency.sqlite'
    new_fs = 6.4102564102564106 * 15
    mfreqs = [6.4102564102564106 / 2, 6.4102564102564106, 44.871794871794876]
    for mf in mfreqs:
        # ####FULL ######
        _file_filter = 'data_fsaverage_2_48_{:}_auto_events_{:}'.format(method, auto_events)
        f_low = 2.0
        f_high = 48.0

        for condition, label in zip(conditions_id, conditions_labels):
            all_data = []
            _stc = []
            for _idx_s, subject in enumerate(subjects):
                all_stcs = []
                if not os.path.isdir(subjects_dir + os.path.sep + subject + os.path.sep + condition):
                    continue
                # file_source = None
                file_source = get_file(subjects_directory=subjects_dir,
                                       subject=subject,
                                       folder=condition,
                                       condition=_file_filter,
                                       type='.pkl')
                if file_source is None:
                    with open(subjects_dir + os.path.sep + subject + os.path.sep + 'processing.json') as json_file:
                        processing = json.load(json_file)
                    data = basic_pipe_line(subject=subject,
                                           subjects_dir=subjects_dir,
                                           bads=processing['bad_channels'],
                                           condition_id=condition,
                                           remove_eoc_ecg=False,
                                           eog_ref_ch='MEG 102',
                                           ecg_ref_ch='MEG 052',
                                           event_channel=processing['trigger_channel'],
                                           f_low=f_low,
                                           f_high=f_high,
                                           auto_events=False
                                           )
                    all_stcs = source_by_epochs(data=data,
                                                method=method,
                                                solve_in_frequency_domain=solve_in_frequency_domain,
                                                sampling_rate=new_fs,
                                                freqs_of_interest=[mf],
                                                source_type=source_type)
                else:
                    if source_type == 'surface':
                        all_stcs = read_source_by_epochs(
                            data_path=file_source,
                            method=method,
                            sampling_rate=new_fs,
                            freqs_of_interest=[mf],
                            source_type=source_type,
                            root_path=root_directory,
                            solve_in_frequency_domain=solve_in_frequency_domain)
                    if source_type == 'mixed_source':
                        all_stcs = read_source_by_average(
                            data_path=file_source,
                            method=method,
                            subjects_mri_dir=subjects_mri_dir,
                            subject=subject,
                            subjects_dir=subjects_dir,
                            sampling_rate=new_fs,
                            freqs_of_interest=[mf],
                            source_type=source_type,
                            solve_in_frequency_domain=solve_in_frequency_domain)
                for _stc in all_stcs:
                    if not isinstance(_stc, list):
                        _stc = [_stc]
                    pulled_data = np.array([_s.data for _s in _stc])
                    pulled_data = np.transpose(pulled_data, axes=[2, 1, 0])
                    all_data.append(pulled_data)
                # break
            _stc_dummy = _stc[0]
            f = tempfile.TemporaryFile()
            all_data = np.dstack(all_data)
            tmp_data = np.memmap(f, dtype=np.complex64, shape=all_data.shape)
            tmp_data[:] = all_data[:] * u.dimensionless_unscaled
            del all_data

            tables = pd.DataFrame()
            subject = 'fsaverage'
            alpha = 0.05
            if solve_in_frequency_domain:
                amp, plv, z_plv, z_crit, p_values, angles, dof, rn = freq_bin_phase_locking_value(
                    yfft=tmp_data)
            else:
                amp, plv, z_plv, z_crit, p_values, angles, dof, rn = discrete_phase_locking_value(
                    data=np.real(tmp_data),
                    alpha=alpha,
                    fs=_stc_dummy.sfreq,
                    frequency=mf)
            # normalize amplitude by noise
            amp = amp / rn

            alpha_fdr = fdr_pvalue(alpha_level=alpha, p_values=p_values.reshape(-1, 1))
            n_sources = plv.shape[1]

            # p-values
            _stc_dummy_p_values = mne.SourceEstimate(p_values.reshape(-1, 1),
                                                     _stc_dummy.vertices,
                                                     _stc_dummy.tmin, _stc_dummy.tstep)
            # amplitudes values; data is scaled by residual noise
            # noise variance is equal to signal variance
            z_value_amp = None
            if source_type == 'surface' or not solve_in_frequency_domain:
                z_value_amp = amp
            if source_type == 'mixed_source' and solve_in_frequency_domain:
                z_value_amp = amp * np.sqrt(dof)

            _stc_dummy_z_amp = mne.SourceEstimate(z_value_amp.reshape(-1, 1),
                                                  _stc_dummy.vertices,
                                                  _stc_dummy.tmin, _stc_dummy.tstep)
            # z-values from plv test
            _stc_dummy_z_plv = mne.SourceEstimate(z_plv.reshape(-1, 1),
                                                  _stc_dummy.vertices,
                                                  _stc_dummy.tmin, _stc_dummy.tstep)
            # phase-values normalized by full-cycle (2 * pi)

            _stc_dummy_phase = mne.SourceEstimate(angles.reshape(-1, 1) / (2 * np.pi),
                                                  _stc_dummy.vertices,
                                                  _stc_dummy.tmin, _stc_dummy.tstep)
            # correct delay of for steady-state responses
            _phase = angles.reshape(-1, 1) * u.rad
            _phase_c = copy.copy(_phase)
            _phase_c[_phase_c < 0] = np.pi * u.rad + _phase_c[_phase_c < 0]
            phase_correction = (0 / 344) * 2 * np.pi * u.rad * mf
            _phase_c = _phase_c + phase_correction
            phase_delay = _phase_c + np.pi * u.rad / 2 * (mf > 30)
            latency = (phase_delay + 2 * np.pi * u.rad * (mf > 30)) / (2 * np.pi * u.rad * mf) * 1000
            _stc_dummy_latency_values = mne.SourceEstimate(latency,
                                                           _stc_dummy.vertices,
                                                           _stc_dummy.tmin, _stc_dummy.tstep)
            # plv
            _stc_dummy_plv = mne.SourceEstimate(plv.reshape(-1, 1),
                                                _stc_dummy.vertices,
                                                _stc_dummy.tmin, _stc_dummy.tstep)

            _folder = figures_path_meg + 'group_averages' + os.path.sep + method
            if not os.path.isdir(_folder):
                os.makedirs(_folder)

            # next we do several plot, all of them at the regions where PLV is significant
            # plot plv #######################################
            _fname = _folder + os.path.sep + '{:}_{:}_{:}_{:}_{:}_plv'.format(subject,
                                                                              method,
                                                                              condition,
                                                                              _file_filter,
                                                                              round(mf))

            out = get_sources_data(stc_in=_stc_dummy_plv,
                                   stc_p_values=_stc_dummy_p_values,
                                   stc_z_values=_stc_dummy_z_plv,
                                   subjects_mri_dir=subjects_mri_dir,
                                   measurement_type='plv',
                                   frequency_tested=mf,
                                   alpha=alpha_fdr,
                                   dof=dof,
                                   n_sources=n_sources,
                                   label=label)
            tables = pd.concat([tables, out])
            if save_plots:
                _lim = [0, 0.5, 1]
                if np.round(mf) == 3.0:
                    _lim = [0, 0.25, 0.5]

                plot_surface(stc_in=_stc_dummy_plv,
                             stc_p_values=_stc_dummy_p_values,
                             file_name=_fname,
                             surface='inflated',
                             significant_only=False,
                             lims=_lim,
                             label=label,
                             subjects_mri_dir=subjects_mri_dir,
                             alpha=alpha_fdr,
                             show_colorbar=condition == 'B9')

            # plot amp-z value ##############################################
            _fname = _folder + os.path.sep + '{:}_{:}_{:}_{:}_{:}_amplitude_z_value'.format(subject,
                                                                                            method,
                                                                                            condition,
                                                                                            _file_filter,
                                                                                            round(mf))
            out = get_sources_data(stc_in=_stc_dummy_z_amp,
                                   stc_p_values=_stc_dummy_p_values,
                                   stc_z_values=_stc_dummy_z_plv,
                                   subjects_mri_dir=subjects_mri_dir,
                                   measurement_type='amp_z_value',
                                   frequency_tested=mf,
                                   alpha=alpha_fdr,
                                   dof=dof,
                                   n_sources=n_sources,
                                   label=label)
            tables = pd.concat([tables, out])
            _lim = [3, 12, 30] if source_type == "surface" else [1, 2, 4]
            if np.round(mf) == 3.0:
                _lim = [2, 4, 6]
            if save_plots:
                plot_surface(stc_in=_stc_dummy_z_amp,
                             stc_p_values=_stc_dummy_p_values,
                             surface='inflated',
                             file_name=_fname,
                             lims=_lim,
                             label=label,
                             subjects_mri_dir=subjects_mri_dir,
                             alpha=alpha_fdr,
                             show_colorbar=condition == 'B9')

            # plot phase of response #################################################
            _fname = _folder + os.path.sep + '{:}_{:}_{:}_{:}_{:}_phase'.format(subject,
                                                                                method,
                                                                                condition,
                                                                                _file_filter,
                                                                                round(mf))
            out = get_sources_data(stc_in=_stc_dummy_phase,
                                   stc_p_values=_stc_dummy_p_values,
                                   stc_z_values=_stc_dummy_z_plv,
                                   subjects_mri_dir=subjects_mri_dir,
                                   measurement_type='phase',
                                   frequency_tested=mf,
                                   alpha=alpha_fdr,
                                   dof=dof,
                                   n_sources=n_sources,
                                   label=label)
            tables = pd.concat([tables, out])
            if save_plots:
                plot_surface(stc_in=_stc_dummy_phase,
                             stc_p_values=_stc_dummy_p_values,
                             significant_only=True,
                             file_name=_fname,
                             color_map='seismic',
                             surface='inflated',
                             lims=[-0.5, 0, 0.5],
                             label=label,
                             subjects_mri_dir=subjects_mri_dir,
                             alpha=alpha_fdr,
                             show_colorbar=condition == 'B9')

            # plot latency #############################################
            _fname = _folder + os.path.sep + '{:}_{:}_{:}_{:}_{:}_latency'.format(subject,
                                                                                  method,
                                                                                  condition,
                                                                                  _file_filter,
                                                                                  round(mf))
            out = get_sources_data(stc_in=_stc_dummy_latency_values,
                                   stc_p_values=_stc_dummy_p_values,
                                   stc_z_values=_stc_dummy_z_plv,
                                   subjects_mri_dir=subjects_mri_dir,
                                   measurement_type='latency',
                                   frequency_tested=mf,
                                   alpha=alpha_fdr,
                                   dof=dof,
                                   n_sources=n_sources,
                                   label=label)
            tables = pd.concat([tables, out])
            _lim = [50, 70, 90] if mf < 30 else [20, 40, 60]
            if save_plots:
                plot_surface(stc_in=_stc_dummy_latency_values,
                             stc_p_values=_stc_dummy_p_values,
                             significant_only=True,
                             file_name=_fname,
                             color_map='hsv',
                             lims=_lim,
                             label=label,
                             surface='inflated',
                             subjects_mri_dir=subjects_mri_dir,
                             alpha=alpha_fdr,
                             show_colorbar=condition == 'B9')

            measure_table = PandasDataTable(table_name='measure_table',
                                            pandas_df=tables)
            subject_info = SubjectInformation(subject_id=subject)
            measurement_info = MeasurementInformation(experiment="itd_damping",
                                                      condition=label)
            _recording = {'type': 'MEG', 'fs': _stc_dummy_latency_values.sfreq}
            _parameters = {'itm_rate': 6.4102564102564106,
                           'modulation_frequency': 44.871794871794876,
                           'measurement_type': label,
                           'mf': mf}
            # # now we save our data to a database
            store_data(database_path=database_path,
                       subject_info=subject_info,
                       measurement_info=measurement_info,
                       recording_info=_recording,
                       stimuli_info=_parameters,
                       pandas_df=[measure_table])
