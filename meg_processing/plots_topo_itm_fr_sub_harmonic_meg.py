from peegy.io.storage.plot_tools import plot_topographic_maps, get_topographic_maps_as_df
from peegy.io.storage.data_storage_reading_tools import sqlite_tables_to_pandas
from peegy.layouts import layouts
import pandas as pd
import numpy as np
import astropy.units as u
import matplotlib
from environment import figures_path_meg, data_path_meg
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')

lay = layouts.Layout(file_name='KIT-160.lay')
lay.plot_layout()
database_path = data_path_meg + 'data_meg_fft_3Hz.sqlite'
df = sqlite_tables_to_pandas(database_path=database_path,
                             tables=['hotelling_t2_freq'])
df = df.hotelling_t2_freq
df = df[df.data_source == 'fft_ave_full_freq']
df['complex_amplitude'] = df['mean_amplitude'] * np.exp(1j * df['mean_phase'])
df['ITD'] = df.loc[:, 'condition']
df = df.sort_values(by=['ITD'], ascending=False)
df['itd'] = (df['ITD'] * 1e3).astype(int).astype('category')
df['diotic'] = df['itd'] == 0
df['frequency_tested'] = np.round(df['frequency_tested'], decimals=1).astype('category')
df['complex_amplitude'] = df['complex_amplitude'] * 1e15
dataset = df
dataset = dataset.reset_index(drop=True)
dataset['frequency_tested'] = dataset['frequency_tested'].cat.remove_unused_categories()
dataset['itd_condition'] = ('$\pm$' + (dataset['itd']).astype(str) + ' $\mu$s')
dataset.loc[dataset['itd_condition'] == '$\pm$0 $\mu$s', ['itd_condition']] = 'Diotic'
dataset['itd_condition'] = dataset['itd_condition'].astype('category')
dataset['itd_condition'] = dataset['itd_condition'].cat.remove_unused_categories()
dataset['itd_condition'] = dataset['itd_condition'].cat.reorder_categories(['$\pm$500 $\mu$s',
                                                                            '$\pm$1000 $\mu$s',
                                                                            '$\pm$1500 $\mu$s',
                                                                            '$\pm$2000 $\mu$s',
                                                                            '$\pm$2500 $\mu$s',
                                                                            '$\pm$3000 $\mu$s',
                                                                            '$\pm$3500 $\mu$s',
                                                                            '$\pm$4000 $\mu$s',
                                                                            'Diotic'])
# normalize
dataset['norm'] = dataset.groupby(['subject_id', 'frequency_tested'], observed=True)['complex_amplitude'].transform(
    lambda x: np.max(np.abs(x)))
dataset['complex_amplitude'] = dataset['complex_amplitude'] / dataset['norm']
# extract the spectral magnitude
complex_average = get_topographic_maps_as_df(dataframe=dataset,
                                             group_by=['itd_condition', 'frequency_tested'],
                                             channels_column='channel',
                                             topographic_value='complex_amplitude',
                                             layout='KIT-160.lay',
                                             apply_function=lambda x: np.ma.mean(x, axis=2)
                                             )


# reference phase set as the reference of the maximum amplitude for the 500 us condition
ref_value = pd.DataFrame()
ref_value['ref_location'] = complex_average.groupby(['itd_condition', 'frequency_tested'],
                                                    observed=True)['potentials'].apply(
    lambda x: np.unravel_index(np.abs(x.values[0]).argmax(), x.values[0].shape))
ref_value = ref_value.reset_index()
centered_data = complex_average.copy()
centered_data['phase'] = centered_data['potentials'].apply(
    lambda x: np.angle(x[np.unravel_index(np.abs(x).argmax(), x.shape)]))

ref_phase = centered_data.groupby(['frequency_tested'], observed=False).apply(
    lambda x: x[x['itd_condition'] == "$\pm$500 $\mu$s"]['phase']).reset_index()

for _, _ref_row in ref_phase.iterrows():
    _loc = centered_data.index[centered_data['frequency_tested'] == _ref_row['frequency_tested']]
    _phase = _ref_row['phase']
    for _is in _loc.tolist():
        centered_data.at[_is, 'potentials'] = (
                np.abs(centered_data.at[_is, 'potentials']) *
                np.cos(np.ma.angle(centered_data.at[_is, 'potentials']) - _phase))

centered_data['frequency_tested'] = centered_data['frequency_tested'].apply(lambda x: x * u.Hz)
# plot topographic map
fig_out = plot_topographic_maps(dataframe=centered_data,
                                rows_by='itd_condition',
                                cols_by='frequency_tested',
                                normalize=False,
                                channels_column='channel',
                                topographic_value='potentials',
                                layout='KIT-160.lay',
                                title_by='col',
                                title_v_offset=-0.05,
                                # min_topographic_value=-0.25,
                                # max_topographic_value=0.25,
                                color_map_label='Normalized Amplitude [A.U.]',
                                show_sensors=False,
                                show_sensor_label=False
                                )

inch = 2.54
fig_out.set_size_inches(3 * 2/inch, 13.5/inch)
fig_out.savefig(figures_path_meg + 'figure_sub_harmonic_amplitude.png', dpi=600)
fig_out.savefig(figures_path_meg + 'figure_sub_harmonic_amplitude.pdf', dpi=600)
