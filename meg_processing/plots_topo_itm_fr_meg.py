from peegy.io.storage.plot_tools import plot_topographic_maps, get_topographic_maps_as_df
from peegy.io.storage.data_storage_reading_tools import sqlite_tables_to_pandas
from peegy.layouts import layouts
import pandas as pd
import numpy as np
import matplotlib
from environment import figures_path_meg, data_path_meg
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')

lay = layouts.Layout(file_name='KIT-160.lay')
lay.plot_layout()
database_path = data_path_meg + 'data_meg_fft_3Hz.sqlite'
df = sqlite_tables_to_pandas(database_path=database_path,
                             tables=['hotelling_t2_freq'])
df = df.hotelling_t2_freq
df = df[df.data_source == 'fft_ave_full_freq']
df['complex_amplitude'] = df['mean_amplitude'] * np.exp(1j * df['mean_phase']) * 1e15
df['ITD'] = df.loc[:, 'condition']
df = df.sort_values(by=['ITD'], ascending=False)
df['itd'] = (df['ITD'] * 1e3).astype(int).astype('category')
df['diotic'] = df['itd'] == 0
df['frequency_tested'] = np.round(df['frequency_tested'], decimals=1).astype('category')
# dataset = df.query('diotic == False and frequency_tested == 6.4')
dataset = df.query('frequency_tested == 6.4')

dataset = dataset.reset_index(drop=True)
dataset['frequency_tested'] = dataset['frequency_tested'].cat.remove_unused_categories()
dataset['itd_condition'] = ('-' + (dataset['itd']).astype(str) + "/" + dataset['itd'].astype(str)).astype('category')

dataset['itd_condition'] = dataset['itd_condition'].cat.remove_unused_categories()
dataset['itd_condition'] = dataset['itd_condition'].cat.reorder_categories(['-500/500',
                                                                            '-1000/1000',
                                                                            '-1500/1500',
                                                                            '-2000/2000',
                                                                            '-2500/2500',
                                                                            '-3000/3000',
                                                                            '-3500/3500',
                                                                            '-4000/4000',
                                                                            '-0/0',
                                                                            ])

# extract the spectral magnitude
complex_average = get_topographic_maps_as_df(dataframe=dataset,
                                             group_by=['itd_condition'],
                                             channels_column='channel',
                                             topographic_value='complex_amplitude',
                                             layout='KIT-160.lay',
                                             apply_function=lambda x: np.ma.mean(x, axis=2)
                                             )
# reference phase set as the reference of the maximum amplitude for the 500 us condition
ref_value = pd.DataFrame()
ref_value['ref_location'] = complex_average.groupby(['itd_condition'], observed=True)['potentials'].apply(
    lambda x: np.unravel_index(np.abs(x.values[0]).argmax(), x.values[0].shape))
ref_value = ref_value.reset_index()
centered_data = complex_average.copy()
_idx_match = int(np.argwhere(ref_value['itd_condition'] == '-500/500'))
_i, _j = ref_value.at[_idx_match, 'ref_location']
ref_phase = np.angle(centered_data.at[_idx_match, 'potentials'][_i, _j])

for _is, _ref_row in ref_value.iterrows():
    # _idx_match = int(np.argwhere(centered_data['itd_condition'] == _ref_row['itd_condition']))
    # _i, _j = _ref_row['ref_location']
    # ref_phase = np.angle(centered_data.at[_idx_match, 'potentials'][_i, _j])
    centered_data.at[_is, 'potentials'] = (
            np.abs(centered_data.at[_is, 'potentials']) *
            np.cos(np.ma.angle(centered_data.at[_is, 'potentials']) - ref_phase))


# plot topographic map
fig_out = plot_topographic_maps(dataframe=centered_data,
                                rows_by='itd_condition',
                                normalize=False,
                                channels_column='channel',
                                topographic_value='mean_amplitude',
                                layout='KIT-160.lay',
                                # title_by='row',
                                title_by=None,
                                title_v_offset=-0.05,
                                min_topographic_value=-16,
                                max_topographic_value=16,
                                # title='ITD-FR',
                                color_map_label='Amplitude [fT]',
                                show_sensors=False,
                                )

inch = 2.54
fig_out.set_size_inches(2.5/inch, 12/inch)
fig_out.savefig(figures_path_meg + 'figure_1f.png', dpi=600)
fig_out.savefig(figures_path_meg + 'figure_1f.pdf', dpi=600)
