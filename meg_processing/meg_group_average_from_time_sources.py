from os import listdir
import mne
import numpy as np
import os
import json
from environment import subjects_dir, subjects_mri_dir, figures_path_meg
from meg_processing.tools import get_file, plot_surface, get_source_by_average
mne.viz.set_3d_backend('pyvistaqt')
os.environ['ETS_TOOLKIT'] = 'qt5'
os.environ['QT_API'] = 'pyqt'


if __name__ == "__main__":
    conditions_id = ['B{:d}'.format(_i) for _i in list(range(1, 10))]
    conditions_labels = ['±500 µs', '±1000 µs', '±1500 µs', '±2000 µs',
                         '±2500 µs', '±3000 µs', '±3500 µs', '±4000 µs', 'Diotic']
    subjects = listdir(subjects_dir)

    # method = 'sLORETA'
    # lims = [1, 2, 3]

    # method = 'eLORETA'
    # lims = [0, 0.1e-10, 0.2e-10]

    method = 'dSPM'
    lims = [3, 12, 20]
    source_type = 'surface'
    new_fs = 6.4102564102564106 * 15
    auto_events = True
    ref_data = None
    mfreqs = [6.4102564102564106, 44.871794871794876, 6.4102564102564106 / 2]
    for mf in mfreqs:
        if mf < 4:
            # Sub-harmonic
            _file_filter = 'data_fsaverage_1_4_{:}_auto_events_{:}'.format(method, auto_events)
            f_low = 1.5
            f_high = 4.5
            lims = [0, 4, 10]
        elif mf < 7:
            #  ####IPM-FR ######
            _file_filter = 'data_fsaverage_2_8_{:}_auto_events_{:}'.format(method, auto_events)
            f_low = 2.0
            f_high = 8.0
            lims = [0, 10, 30]
        else:
            # ####ASSR ######
            _file_filter = 'data_fsaverage_37_48_{:}_auto_events_{:}'.format(method, auto_events)
            f_low = 37.0
            f_high = 48.0
            lims = [0, 10, 30]
        for condition, label in zip(conditions_id, conditions_labels):
            all_stc = []
            for _idx_s, subject in enumerate(subjects):
                if subject in ['.', 'figures', '.data', 'group_averages', 'mri']:
                    continue
                if not os.path.isdir(subjects_dir + os.path.sep + subject):
                    continue

                with open(subjects_dir + os.path.sep + subject + os.path.sep + 'processing.json') as json_file:
                    processing = json.load(json_file)
                    _stc = get_source_by_average(subject=subject,
                                                 subjects_dir=subjects_dir,
                                                 subjects_mri_dir=subjects_mri_dir,
                                                 sampling_rate=new_fs,
                                                 bads=processing['bad_channels'],
                                                 condition=condition,
                                                 method=method,
                                                 eog_ref_ch='MEG 102',
                                                 ecg_ref_ch='MEG 052',
                                                 event_channel=processing['trigger_channel'],
                                                 f_low=f_low,
                                                 f_high=f_high,
                                                 auto_events=auto_events,
                                                 auto_events_rate=6.4102564102564106,
                                                 solve_in_frequency_domain=False,
                                                 )
                    all_stc.append(_stc[0])
            if all_stc:
                # because we compute RMS, peak amplitude is estimated by multiplying by sqrt(2).
                # In addition, amplitude are scaled by sqrt(len(all_stc)) so that stc maps maintain null hypothesis
                # at 1. In other words, by averaging, the standard deviation of the noise decreases
                # by std(noise)/sqrt(N)
                data_stc = np.sqrt(len(all_stc)) * np.sqrt(2) * np.mean([np.sqrt(np.mean(s.data ** 2, axis=1))
                                                                         for s in all_stc], axis=0)[:, np.newaxis]
                stc = mne.SourceEstimate(data_stc, all_stc[0].vertices, all_stc[0].tmin, all_stc[0].tstep)
                _folder = figures_path_meg + 'group_averages' + os.path.sep + method
                if not os.path.isdir(_folder):
                    os.makedirs(_folder)
                _fname = _folder + os.path.sep + 'average_{:}_{:}_{:}'.format(method, condition, _file_filter)
                plot_surface(stc_in=stc,
                             file_name=_fname,
                             lims=lims,
                             label=label,
                             subjects_mri_dir=subjects_mri_dir,
                             surface='inflated',
                             plot_parcellation=False)
