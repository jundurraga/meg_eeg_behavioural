import numpy as np
import matplotlib
from peegy.io.storage.data_storage_reading_tools import sqlite_waveforms_to_pandas
from peegy.io.storage.plot_tools import plot_time_frequency_responses
from peegy.layouts import layouts
from environment import figures_path_meg, data_path_meg
from astropy import units as u
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')
database_path = data_path_meg + 'data_meg_fft_3Hz.sqlite'

lay = layouts.Layout(file_name='KIT-160.lay')
# pick sensor above auditory cortex for example
# channel = 'MEG 125'
# channel = ['MEG 126']
channel = ['MEG 054']
# channel = ['MEG 123', 'MEG 054']
# channel = ['MEG 126', 'MEG 106'] # shows dipole
df = sqlite_waveforms_to_pandas(database_path=database_path,
                                group_factors=['condition',
                                               'data_source',
                                               'itm_rate',
                                               'modulation_frequency',
                                               'channel'],
                                channels=channel)
df = df[np.logical_or(df.data_source == 'time_average_sub_harmonic', df.data_source == 'fft_ave_full_freq')]
df['ITD'] = df['condition']
df = df.sort_values(by=['ITD'], ascending=False)
df['itd'] = (df['ITD'] * 1e3).astype(int).astype('category')
df['diotic'] = df['itd'] == 0
# dataset = df.query('diotic == False')
dataset = df
dataset = dataset.reset_index(drop=True)
dataset['itd_condition'] = ('$\pm$' + (dataset['itd']).astype(str) + ' $\mu$s').astype('category')
dataset['itd_condition'] = dataset['itd_condition'].cat.remove_unused_categories()
dataset['itd_condition'] = dataset['itd_condition'].cat.reorder_categories(['$\pm$500 $\mu$s',
                                                                            '$\pm$1000 $\mu$s',
                                                                            '$\pm$1500 $\mu$s',
                                                                            '$\pm$2000 $\mu$s',
                                                                            '$\pm$2500 $\mu$s',
                                                                            '$\pm$3000 $\mu$s',
                                                                            '$\pm$3500 $\mu$s',
                                                                            '$\pm$4000 $\mu$s',
                                                                            '$\pm$0 $\mu$s',
                                                                            ])
n_cycles_to_plot = 4
x_fs = np.mean(dataset['x_fs'])
samples_per_cycle = np.mean(dataset['x_fs'] / dataset['itm_rate'])
sub_average_time_buffer_size = int(samples_per_cycle * n_cycles_to_plot)
time_vmarkers = np.arange(0, sub_average_time_buffer_size, samples_per_cycle) / x_fs
itm_rate = np.mean(df['itm_rate'])
am_rate = np.mean(df['modulation_frequency'])
freq_vmarkers = np.array([itm_rate/2, itm_rate, am_rate])
# dataset.y = dataset.y.apply(lambda x: x * 1e15)
dataset['domain'] = dataset['domain'].astype('category')
dataset['domain'] = dataset['domain'].cat.reorder_categories(['time', 'frequency'])
fig_out = plot_time_frequency_responses(dataframe=dataset,
                                        cols_by='domain',
                                        rows_by='itd_condition',
                                        title_by='',
                                        y_unit_to=u.fT,
                                        sub_average_time_buffer_size=sub_average_time_buffer_size,
                                        time_xlim=[0, sub_average_time_buffer_size / x_fs],
                                        time_ylim=[-23, 23],
                                        freq_xlim=[0, 48],
                                        freq_ylim=[0, 23],
                                        time_vmarkers=time_vmarkers,
                                        freq_vmarkers=freq_vmarkers,
                                        show_sem=False,
                                        freq_vmarkers_style='v',
                                        ylabel='Amplitude [fT]',
                                        show_individual_waveforms=True,
                                        individual_waveforms_alpha=0.1,
                                        )
inch = 2.54
fig_out.set_size_inches(6.5/inch, 12/inch)
fig_out.subplots_adjust(top=0.98, bottom=0.07, wspace=0.22, left=0.16, right=0.90)
fig_out.savefig(figures_path_meg + 'figure_sub_harmonic.pdf'.format(channel), dpi=600)
fig_out.savefig(figures_path_meg + 'figure_sub_harmonic.png'.format(channel), dpi=600)
