import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import basinhopping
from scipy.stats import pearsonr
from environment import data_path_behavioural
if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
    matplotlib.use('Qt5Agg')


def t_value(r, n):
    t = r * np.sqrt(n - 2) / np.sqrt(1 - r ** 2)
    return t


def func(x, p):
    return p[0] * np.exp((500 - x) / p[1]) + p[2] * np.sin(2 * np.pi * x / 1500 + p[4]) + p[3]


def error(p, x, target):
    e = np.mean((target - func(x, p)) ** 2.0)
    return e


data_base_path = data_path_behavioural + 'results.csv'
df = pd.read_csv(data_base_path)
df.Condition = df.Condition * 1000
df.Order = 8 - df.Order

group = df.groupby('Condition').mean('Order')
group = group.reset_index()
pdopt = []
# for _, _group in groups:
xdata = group.Condition.values
ydata = group.Order.values
xdata = np.concatenate(([500], xdata))
ydata = np.concatenate(([8], ydata))
x0 = np.array([2, 10, 0, 0, 0])
ydata = ydata - np.max(ydata)
bounds = ((0, 100),
          (10, 10000),
          (0, 100),
          (-15, 15),
          (-np.pi, np.pi),
          )
minimizer_kwargs = {"method": "L-BFGS-B", "bounds": bounds, "tol": 1e-10,
                    "args": (xdata, ydata)}
res_1 = basinhopping(error, x0=x0,
                     niter=2000, stepsize=0.2, T=1e-8,
                     minimizer_kwargs=minimizer_kwargs)
optimal_fit = res_1.x
plt.plot(xdata, ydata)
plt.plot(xdata, func(xdata, optimal_fit))
plt.show()
corr = pearsonr(ydata, func(xdata, optimal_fit))
t_val = t_value(corr.statistic, corr._n)
print('Thau: {:}, Phi {:}'.format(optimal_fit[1], optimal_fit[4] * 180 / np.pi))
print('R2 = {:}'.format(corr[0] ** 2))
data = pd.DataFrame([{'thau': optimal_fit[1], 'phi': optimal_fit[4]}])
data.to_csv(data_path_behavioural + 'behavioural_fitted_sin_exp.csv', index=False)
