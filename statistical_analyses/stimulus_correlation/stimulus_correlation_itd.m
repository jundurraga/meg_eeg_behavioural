%% To run this code you download AEP_GUI and add folders with subfolders to matlab path
%% https://gitlab.com/open-source-brain/aep_gui
close all;
clear all;
cd(fileparts(which(mfilename)));
complex_corrs = [];
real_corrs = [];
for i = 1: 64
    [y,t,s] = generateNoiseAltDelaySignal(...
        'Fs', 48000, ...
        'Duration', 4.056,...
        'CycleRate', 16384, ...
        'ModulationFrequency', 44.871794871794876, ...
        'ITDModRate', 6.4102564102564106, ...
        'RoundToCycle', 1 ...
    );
    yh = y(:, 1) + 1j * hilbert(y(:, 1));
    [c, lags] = xcorr(yh, conj(yh), round(s.Fs*0.004), 'normalized');
    [cr, lags] = xcorr(y(:,1), y(:,1), round(s.Fs*0.004), 'normalized');
    complex_corrs = [complex_corrs, abs(c)];
    real_corrs = [real_corrs, cr];
end
figure;
times = lags/s.Fs*1000;
plot(times, complex_corrs, ':', 'Color', [1,1,1] * 0.9);
hold on;
plot(times, real_corrs , 'Color', [1,1,1] * 0.9);
hold on;
h1 = plot(times, mean(complex_corrs, 2), 'red');
hold on;
h2 = plot(times, mean(real_corrs, 2), 'k');
vline(-4.5:0.5:4.5);
legend([h1, h2], 'complex-valued-corr', 'real-valued-corr');
origin = readmatrix( ...
"../../meg_processing/data/interpolated_fitting_data_meg.csv");
times_int = (-4:0.5:4)';
int_data_x_corr_complex = interp1(times, mean(complex_corrs, 2), ...
    times_int);
int_data_x_corr_origin= interp1(times, mean(real_corrs, 2), times_int);
plot(times_int, int_data_x_corr_origin, 'ko', 'DisplayName', ...
    'real-valued-corr');
plot(times_int, int_data_x_corr_complex, 'r*', ...
    'DisplayName', 'complex-valued-corr');
% plot(times_int, max(int_data_x_corr_complex) - int_data_x_corr_complex, 's', ...
%     'DisplayName', 'complex-valued-corr-change');
times_orig = linspace(-4, 4, 80)';
int_origin = interp1(times_orig , origin(:, 3), times);

int_data_x_origin= interp1(times, int_origin, times_int);
h3 = plot(times , int_origin, 'b-', ...
    'DisplayName', 'meg-response');
plot(times_int, int_data_x_origin, 'bo', 'DisplayName', ...
    'real-valued-corr');
   
half_origin = interp1(times_orig , origin(:, 3), times(1:(end + 1)/2));

figure; 
% plot(times(1:end/2), half_origin);
% hold on;
% plot(times(1:end/2), half_iac);
% [cor_half,lags] = xcorr(half_origin, half_iac);
% plot(times(1:end/2), circshift(half_complex, loc));
% plot(times(1:end/2), circshift(half_complex, loc));
% [val, loc] = max(half_origin);
% plot(times(1:end/2), circshift(half_real, loc));
complex_corrs = mean(complex_corrs, 2)';
real_corrs = mean(real_corrs, 2)';
[val1, loc1] = max(int_origin);
[val2, loc2] = max(complex_corrs);
delay = abs(loc2 - loc1);
sh_complex_corrs = circshift(complex_corrs, -delay);
sh_real_corrs = circshift(real_corrs, -delay);
half_complex = sh_complex_corrs(1 : (end +1)/2);
half_real = sh_real_corrs(1 : (end +1)/2);
aligned_complex = [half_complex, half_complex(end - 1:-1:1)];
aligned_real = [half_real, half_real(end - 1:-1:1)];
int_aligned_complex_corr = interp1(times, aligned_complex, ...
    times_int);
int_aligned_real_corr= interp1(times, aligned_real, ...
    times_int);
h4 = plot(times, aligned_complex, 'b-', ...
    'DisplayName', 'complex-aligned');
hold on;
plot(times_int, int_aligned_complex_corr, 'bo', 'DisplayName', ...
    'real-valued-corr');
plot(times, int_origin);
h5 = plot(times, aligned_real, ...
   'DisplayName', 'real-aligned');
plot(times_int, int_aligned_real_corr, 'bo', 'DisplayName', ...
    'real-valued-corr');

ylabel('Correlation');
xlabel('Time lag [ms]');
legend([h1, h2, h3]);
data = [times_int, ...
    int_data_x_corr_origin, ...
    int_data_x_corr_complex, ...
    int_aligned_real_corr, ...
    int_aligned_complex_corr ...    
    ];
T = array2table(data);
T.Properties.VariableNames(1:5) = {'time', ...
    'real_valued_corr', ...
    'complex_valued_corr', ...
    'real_valued_corr_aligned', ...
    'complex_valued_aligned'...
    };
writetable(T, ...
    "../../meg_processing/data/interpolated_correlation_signal.csv")

figure;
[cor, lag] = xcorr(int_origin, aligned_complex);
plot(lag, cor)
